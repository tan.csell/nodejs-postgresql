import Joi from 'joi';
import { values } from 'lodash';
import Banner from '../../common/models/banner.model';

module.exports = {
    // GET v1/imports
    listValidation: {
        query: {
            // pagging
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .allow(null, ''),
            sort_by: Joi.string()
                .only([
                    'created_at',
                    'updated_at'
                ])
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),

            // search
            types: Joi.string()
                .trim()
                .allow(null, '')
        }
    },

    // POST v1/imports
    createValidation: {
        body: {
            url: Joi.string()
                .max(255)
                .trim()
                .required(),
            type: Joi.string()
                .trim()
                .only(values(Banner.Types))
                .default(Banner.Types.HOME_BANNER_V_1),
            title: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            content: Joi.string()
                .trim()
                .allow(null, ''),
            position: Joi.number()
                .integer()
                .default(0)
                .allow(null, ''),
            image_url: Joi.string()
                .max(255)
                .trim()
                .required(),
            mobile_url: Joi.string()
                .max(255)
                .trim()
                .required(),
            is_visible: Joi.bool()
                .default(true)
                .allow(null, '')
        }
    },

    // POST v1/imports
    updateValidation: {
        body: {
            url: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            type: Joi.string()
                .trim()
                .only(values(Banner.Types)),
            title: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            content: Joi.string()
                .trim()
                .allow(null, ''),
            position: Joi.number()
                .integer()
                .allow(null, ''),
            image_url: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            mobile_url: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, '')
        }
    }
};
