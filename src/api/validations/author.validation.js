import Joi from 'joi';

module.exports = {
    // GET /admin/v1/collection.models
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .default(20)
                .allow(null, ''),
            sort_by: Joi.string()
                .only([
                    'name',
                    'created_at',
                    'updated_at',
                ])
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            keyword: Joi.string()
                .trim()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, ''),
        }
    },

    // POST /admin/v1/posts
    createValidation: {
        body: {
            // attributes
            title: Joi.string()
                .trim()
                .required()
                .error(() => ({ message: 'Vui lòng nhập tên trang' })),
            slug: Joi.string()
                .allow(null, ''),
            content: Joi.string()
                .trim()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung' })),
            // social seo
            meta_url: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta url' })),
            meta_title: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta title' })),
            meta_image: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta image' })),
            meta_keyword: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta keyword' })),
            meta_description: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta description' })),

            // manager
            is_visible: Joi.bool()
                .allow(null, ''),
            is_home_visible: Joi.bool()
                .default(false)
                .allow(null, ''),
            is_active: Joi.bool()
                .default(true)
                .allow(null, '')
        }
    },

    // PUT /admin/v1/posts/:id
    updateValidation: {
        params: {
            id: Joi.number()
                .required()
        },
        body: {
            title: Joi.string()
                .trim()
                .required()
                .error(() => ({ message: 'Vui lòng nhập tên trang' })),
            slug: Joi.string()
                .trim()
                .max(255)
                .required()
                .error(() => ({ message: 'Vui lòng nhập đường dẫn' })),
            content: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung' })),

            // social seo
            meta_url: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta url' })),
            meta_title: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta title' })),
            meta_image: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta image' })),
            meta_keyword: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta keyword' })),
            meta_description: Joi.string()
                .max(255)
                .allow(null, '')
                .error(() => ({ message: 'Vui lòng nhập đúng định dạng meta description' })),

            // manager
            is_visible: Joi.bool()
                .allow(null, ''),
            is_active: Joi.bool()
                .allow(null, '')
        }
    },

    // GET /admin/v1/posts/:id
    detailValidation: {
        params: {
            id: Joi.string().trim()
                .required()
        }
    },

    // DEL /admin/v1/posts/:id
    deleteValidation: {
        params: {
            id: Joi.string().trim()
                .required()
        }
    },
};
