import Joi from 'joi';
import { values } from 'lodash';
import Category from '../../common/models/category.model';

module.exports = {
    // GET /v1/categories
    listCategory: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .default(1),
            slug: Joi.string(),
            group: Joi.string(),
            nested: Joi.bool(),
            is_active: Joi.bool(),
            is_visible: Joi.bool(),
            is_home_visible: Joi.bool(),
            keyword: Joi.string()
                .allow(null, '')
        }
    },

    // POST /v1/categories
    createCategory: {
        body: {
            name: Joi.string()
                .max(255)
                .trim()
                .required(),
            slug: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            logo: Joi.string()
                .trim()
                .allow('', null),
            group: Joi.string()
                .only(values(Category.Groups))
                .required(),
            image: Joi.string()
                .max(255)
                .trim()
                .allow('', null),
            content: Joi.string()
                .allow('', null),
            parent_id: Joi.number()
                .allow(null),

            // configuration
            is_active: Joi.bool(),
            is_visible: Joi.bool(),
            is_home_visible: Joi.bool()
        }
    },

    // PUT /v1/categories/:id
    updateCategory: {
        body: {
            name: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            slug: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            logo: Joi.string()
                .trim()
                .allow('', null),
            image: Joi.string()
                .max(255)
                .trim()
                .allow('', null),
            content: Joi.string()
                .allow('', null),

            // configuration
            is_active: Joi.bool(),
            is_visible: Joi.bool(),
            is_home_visible: Joi.bool()
        }
    }
};
