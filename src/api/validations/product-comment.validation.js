import Joi from 'joi';

module.exports = {
    // GET /v1/product-comments
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .default(20),
            sort_by: Joi.string()
                .only([
                    'created_at',
                    'updated_at',
                    'parent_id',
                    'like_count'
                ])
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            product_id: Joi.number()
                .allow(null, ''),
            parent_id: Joi.number()
                .allow(null, ''),
        }
    },

    // POST /v1/product-comments
    createValidation: {
        body: {
            content: Joi.string()
                .trim()
                .required(),
            parent_id: Joi.number()
                .allow(null, '')
                .default(null),
            product: Joi.object({
                id: Joi.number()
                    .required(),
                name: Joi.string()
                    .required(),
                option_id: Joi.number()
                    .required(),
                thumbnail_url: Joi.string()
                    .allow(null, ''),
            }).required(),
            is_reply: Joi.bool()
                .allow(null, '')
                .default(false)
        }
    },

    // PUT /v1/product-comments/:id
    updateValidation: {
        body: {
            content: Joi.string()
                .trim()
                .allow(null, '')
        }
    }
};
