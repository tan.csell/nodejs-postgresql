import Joi from 'joi';

module.exports = {
    // GET /v1/vouchers
    listVoucher: {
        skip: Joi.number()
            .min(0)
            .default(0),
        limit: Joi.number()
            .min(1)
            .max(1000)
            .default(20),
        sort_by: Joi.string()
            .only([
                'created_at',
                'updated_at',
                'name'
            ])
            .allow(null, ''),
        order_by: Joi.string()
            .only([
                'desc',
                'asc'
            ])
            .allow(null, ''),
        is_visible: Joi.bool()
            .allow(null, ''),
        is_active: Joi.bool()
            .allow(null, ''),
        keyword: Joi.string()
            .trim()
            .allow(null, ''),
        statuses: Joi.string()
            .trim()
            .allow(null, ''),
        categories: Joi.string()
            .trim()
            .allow(null, ''),
        members: Joi.string()
            .trim()
            .allow(null, ''),
        stores: Joi.string()
            .trim()
            .allow(null, ''),
        users: Joi.string()
            .trim()
            .allow(null, ''),
        product_id: Joi.number()
            .allow(null, ''),
    },
};
