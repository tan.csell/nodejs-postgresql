import Joi from 'joi';
import { values } from 'lodash';
import Order from '../../common/models/order.model';
import Payment from '../../common/models/payment.model';

const phoneRegex = /^\+?[0-9]{9,15}$/;

const itemSchema = {
    id: Joi.number()
        .integer()
        .required(),
    option_id: Joi.number()
        .integer()
        .required(),
    campaign_option_id: Joi.number()
        .integer()
        .allow(null, ''),
    total_quantity: Joi.number()
        .min(1)
        .required()
};

const customerSchema = {
    id: Joi.number()
        .required(),
    name: Joi.string()
        .allow(null, ''),
    phone: Joi.string()
        .trim()
        .regex(phoneRegex)
        .allow(null, ''),
    address: Joi.string()
        .allow(null, '')
};

const discountSchema = {
    id: Joi.number()
        .default(0)
        .allow(null),
    type: Joi.number()
        .integer()
        .required(),
    name: Joi.string()
        .trim()
        .required(),
    group: Joi.string()
        .trim()
        .required(),
    value: Joi.number()
        .required()
};

const paymentSchema = {
    type: Joi.string()
        .only(values(Payment.Types))
        .allow(null, ''),
    note: Joi.string()
        .allow(null, ''),
    card: Joi.object()
        .keys({
            ccv: Joi.string()
                .required(),
            name: Joi.string()
                .required(),
            bank: Joi.string()
                .required(),
            number: Joi.string()
                .required(),
        })
        .allow(null, ''),
    value: Joi.number()
        .allow(null, ''),
    method: Joi.number()
        .only(values(Payment.Methods))
        .allow(null, ''),
};

const deliverySchema = {
    code: Joi.string()
        .allow(null, ''),
    note: Joi.string()
        .allow(null, ''),
    weight: Joi.number()
        .allow(null, ''),
    length: Joi.number()
        .allow(null, ''),
    height: Joi.number()
        .allow(null, ''),
    width: Joi.number()
        .allow(null, ''),
    payment_method: Joi.string()
        .allow(null, ''),
    payment_by: Joi.string()
        .required(),
    service_id: Joi.string()
        .required(),
    service_name: Joi.string()
        .required(),
    receiver_name: Joi.string()
        .required(),
    receiver_phone: Joi.string()
        .trim()
        .regex(phoneRegex)
        .required(),
    receiver_note: Joi.string()
        .allow(null, ''),
    receiver_address: Joi.string()
        .required(),
    receiver_province: Joi.object()
        .required(),
    receiver_district: Joi.object()
        .required(),
    receiver_ward: Joi.object()
        .required(),
    receiver_date: Joi.string()
        .allow(null, ''),
    total_shipping_fee: Joi.number()
        .default(0)
        .allow(null, ''),
    total_order_price: Joi.number()
        .default(0)
        .allow(null, ''),
    total_shipping_cod: Joi.number()
        .default(0)
        .allow(null, ''),
    total_shipping_price: Joi.number()
        .default(0)
        .allow(null, ''),
    total_insurrance_price: Joi.number()
        .default(0)
        .allow(null, ''),
};

module.exports = {
    // GET /v1/carts/list
    listCart: {
        query: {
            // sort
            offset: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .default(20)
                .allow(null, ''),
            sort_by: Joi.string()
                .trim()
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            date_type: Joi.string()
                .only([
                    'Date',
                    'Time'
                ])
                .allow(null, ''),

            // search
            code: Joi.string()
                .trim()
                .allow(null, ''),
            note: Joi.string()
                .trim()
                .allow(null, ''),
            status: Joi.string()
                .trim()
                .allow(null, ''),
            hashtag: Joi.string()
                .trim()
                .allow(null, ''),
            customer: Joi.string()
                .trim()
                .allow(null, ''),
            product_name: Joi.string()
                .trim()
                .allow(null, ''),
            product_sku: Joi.string()
                .trim()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, '')
        }
    },

    // POST /v1/carts
    createCart: {
        headers: {
            'x-client-id': Joi.string()
                .required(),
            'user-agent': Joi.string()
                .required()
        }
    },

    // PUT /v1/carts
    updateCart: {
        body: {
            note: Joi.string()
                .allow(null, ''),
            customer: Joi.object()
                .keys(customerSchema)
                .allow(null, '')
        }
    },

    // POST /v1/carts/confirm
    confirmCart: {
        body: {
            note: Joi.string()
                .max(255)
                .allow(null, ''),
            source: Joi.string()
                .max(50)
                .only(values(Order.Sources))
                .required(),
            payments: Joi.array()
                .items(paymentSchema)
                .allow(null, ''),
            deliveries: Joi.array()
                .items(deliverySchema)
                .allow(null, ''),
            discounts: Joi.array()
                .items(discountSchema)
                .allow(null, ''),
            products: Joi.array()
                .items(itemSchema)
                .min(1)
                .required(),
            total_coin: Joi.number()
                .integer()
                .allow(null, ''),
            total_point: Joi.number()
                .integer()
                .allow(null, ''),
            total_quantity: Joi.number()
                .integer()
                .allow(null, ''),
            total_price_before_discount: Joi.number()
                .integer()
                .allow(null, ''),
            total_price_after_discount: Joi.number()
                .integer()
                .allow(null, ''),
            total_discount_value: Joi.number()
                .integer()
                .allow(null, ''),
            total_exchange_price: Joi.number()
                .integer()
                .allow(null, ''),
            total_original_price: Joi.number()
                .integer()
                .allow(null, ''),
            total_shipping_fee: Joi.number()
                .integer()
                .allow(null, ''),
            total_return_fee: Joi.number()
                .integer()
                .allow(null, ''),
            total_price: Joi.number()
                .integer()
                .allow(null, '')
        }
    },

    // POST /v1/carts/items/:id
    addItem: {
        body: itemSchema
    },

    // PUT /v1/carts/items/:id
    updateItem: {
        body: {
            total_quantity: Joi.number()
                .required(),
        }
    },

    // DELETE /v1/carts/items/:id
    removeItem: {
        params: {
            id: Joi.string()
                .required()
        }
    },

    // POST /v1/carts/payments
    addPayment: {
        body: paymentSchema
    },

    // POST /v1/carts/deliveries
    addDelivery: {
        body: deliverySchema
    }
};
