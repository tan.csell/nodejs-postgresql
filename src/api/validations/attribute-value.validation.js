import Joi from 'joi';

module.exports = {
    // GET v1/product-attributes
    listAttributeValue: {
        query: {
            // pagging
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .allow(null, ''),
            sort_by: Joi.string()
                .allow(null, ''),
            order_by: Joi.string()
                .allow(null, ''),
            attribute_code: Joi.string()
                .trim()
                .allow(null, ''),
            attribute_group: Joi.string()
                .trim()
                .allow(null, ''),
            attribute_value: Joi.string()
                .trim()
                .allow(null, ''),
            is_active: Joi.bool()
                .allow(null, ''),
            systems: Joi.string()
                .trim()
                .allow(null, '')
        }
    },

    // POST v1/product-attributes
    createAttributeValue: {
        body: {
            icon: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            value: Joi.string()
                .trim()
                .max(255)
                .required(),
            position: Joi.number()
                .integer()
                .allow(null, ''),
            meta_url: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_title: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_image: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_keyword: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_description: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            attribute_code: Joi.string()
                .trim()
                .required(),
            attribute_group: Joi.string()
                .trim()
                .required()
        }
    },

    // POST v1/product-attributes
    updateAttributeValue: {
        body: {
            slug: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            icon: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            value: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            position: Joi.number()
                .integer()
                .allow(null, ''),
            meta_url: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_title: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_image: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_keyword: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
            meta_description: Joi.string()
                .trim()
                .max(255)
                .allow(null, ''),
        }
    }
};
