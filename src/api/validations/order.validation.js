import Joi from 'joi';

module.exports = {
    // GET: /v1/orders
    listOrder: {
        query: {
            // sort
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .default(20)
                .allow(null, ''),
            sort_by: Joi.string()
                .trim()
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            date_type: Joi.string()
                .only([
                    'Date',
                    'Time'
                ])
                .allow(null, ''),

            // search
            code: Joi.string()
                .trim()
                .allow(null, ''),
            note: Joi.string()
                .trim()
                .allow(null, ''),
            hashtag: Joi.string()
                .trim()
                .allow(null, ''),
            customer: Joi.string()
                .trim()
                .allow(null, ''),
            receiver: Joi.string()
                .trim()
                .allow(null, ''),
            is_match: Joi.bool()
                .allow(null, ''),
            user_code: Joi.string()
                .trim()
                .allow(null, ''),
            order_id: Joi.string()
                .trim()
                .allow(null, ''),
            order_code: Joi.string()
                .trim()
                .allow(null, ''),
            return_code: Joi.string()
                .trim()
                .allow(null, ''),
            delivery_code: Joi.string()
                .trim()
                .allow(null, ''),
            product_sku: Joi.string()
                .trim()
                .allow(null, ''),
            product_name: Joi.string()
                .trim()
                .allow(null, ''),
            product_note: Joi.string()
                .trim()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, ''),
            min_total_price: Joi.number()
                .allow(null, ''),
            max_total_price: Joi.number()
                .allow(null, ''),
            min_total_discount_value: Joi.number()
                .allow(null, ''),
            max_total_discount_value: Joi.number()
                .allow(null, ''),
            payment_methods: Joi.string()
                .trim()
                .allow(null, ''),
            shipping_methods: Joi.string()
                .trim()
                .allow(null, ''),
            statuses: Joi.string()
                .trim()
                .allow(null, ''),
            channels: Joi.string()
                .trim()
                .allow(null, ''),
            sources: Joi.string()
                .trim()
                .allow(null, ''),
            confirmers: Joi.string()
                .trim()
                .allow(null, ''),
            creaters: Joi.string()
                .trim()
                .allow(null, ''),
            types: Joi.string()
                .trim()
                .allow(null, ''),
            stores: Joi.string()
                .trim()
                .allow(null, '')
        }
    },
};
