import Joi from 'joi';
import { values } from 'lodash';
import Post from '../../common/models/post.model';

module.exports = {
    // GET /admin/v1/posts
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .default(20)
                .allow(null, ''),
            sort_by: Joi.string()
                .only([
                    'title',
                    'created_at',
                    'updated_at',
                ])
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            keyword: Joi.string()
                .trim()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, ''),
            is_home_visible: Joi.bool()
                .allow(null, ''),
            is_favorite_visible: Joi.bool()
                .allow(null, ''),
            categories: Joi.string()
                .trim()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, '')
        }
    },

    // POST /admin/v1/posts
    createValidation: {
        body: {
            // attributes
            type: Joi.string()
                .only(values(Post.Types))
                .required(),
            title: Joi.string()
                .max(155)
                .required(),
            slug: Joi.string()
                .max(255)
                .allow(null, ''),
            avatar: Joi.string()
                .required(),
            content: Joi.string()
                .required(),
            description: Joi.string()
                .required(),
            categories: Joi.array()
                .items({
                    id: Joi.number().required(),
                    name: Joi.string().required(),
                    slug: Joi.string().required()
                })
                .required(),
            products: Joi.array()
                .items({
                    id: Joi.number()
                        .integer(),
                    name: Joi.string()
                        .trim(),
                    sku: Joi.string()
                        .trim(),
                    slug: Joi.string()
                        .trim(),
                    thumbnail_url: Joi.string()
                        .trim()
                        .allow('', null),
                    normal_price: Joi.number(),
                    price: Joi.number(),
                })
                .error(() => ({ message: 'Vui lòng kiểm tra lại thông tin hàng hoá' })),


            hashtag: Joi.array()
                .items(Joi.string())
                .allow(null, '')
                .default([]),
            position: Joi.number()
                .allow(null, ''),
            // social seo
            meta_url: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_title: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_image: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_keyword: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_description: Joi.string()
                .max(255)
                .allow(null, ''),

            // manager
            is_visible: Joi.bool()
                .default(true)
                .allow(null, ''),
            is_home_visible: Joi.bool()
                .default(false)
                .allow(null, ''),
            is_active: Joi.bool()
                .default(true)
                .allow(null, '')
        }
    },

    // PUT /admin/v1/posts/:id
    updateValidation: {
        params: {
            id: Joi.number()
                .required()
        },
        body: {
            type: Joi.string()
                .allow(null, ''),
            title: Joi.string()
                .allow(null, ''),
            slug: Joi.string()
                .allow(null, ''),
            avatar: Joi.string()
                .allow(null, ''),
            content: Joi.string()
                .allow(null, ''),
            description: Joi.string()
                .allow(null, ''),
            categories: Joi.array()
                .items({
                    id: Joi.number().required(),
                    name: Joi.string().required(),
                    slug: Joi.string().required()
                })
                .allow(null, ''),
            hashtag: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            position: Joi.number()
                .allow(null, ''),
            products: Joi.array()
                .items({
                    id: Joi.number()
                        .integer(),
                    name: Joi.string()
                        .trim(),
                    sku: Joi.string()
                        .trim(),
                    slug: Joi.string()
                        .trim(),
                    thumbnail_url: Joi.string()
                        .trim()
                        .allow('', null),
                    normal_price: Joi.number(),
                    price: Joi.number(),
                })
                .error(() => ({ message: 'Vui lòng kiểm tra lại thông tin hàng hoá' })),

            // social seo
            meta_url: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_title: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_image: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_keyword: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_description: Joi.string()
                .max(255)
                .allow(null, ''),

            // manager
            is_visible: Joi.bool()
                .allow(null, ''),
            is_active: Joi.bool()
                .allow(null, '')
        }
    },

    // GET /admin/v1/posts/:id
    detailValidation: {
        params: {
            id: Joi.number()
                .required()
        }
    },

    // DEL /admin/v1/posts/:id
    deleteValidation: {
        params: {
            id: Joi.number()
                .required()
        }
    },
};
