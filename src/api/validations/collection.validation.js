import Joi from 'joi';

module.exports = {
    // GET /admin/v1/collection.models
    listValidation: {
        query: {
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .default(20)
                .allow(null, ''),
            sort_by: Joi.string()
                .only([
                    'title',
                    'created_at',
                    'updated_at',
                ])
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            keyword: Joi.string()
                .trim()
                .allow(null, ''),
            product_name: Joi.string()
                .trim()
                .allow(null, ''),
            product_sku: Joi.string()
                .trim()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, ''),
            is_home_visible: Joi.bool()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, '')
        }
    },

    // POST /admin/v1/posts
    createValidation: {
        body: {
            // attributes
            title: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập tiêu đề tối đa 155 ký tự' })),
            slug: Joi.string()
                .allow(null, ''),
            avatar: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập ảnh đại diện tỉ lệ ngang' })),
            images: Joi.array()
                .min(1)
                .items(
                    Joi.string().required()
                        .error(() => ({ message: 'Vui lòng nhập DS ảnh bộ sưu tập trên desktop' })),
                )
                .error(() => ({ message: 'Vui lòng nhập DS ảnh bộ sưu tập trên desktop' })),
            mobile_images: Joi.array()
                .min(1)
                .items(
                    Joi.string().required()
                ).error(() => ({ message: 'Vui lòng nhập DS ảnh bộ sưu tập trên mobile' })),
            background: Joi.string()
                .allow(null, ''),
            cover: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập ảnh đại diện tỉ lệ dọc' })),
            content: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung trên desktop' })),
            mobile_content: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung trên mobile' })),
            description: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập mô tả tối đa 255 ký tự' })),
            products: Joi.array()
                .min(1)
                .items({
                    id: Joi.number()
                        .integer()
                        .required(),
                    name: Joi.string()
                        .trim()
                        .required(),
                    sku: Joi.string()
                        .trim()
                        .required(),
                    slug: Joi.string()
                        .trim()
                        .required(),
                    thumbnail_url: Joi.string()
                        .trim()
                        .allow('', null),
                    normal_price: Joi.number()
                        .required(),
                    price: Joi.number()
                        .required(),
                })
                .error(() => ({ message: 'Vui lòng kiểm tra lại thông tin hàng hoá' })),

            // social seo
            meta_url: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_title: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_image: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_keyword: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_description: Joi.string()
                .max(255)
                .allow(null, ''),

            // manager
            is_visible: Joi.bool()
                .allow(null, ''),
            is_home_visible: Joi.bool()
                .default(false)
                .allow(null, ''),
            is_active: Joi.bool()
                .default(true)
                .allow(null, '')
        }
    },

    // PUT /admin/v1/posts/:id
    updateValidation: {
        params: {
            id: Joi.number()
                .required()
        },
        body: {
            type: Joi.string()
                .allow('', null),
            title: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung tiêu đề' })),
            slug: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập đường dẫn' })),
            avatar: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập ảnh đại diện' })),
            images: Joi.array()
                .min(1)
                .items(
                    Joi.string().required()
                ).error(() => ({ message: 'Vui lòng nhập DS ảnh bộ sưu tập trên desktop' })),
            mobile_images: Joi.array()
                .min(1)
                .items(
                    Joi.string().required()
                )
                .error(() => ({ message: 'Vui lòng nhập DS ảnh bộ sưu tập trên mobile' })),
            content: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung trên desktop' })),
            mobile_content: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập nội dung trên mobile' })),
            description: Joi.string()
                .required()
                .error(() => ({ message: 'Vui lòng nhập mô tả tối đa 255 ký tự' })),
            products: Joi.array()
                .min(1)
                .items({
                    id: Joi.number()
                        .integer()
                        .required(),
                    name: Joi.string()
                        .trim()
                        .required(),
                    sku: Joi.string()
                        .trim()
                        .required(),
                    slug: Joi.string()
                        .trim()
                        .required(),
                    thumbnail_url: Joi.string()
                        .trim()
                        .allow('', null),
                    normal_price: Joi.number()
                        .required(),
                    price: Joi.number()
                        .required(),
                })
                .error(() => ({ message: 'Vui lòng kiểm tra lại thông tin hàng hoá' })),

            // social seo
            meta_url: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_title: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_image: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_keyword: Joi.string()
                .max(255)
                .allow(null, ''),
            meta_description: Joi.string()
                .max(255)
                .allow(null, ''),

            // manager
            is_visible: Joi.bool()
                .allow(null, ''),
            is_active: Joi.bool()
                .allow(null, '')
        }
    },

    // GET /admin/v1/posts/:id
    detailValidation: {
        params: {
            id: Joi.number()
                .required()
        }
    },

    // DEL /admin/v1/posts/:id
    deleteValidation: {
        params: {
            id: Joi.number()
                .required()
        }
    },
};
