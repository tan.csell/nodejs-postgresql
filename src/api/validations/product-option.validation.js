import Joi from 'joi';

module.exports = {
    // GET v1/products
    listProduct: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .min(1)
                .max(100000),
            sort_by: Joi.string()
                .trim()
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            sku: Joi.string()
                .trim()
                .allow(null, ''),
            product_name: Joi.string()
                .trim()
                .allow(null, ''),
            product_sku: Joi.string()
                .trim()
                .allow(null, ''),
            child_id: Joi.number()
                .integer()
                .allow(null, ''),
            keyword: Joi.string()
                .trim()
                .allow(null, ''),
            price_books: Joi.string()
                .trim()
                .allow(null, ''),
            categories: Joi.string()
                .trim()
                .allow(null, ''),
            attributes: Joi.string()
                .trim()
                .allow(null, ''),
            statuses: Joi.string()
                .trim()
                .allow(null, ''),
            brands: Joi.string()
                .trim()
                .allow(null, ''),
            types: Joi.string()
                .trim()
                .allow(null, ''),
            skus: Joi.string()
                .trim()
                .allow(null, ''),
            provider_option_id: Joi
                .allow(null, ''),
            provider_type: Joi.string()
                .trim()
                .allow(null, ''),
            provider_shop_id: Joi
                .allow(null, ''),
            is_connected: Joi.bool()
                .allow(null, ''),
            is_discounted: Joi.string()
                .allow(null, '')
        }
    },
    // PUT v1/products
    updateProduct: {
        body: {
            name: Joi.string()
                .max(155)
                .trim()
                .allow(null, ''),
            images: Joi.array()
                .items(Joi.string().max(255))
                .allow(null, ''),
            barcode: Joi.string()
                .max(50)
                .trim()
                .allow(null, ''),
            option_name: Joi.string()
                .max(155)
                .allow(null, ''),
            original_price: Joi.number()
                .allow(null, ''),
            normal_price: Joi.number()
                .allow(null, ''),
            price: Joi.number()
                .allow(null, '')
        }
    },
};
