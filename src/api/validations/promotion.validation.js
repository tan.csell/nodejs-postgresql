import Joi from 'joi';
import { values } from 'lodash';
import Promotion from '../../common/models/promotion.model';

module.exports = {
    // GET /v1/promotions
    listPromotion: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .default(20),
            sort_by: Joi.string()
                .trim()
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            statuses: Joi.string()
                .trim()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, ''),
            product_name: Joi.string()
                .trim()
                .allow(null, ''),
            product_sku: Joi.string()
                .trim()
                .allow(null, ''),
            type: Joi.string()
                .only(values(Promotion.Types)),
            keyword: Joi.string()
                .allow(null, ''),
        }
    }
};
