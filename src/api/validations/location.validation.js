import Joi from 'joi';

module.exports = {
    // GET /admin/v1/collection.models
    listValidation: {
        query: {
            skip: Joi.number().default(0).allow(null, ''),
            limit: Joi.number().default(20).allow(null, ''),
            sort_by: Joi.string().allow(null, ''),
            order_by: Joi.string().allow(null, ''),
            name: Joi.string().allow(null, '').error(() => ({ message: 'Vui lòng nhập đúng định dạng tìm kiếm theo tên khu vực' })),
            types: Joi.number().allow(null, '').error(() => ({ message: 'Vui lòng nhập đúng định dạng bộ lọc theo loại khu vực' })),
            parent_id: Joi.number().allow(null, ''),
        }
    },
};
