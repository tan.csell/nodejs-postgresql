import Joi from 'joi';

module.exports = {
    // GET v1/product-attributes
    listAttribute: {
        query: {
            // pagging
            skip: Joi.number()
                .min(0)
                .allow(null, ''),
            limit: Joi.number()
                .min(1)
                .max(100000)
                .allow(null, ''),
            sort_by: Joi.string()
                .only([
                    'created_at',
                    'updated_at'
                ])
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            type: Joi.string()
                .trim()
                .allow(null, ''),
            group: Joi.string()
                .trim()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, ''),
        }
    },

    // POST v1/product-attributes
    createAttribute: {
        body: {
            code: Joi.string()
                .trim()
                .max(255)
                .required(),
            name: Joi.string()
                .trim()
                .max(255)
                .required()
        }
    },

    // POST v1/product-attributes
    updateAttribute: {
        body: {
            name: Joi.string()
                .trim()
                .max(255)
                .allow(null, '')
        }
    }
};
