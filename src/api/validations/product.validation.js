import Joi from 'joi';

module.exports = {
    // GET v1/products
    listProduct: {
        query: {
            skip: Joi.number()
                .min(0)
                .default(0),
            limit: Joi.number()
                .min(1)
                .max(100000),
            sort_by: Joi.string()
                .trim()
                .allow(null, ''),
            order_by: Joi.string()
                .only([
                    'desc',
                    'asc'
                ])
                .allow(null, ''),
            types: Joi.string()
                .trim()
                .allow(null, ''),
            statuses: Joi.string()
                .trim()
                .allow(null, ''),
            attributes: Joi.string()
                .trim()
                .allow(null, ''),
            categories: Joi.string()
                .trim()
                .allow(null, ''),
            variations: Joi.string()
                .trim()
                .allow(null, ''),
            suppliers: Joi.string()
                .trim()
                .allow(null, ''),
            is_has_discount: Joi.bool()
                .allow(null, ''),
            is_top_hot: Joi.bool()
                .allow(null, ''),
            is_new_arrival: Joi.bool()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, ''),
            min_price: Joi.number()
                .allow(null, ''),
            max_price: Joi.number()
                .allow(null, ''),
            min_created_at: Joi.date()
                .allow(null, ''),
            max_created_at: Joi.date()
                .allow(null, ''),
            flash_deal_id: Joi.string()
                .allow(null, ''),
            hot_deal_id: Joi.string()
                .allow(null, ''),
            stock_value: Joi.number()
                .allow(null, ''),
            stock_id: Joi.number()
                .allow(null, ''),
            description: Joi.string()
                .trim()
                .allow(null, ''),
            keyword: Joi.string()
                .trim()
                .allow(null, '')
        }
    },
    // PUT v1/products
    updateProduct: {
        body: {
            slug: Joi.string()
                .max(255)
                .trim()
                .allow(null, ''),
            name: Joi.string()
                .max(155)
                .trim()
                .allow(null, ''),
            brand: Joi.string()
                .max(50)
                .trim()
                .allow(null, ''),
            barcode: Joi.string()
                .max(50)
                .trim()
                .allow(null, ''),
            hashtag: Joi.array()
                .items(Joi.string())
                .allow(null, ''),
            document: Joi.string()
                .allow(null, ''),
            description: Joi.string()
                .allow(null, ''),
            short_name: Joi.string()
                .max(50)
                .allow(null, ''),
            video_url: Joi.string()
                .max(255)
                .allow(null, ''),
            thumbnail_url: Joi.string()
                .max(255)
                .allow(null, ''),
            background_url: Joi.string()
                .max(255)
                .allow(null, ''),
            position: Joi.number()
                .allow(null, ''),
            position_new_hot: Joi.number()
                .allow(null, ''),
            position_new_arrival: Joi.number()
                .allow(null, ''),

            // config
            is_top_hot: Joi.bool()
                .allow(null, ''),
            is_new_arrival: Joi.bool()
                .allow(null, ''),
            is_visible: Joi.bool()
                .allow(null, ''),

            // stock & delivery
            unit: Joi.string()
                .max(20)
                .allow(null, ''),
            weight: Joi.number()
                .allow(null, ''),
            stock_min: Joi.number()
                .allow(null, ''),
            stock_max: Joi.number()
                .allow(null, ''),

            // extra data
            variations: Joi.array()
                .items({
                    name: Joi.string(),
                    values: Joi.array()
                        .items(Joi.string())
                })
                .allow(null, ''),
            attributes: Joi.array()
                .items({
                    id: Joi.number(),
                    name: Joi.string(),
                    values: Joi.array()
                        .items({
                            id: Joi.number(),
                            name: Joi.string()
                        })
                })
                .allow(null, ''),
            categories: Joi.array()
                .items({
                    id: Joi.number(),
                    name: Joi.string(),
                    slug: Joi.string()
                })
                .allow(null, ''),
            units: Joi.array()
                .items({
                    name: Joi.string()
                        .required(),
                    quantity: Joi.number()
                        .required(),
                    original_price: Joi.number()
                        .allow(null, '')
                        .default(0)
                })
                .allow(null, ''),
            parts: Joi.array()
                .items({
                    id: Joi.number()
                        .required(),
                    name: Joi.string()
                        .required(),
                    price: Joi.number()
                        .required(),
                    option_id: Joi.number()
                        .required(),
                    options: Joi.array()
                        .items({
                            name: Joi.string()
                                .required(),
                            quantity: Joi.number()
                                .required()
                        })
                        .required()
                })
                .allow(null, ''),
        }
    }
};
