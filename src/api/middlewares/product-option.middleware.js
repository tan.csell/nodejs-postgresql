import httpStatus from 'http-status';
import { isNil, omitBy, pick, includes } from 'lodash';
import { handler as ErrorHandler } from './error';
import Product from '../../common/models/product.model';
import ProductOption from '../../common/models/product-option.model';
import productOptionAdapter from '../../common/services/adapters/product-option-adapter';
import APIError from '../../common/utils/APIError';

/**
 * Load productOption and append to req.
 * @public
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await ProductOption.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.productOption = dataValues;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

exports.count = async (req, res, next) => {
    try {
        const count = await ProductOption.totalRecords(req.query);
        req.locals = req.locals ? req.locals : {};
        req.locals.count = count;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Load params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        if (req.body.price) {
            req.body.price = Number(req.body.price);
        } else {
            req.body.price = 0;
        }
        if (req.body.normal_price) {
            req.body.normal_price = Number(req.body.normal_price);
        } else {
            req.body.normal_price = 0;
        }
        if (req.body.original_price) {
            req.body.original_price = Number(req.body.original_price);
        } else {
            req.body.original_price = 0;
        }
        const params = ProductOption.filterParams(req.body);
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);
        req.body = productOptionAdapter.parseData(params);

        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

exports.all = async (req, res, next) => {
    try {
        const all = await ProductOption.allRecords(req.query);
        req.all = req.all ? req.all : {};
        req.all = all;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

/**
 * check variation
 */
exports.checkVariation = async (req, res, next) => {
    try {
        let check = false;
        let name = '';
        if (req.body.parent_id) {
            if (req.body.options && req.body.options.length > 0) {
                const product = await Product.get(req.body.parent_id);
                if (product.variations) {
                    product.variations.map((v, i) => {
                        if (product.units.length > 1) {
                            if (i < product.variations.length - 1) {
                                if (includes(v.values, req.body.options[i])) {
                                    check = true;
                                    name = req.body.options[i];
                                }
                            }
                        }
                        if (product.units.length === 0) {
                            if (includes(v.values, req.body.options[i])) {
                                check = true;
                            }
                        }
                        return null;
                    });
                }
            }
        }
        if (check) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: `Phân loại ${name} đã tồn tại`
            });
        } else {
            return next();
        }
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

/**
 * Check duplicate
 */
exports.checkDuplicate = async (req, res, next) => {
    try {
        const { sku } = req.body;
        const { productOption: option } = req.locals ? req.locals : {};

        // Check duplicate
        if (sku) {
            await ProductOption.checkDuplicateSku({
                sku: sku,
                id: option ? option.id : null
            });
        }

        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

/**
 * Load params
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { productOption } = req.locals;
        const params = ProductOption.filterParams(
            req.body
        );
        if (productOption.price) {
            productOption.price = Number(productOption.price);
        }
        if (productOption.normal_price) {
            productOption.normal_price = Number(productOption.normal_price);
        }
        if (productOption.original_price) {
            productOption.original_price = Number(productOption.original_price);
        }
        const dataChanged = ProductOption.getChangedProperties({
            oldModel: productOption,
            newModel: params
        });
        const paramChanged = pick(
            params, dataChanged
        );
        paramChanged.updated_by = pick(
            req.user, ['id', 'name']
        );
        if (isNil(paramChanged.sku)) {
            paramChanged.sku = productOption.sku;
        }
        if (isNil(paramChanged.name)) {
            paramChanged.name = productOption.name;
        }
        if (isNil(paramChanged.price_books)) {
            paramChanged.price_books = productOption.price_books;
        }

        req.body = productOptionAdapter.parseData(
            paramChanged
        );

        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};
