import { isNil, omitBy, pick } from 'lodash';
import { handler as ErrorHandler } from './error';
import Attribute from '../../common/models/attribute.model';

/**
 * Load product attribute add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Attribute.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.attribute = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Attribute.totalRecords(
            req.query
        );
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Load attribute add to req locals.
 */
exports.prepareAttribute = async (req, res, next) => {
    try {
        const data = await Attribute.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.attribute = Attribute.transform(data);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = Attribute.filterParams(req.body);
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);
        req.body = params;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare update
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { attribute: oldModel } = req.locals;
        const params = Attribute.filterParams(
            req.body
        );
        const dataChanged = Attribute.getChangedProperties({
            oldModel,
            newModel: params
        });
        const paramChanged = pick(params, dataChanged);
        paramChanged.updated_by = pick(req.user, ['id', 'name']);
        paramChanged.updated_at = new Date();
        req.body = paramChanged;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
