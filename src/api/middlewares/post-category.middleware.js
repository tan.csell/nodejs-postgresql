import { Op } from 'sequelize';
import { cloneDeep, isNil, pick } from 'lodash';
import httpStatus from 'http-status';
import { handler as ErrorHandel } from './error';
import Category from '../../common/models/post-category.model';
import Post from '../../common/models/post.model';
import APIError from '../../common/utils/APIError';

/**
 * Load category by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        req.locals = {};
        const category = await Category.get(
            req.params.id
        );
        req.locals.category = category;
        return next();
    } catch (ex) {
        return ErrorHandel(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Category.totalRecords(
            req.query
        );
        return next();
    } catch (ex) {
        return ErrorHandel(ex, req, res, next);
    }
};

/**
 * Perpare banner params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = cloneDeep(req.body);
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);
        if (
            params.name &&
            isNil(params.meta_title)
        ) {
            params.meta_title = params.name;
        }
        if (
            params.image &&
            isNil(params.meta_image)
        ) {
            params.meta_image = params.image;
        }
        if (params.name &&
            isNil(params.meta_keyword)
        ) {
            params.meta_keyword = params.name;
        }
        if (
            params.content &&
            isNil(params.meta_description)
        ) {
            params.meta_description = params.content.slice(0, 155);
        }
        // transform body
        req.body = params;
        return next();
    } catch (ex) {
        return ErrorHandel(ex, req, res, next);
    }
};

/**
 * Kiểm tra xem trong các sản phẩm có category này chưa?
 * Nếu sản phẩm có category này thì không được xóa
 */
exports.prepareDelete = async (req, res, next) => {
    try {
        const { category } = req.locals;
        const post = await Post.findOne({
            where: {
                normalize_category: {
                    [Op.contains]: [category.id]
                }
            }
        });
        if (post) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Danh mục này đang có sản phẩm nên bạn không thể xóa'
            });
        }
        return next();
    } catch (ex) {
        return ErrorHandel(ex, req, res, next);
    }
};
