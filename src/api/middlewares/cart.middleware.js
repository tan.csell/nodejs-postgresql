import httpStatus from 'http-status';
import { cloneDeep, findIndex, includes, isNil, omitBy, parseInt, pick } from 'lodash';
import messages from '../../config/messages';
import { handler as ErrorHandler } from './error';
import APIError from '../../common/utils/APIError';
import Cart from '../../common/models/cart.model';
import Order from '../../common/models/order.model';
import Payment from '../../common/models/payment.model';
import cartAdapter from '../../common/services/adapters/cart-adapter';

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.locals = req.locals ? req.locals : {};
        req.locals.totalRecords = await Cart.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};


/**
 * Update Cart
 * @param {*} body
 */
exports.updateCart = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const newOrder = cloneDeep(order);
        // trasnform data
        const { customer, note, source } = req.body;
        if (customer) newOrder.customer = customer;
        if (note) newOrder.note = note;
        if (source) newOrder.source = source;
        req.locals.order = newOrder;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Add Item
 * @param {*} product
 */
exports.addItem = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const { products } = order;
        const params = omitBy(req.body, isNil);

        if (params.id) {
            const indexOf = findIndex(
                products, o => o.option_id === params.option_id
            );
            if (indexOf !== -1) {
                const item = products[indexOf];
                item.total_quantity += params.total_quantity;
                item.total_discount = 0; // Calculate price if has discount
                item.total_before_discount = Math.ceil(item.price * item.total_quantity);
                item.total_price = Math.ceil(item.total_before_discount - item.total_discount);
                products[indexOf] = cloneDeep(item);
            } else {
                products.push({
                    id: params.id,
                    option_id: params.option_id,
                    total_quantity: params.total_quantity
                });
            }
        }

        // trasnform data
        order.products = products;
        req.locals.order = order;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Add Item
 * @param {*} option_id
 * @param {*} total_quantity
 */
exports.updateItem = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const { products } = order;
        const { total_quantity } = req.body;
        const indexOf = findIndex(
            products, o => o.option_id === parseInt(req.params.id, 0)
        );
        if (indexOf !== -1) {
            const item = products[indexOf];
            item.total_quantity = total_quantity;
            products[indexOf] = cloneDeep(item);
        } else {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy sản phẩm: ${req.params.id}`
            });
        }
        // trasnform data
        order.products = products;
        req.locals.order = order;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Remove Item
 * @param {*} id
 */
exports.removeItem = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const { products } = order;
        const indexOf = findIndex(
            products, o => o.option_id === parseInt(req.params.id, 0)
        );
        if (indexOf !== -1) {
            products.splice(indexOf, 1);
        } else {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy sản phẩm: ${req.params.id}`
            });
        }
        const newProducts = products.map(i => {
            const el = cloneDeep(i);
            if (i.campaign) {
                const element = cloneDeep(i.campaign);
                element.hasItemRemove = true;
                el.campaign = element;
            }
            return el;
        });
        // trasnform data
        order.products = newProducts;
        req.locals.order = order;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Add Payment
 * @param {*} payment
 */
exports.addPayment = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const { payments } = order;
        const params = omitBy(req.body, isNil);

        const indexOf = findIndex(
            payments, p => p.method === params.method
        );
        if (indexOf !== -1) {
            const payment = payments[indexOf];
            payment.type = params.type;
            payment.note = params.note;
            payment.card = params.card;
            payment.value = params.value;
            payment.method = params.method;
            payment.voucher = params.voucher;
            payments[indexOf] = cloneDeep(payment);
        } else {
            payments.push({
                type: params.type,
                note: params.note,
                card: params.card,
                value: params.value,
                method: params.method,
                voucher: params.voucher,
            });
        }
        const includeRestrictedPayments = [
            params.method,
            Payment.Methods.POINT,
            Payment.Methods.VOUCHER
        ];
        order.payments = payments.filter(
            payment => includes(includeRestrictedPayments, payment.method)
        );
        req.locals.order = order;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Remove Payment
 * @param {*} payment
 */
exports.removePayment = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const { payments } = order;
        const indexOf = findIndex(
            payments, p => p.method === parseInt(req.params.id, 0)
        );
        if (indexOf !== -1) {
            payments.splice(indexOf, 1);
        }
        order.payments = payments;
        req.locals.order = order;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Add Delivery
 * @param {*} Delivery
 */
exports.addDelivery = async (req, res, next) => {
    try {
        const { order } = req.locals;
        const params = omitBy(req.body, isNil);
        const deliveries = [{
            code: params.code,
            note: params.note,
            weight: params.weight,
            length: params.length,
            height: params.height,
            width: params.width,
            payment_method: 'CASH',
            payment_by: params.payment_by,
            service_id: params.service_id,
            service_name: params.service_name,
            receiver_name: params.receiver_name,
            receiver_phone: params.receiver_phone,
            receiver_note: params.receiver_note,
            receiver_address: params.receiver_address,
            receiver_province: params.receiver_province,
            receiver_district: params.receiver_district,
            receiver_ward: params.receiver_ward,
            receiver_date: params.receiver_date,
            total_shipping_fee: params.total_shipping_fee,
            total_order_price: params.total_order_price,
            total_shipping_cod: params.total_shipping_cod,
            total_shipping_price: params.total_shipping_price,
            total_insurrance_price: params.total_insurrance_price,
        }];
        order.deliveries = deliveries;
        req.locals.order = order;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare cart params
 */
exports.prepareCart = async (req, res, next) => {
    try {
        const params = req.locals.order;
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.client_id = req.headers['x-client-id'];

        if (
            params.device_id &&
            params.device_id.length > 255
        ) {
            params.device_id = 'System/1.0';
        }

        if (params.products && params.products.length) {
            const masterId = params.products.map(
                i => i.id
            );
            const products = params.products.filter(
                (i) => (isNil(i.campaign) || includes(masterId, i.campaign.master_id))
            );
            params.products = await cartAdapter.parseItems(
                products
            );
        }

        if (params.payments && params.payments.length) {
            params.total_coin = params.payments
                .filter(p => p.method === Payment.Methods.POINT)
                .map(p => p.value)
                .reduce((a, b) => a + b, 0);
        }

        if (req.user) {
            params.created_by = pick(req.user, ['id', 'name']);
            params.customer = pick(req.user, ['id', 'name', 'phone', 'address']);
        }

        if (!params.customer && params.deliveries) {
            const { deliveries } = params;
            const delivery = deliveries[0];
            if (delivery && delivery.receiver_name) {
                params.created_by = { id: 0, name: delivery.receiver_name };
                params.customer = { id: 0, name: delivery.receiver_name, phone: delivery.receiver_phone };
            }
        }
        if (params.created_at) {
            params.created_at = new Date();
        }

        // Calculate price
        const returnAmount = await cartAdapter.calTotalPrice(params);
        params.total_price_before_discount = returnAmount.total_price_before_discount;
        params.total_price_after_discount = returnAmount.total_price_after_discount;
        params.total_discount_value = returnAmount.total_discount_value;
        params.total_original_price = returnAmount.total_original_price;
        params.total_shipping_fee = returnAmount.total_shipping_fee;
        params.total_quantity = returnAmount.total_quantity;
        params.total_price = returnAmount.total_price;
        params.total_point = returnAmount.total_point;
        params.total_paid = returnAmount.total_paid;
        params.total_unpaid = returnAmount.total_unpaid;

        // transform body
        req.locals.order = params;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};

/**
 * Perpare cart params
 */
exports.prepareConfirm = async (req, res, next) => {
    try {
        const params = Order.filterParams(req.body);
        const order = req.locals.order;
        params.type = Order.Types.ORDER;
        params.status = Order.Statuses.DRAFT;
        params.status_name = Order.StatusNames.DRAFT;
        // params.price_book = ProductPrice.DefaultValues;
        params.channel = { id: 1, name: 'Bán trực tiếp' };
        params.store = { id: 1, name: 'Online', phone: '0987654321' };

        // Transform items
        if (params.products && params.products.length) {
            const masterId = params.products.map(
                i => i.id
            );
            const products = params.products.filter(
                (i) => (isNil(i.campaign) || includes(masterId, i.campaign.master_id))
            );
            const newProducts = products.map(i => {
                const el = cloneDeep(i);
                if (i.campaign) {
                    const element = cloneDeep(i.campaign);
                    element.hasItemRemove = false;
                    el.campaign = element;
                }
                return el;
            });
            params.products = await cartAdapter.parseItems(
                newProducts
            );
            const productPath = params.products.map(p => `${p.sku}:${p.name}`);
            params.normalize_product = productPath.join(' - ');
        }

        if (req.user) {
            params.created_by = pick(req.user, ['id', 'name']);
            params.customer = pick(req.user, ['id', 'name', 'phone', 'address']);
            params.customer.source = order.source;
        }

        if (!params.customer && params.deliveries) {
            const { deliveries } = params;
            const delivery = deliveries[0];
            if (delivery && delivery.receiver_name) {
                params.created_by = { id: 0, name: delivery.receiver_name };
                params.customer = { id: 0, name: delivery.receiver_name, phone: delivery.receiver_phone };
                params.customer.source = Cart.Sources.OTHER;
            }
        }

        if (params.created_at) {
            params.created_at = new Date();
        }

        // Calculate price
        const returnAmount = await cartAdapter.calTotalPrice(params);
        params.total_price_before_discount = returnAmount.total_price_before_discount;
        params.total_price_after_discount = returnAmount.total_price_after_discount;
        params.total_discount_value = returnAmount.total_discount_value;
        params.total_original_price = returnAmount.total_original_price;
        params.total_shipping_fee = returnAmount.total_shipping_fee;
        params.total_quantity = returnAmount.total_quantity;
        params.total_price = returnAmount.total_price;
        params.total_point = returnAmount.total_point;
        params.total_paid = returnAmount.total_paid;
        params.total_unpaid = returnAmount.total_unpaid;

        // transform body
        req.locals.order = params;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};

/**
 * Reset cart to default
 */
exports.renewStatus = async (req, res, next) => {
    try {
        const clientId = req.headers['x-client-id'];
        if (isNil(clientId)) {
            throw new APIError({
                status: httpStatus.UNAUTHORIZED,
                message: messages.UNAUTHORIZED
            });
        }
        await Cart.update({
            status: Cart.Statuses.CONFIRMED,
            status_name: Cart.NameStatuses.CONFIRMED
        }, {
            where: {
                client_id: clientId,
                status: Cart.Statuses.PICKING
            }
        });
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};

/**
 * Check duplicate clientId.
 */
exports.checkDuplicate = async (req, res, next) => {
    try {
        const clientId = req.headers['x-client-id'];
        if (isNil(clientId)) {
            throw new APIError({
                status: httpStatus.UNAUTHORIZED,
                message: messages.UNAUTHORIZED
            });
        }
        await Cart.checkDuplicate(clientId);
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};

/**
 * Check confirm cart.
 */
exports.checkConfirm = async (req, res, next) => {
    try {
        const order = req.body;
        if (
            order.products &&
            order.products.length === 0
        ) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Giỏ hàng của bạn chưa có sản phẩm nào được chọn'
            });
        }
        if (
            order.deliveries &&
            order.deliveries.length === 0
        ) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Vui lòng chọn thông tin nhận hàng của bạn'
            });
        }
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};

/**
 * Load cart by client_id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const clientId = req.headers['x-client-id'];
        const deviceId = req.headers['user-agent'];

        // validate consumer
        if (isNil(clientId)) {
            throw new APIError({
                status: httpStatus.UNAUTHORIZED,
                message: messages.UNAUTHORIZED
            });
        }

        // load or generate order
        let order = await Cart.get(
            clientId
        );
        if (isNil(order)) {
            order = await Cart.create({
                client_id: clientId,
                device_id: deviceId.length > 255 ?
                    'System/1.0' : deviceId
            });
        }
        // next request with new data
        req.locals = req.locals ? req.locals : {};
        req.locals.order = Cart.transform(order);
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};
