import { pick, cloneDeep, isNil } from 'lodash';
import { handler as ErrorHandler } from './error';
import Collection from '../../common/models/collection.model';


/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const data = await Collection.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.collection = data;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Collection.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Prepare Params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = cloneDeep(req.body);
        if (params.products && params.products.length) {
            const productPath = params.products.map(p => `${p.sku}:${p.name}`);
            params.normalize_product = productPath.join(' - ');
        }
        if (
            params.name &&
            isNil(params.meta_title)
        ) {
            params.meta_title = params.name;
        }
        if (
            params.avatar &&
            isNil(params.meta_image)
        ) {
            params.meta_image = params.avatar;
        }
        if (params.name &&
            isNil(params.meta_keyword)
        ) {
            params.meta_keyword = params.name;
        }
        if (
            params.description &&
            isNil(params.meta_description)
        ) {
            params.meta_description = params.description;
        }
        params.created_by = pick(req.user, ['id', 'name']);
        req.body = params;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare collection update
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { collection: oldModel } = req.locals;
        const params = Collection.filterParams(req.body);
        const dataChanged = Collection.getChangedProperties({ oldModel, newModel: params });
        const paramChanged = pick(params, dataChanged);
        paramChanged.updated_by = pick(req.user, ['id', 'name']);
        paramChanged.updated_at = new Date();
        req.body = paramChanged;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
