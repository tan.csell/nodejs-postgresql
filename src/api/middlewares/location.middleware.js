import { handler as ErrorHandler } from './error';
import Location from '../../common/models/location.model';

/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Location.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.location = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Location.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
