/* eslint-disable no-await-in-loop */
import httpStatus from 'http-status';
import { isNil, omitBy, pick } from 'lodash';
import { ConsumerGroups } from 'auth-adapter';
import { handler as ErrorHandler } from './error';
import APIError from '../../common/utils/APIError';
import Product from '../../common/models/product.model';
import productAdapter from '../../common/services/adapters/product-adapter';
import ProductOption from '../../common/models/product-option.model';

/**
 * Load product and append to req.
 * @public
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Product.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.product = dataValues;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

exports.count = async (req, res, next) => {
    try {
        const count = await Product.totalRecords(req.query);
        req.locals = req.locals ? req.locals : {};
        req.locals.count = count;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

exports.sum = async (req, res, next) => {
    try {
        if (req.query.is_include) {
            const sum = await Product.sumRecords(req.query);
            req.locals = req.locals ? req.locals : {};
            req.locals.sum = sum;
        }
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    const includeRestrictedFields = req.authInfo.accessLevel !== ConsumerGroups.USER;
    params.fields = Product.includeFields(includeRestrictedFields);
    params.is_include = params.is_include || includeRestrictedFields;
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Load params
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { product } = req.locals;
        const params = Product.filterParams(req.body);
        const dataChanged = Product.getChangedProperties({
            oldModel: product,
            newModel: params
        });
        const paramChanged = pick(
            params, dataChanged
        );
        paramChanged.updated_by = pick(
            req.user, ['id', 'name']
        );
        if (params.attributes) {
            paramChanged.attributes = params.attributes.map(a => Product.filterFieldParams('ATTRIBUTE', a));
        }

        const newUnits = [];
        let removeDefault = false;
        if (paramChanged.variations) {
            paramChanged.variations = params.variations.map((v, index) => {
                const productOptions = ProductOption.filterParams(v);
                delete productOptions.providers;
                if (productOptions.sku) {
                    productOptions.sku = productOptions.sku.toLowerCase().trim();
                }
                // case remove product
                if (productOptions.is_active === false && productOptions.is_default === true) {
                    removeDefault = true;
                }
                if (index === 0 && productOptions.is_active === false) {
                    removeDefault = true;
                }
                // case update unit từ chưa có unit
                if (paramChanged.unit && !product.unit) {
                    productOptions.units = paramChanged.units ? paramChanged.units : productOptions.units;
                    productOptions.unit = productOptions.unit ? productOptions.unit : paramChanged.unit;
                    productOptions.master_id = !productOptions.master_id ? productOptions.id : productOptions.master_id;
                }
                productOptions.updated_by = pick(req.user, ['id', 'name']);

                // case đồng bộ unit cho thg cha
                if ((productOptions.is_active !== false)) {
                    if (productOptions.units) {
                        productOptions.units.forEach(item => {
                            const indexOf = newUnits.findIndex(o => o.name === item.name);
                            if (indexOf === -1) {
                                newUnits.push(item);
                            }
                        });
                    }
                }

                // gen lại varation các thg con master_id
                if (v.master_id) {
                    const defaultProduct = params.variations.find(o => o.id === v.master_id);
                    if (defaultProduct && defaultProduct.variations && defaultProduct.variations.length) {
                        productOptions.variations = defaultProduct.variations.map((o, i) => {
                            if (o.name === 'Đơn vị') {
                                productOptions.options[i] = productOptions.unit;

                                return { name: o.name, value: productOptions.unit };
                            }

                            productOptions.options[i] = o.value;
                            return { name: o.name, value: o.value };
                        });
                    }

                    if (productOptions.variations && productOptions.variations.length > 1) {
                        productOptions.option_name = productOptions.variations.filter(o => o.name !== 'Đơn vị').map(o => o.value).join(' - ');
                    }
                    if (productOptions.variations && productOptions.variations.length && productOptions.variations[0].name === 'Đơn vị') {
                        productOptions.option_name = productOptions.variations.map(o => o.value).join(' - ');
                    }
                }
                return productOptions;
            });
        }
        if (newUnits.length > 0) {
            paramChanged.units = newUnits;
        }
        if (paramChanged.categories) {
            paramChanged.categories = paramChanged.categories.map(c => Product.filterFieldParams('CATEGORY', c));
        }
        if (paramChanged.units) {
            paramChanged.units = paramChanged.units.map(u => Product.filterFieldParams('UNIT', u));
        }
        if (paramChanged.parts) {
            paramChanged.parts = await productAdapter.parseParts(paramChanged.parts);
        }
        if (paramChanged.brand) {
            paramChanged.brand = Product.filterFieldParams('BRAND', paramChanged.brand);
        }
        if (paramChanged.name) {
            product.name = req.body.name;
        }
        if (paramChanged.sku) {
            product.sku = req.body.sku;
        }
        req.body = productAdapter.parseData(
            Object.assign(product, paramChanged)
        );
        if (removeDefault) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không thể xoá sản phẩm Default'
            });
        }
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};

/**
 * Check duplicate
 */
exports.checkDuplicate = async (req, res, next) => {
    try {
        const { variations } = req.body;
        const { sku } = req.body;
        const params = req.body;
        const { product: option } = req.locals ? req.locals : {};
        let status = {
            pass: true,
            error: ''
        };


        // Check duplicate
        if (option && option.sku !== sku) {
            await Product.checkDuplicateSku({
                sku: sku,
                id: option ? option.id : null
            });
        }
        if (!option) {
            params.variations = params.variations.map((v, i) => {
                const productOptions = v;

                if (productOptions.sku) {
                    productOptions.sku = productOptions.sku.toLowerCase().trim();
                }
                if (i === 0) {
                    productOptions.sku = params.sku.toLowerCase().trim();
                }
                return productOptions;
            });
        }
        if (variations && variations.length > 0) {
            // eslint-disable-next-line no-restricted-syntax
            for (const [index, product] of variations.entries()) {
                if (option && option.variations && option.variations.length) {
                    if (!option.variations[index] ||
                        (
                            option.variations[index] && product.sku &&
                            option.variations[index].sku.toLowerCase() !== product.sku.toLowerCase()
                        )
                    ) {
                        await ProductOption.checkDuplicateSku({
                            sku: product.sku
                        });
                    }
                }

                // handle duplicate thêm mới
                if (!option) {
                    await ProductOption.checkDuplicateSku({
                        sku: product.sku
                    });
                }

                // handle duplicate trong mảng gửi lên
                if (product) {
                    const findIndex = variations.findIndex((o, i) => (o.sku === product.sku && i !== index));

                    if (findIndex !== -1) {
                        status = {
                            pass: false,
                            error: `Sản phẩm có sku ${product.sku} bị trùng`
                        };
                    }
                }
            }
        }
        // tạo mới
        if (variations.length === 0 && !option) {
            await ProductOption.checkDuplicateSku({
                sku: sku
            });
        }
        if (!status.pass) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: status.error
            });
        }
        req.body = params;
        return next();
    } catch (error) {
        return ErrorHandler(error, req, res);
    }
};
