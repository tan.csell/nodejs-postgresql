import { isNil, omitBy } from 'lodash';
import { handler as ErrorHandler } from './error';
import Promotion from '../../common/models/promotion.model';

/**
 * Load order by id appendd to locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Promotion.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.promotion = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Promotion.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};
