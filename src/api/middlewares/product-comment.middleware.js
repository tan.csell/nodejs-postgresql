import httpStatus from 'http-status';
import { isNil, omitBy, pick } from 'lodash';

import { handler as ErrorHandler } from './error';
import APIError from '../../common/utils/APIError';
import ProductComment from '../../common/models/product-comment.model';

/**
 * Load comment by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        req.locals = {};
        const comment = await ProductComment.get(
            req.params.id
        );
        req.locals.comment = comment;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await ProductComment.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Check parent_id
 */
exports.prepareComment = async (req, res, next) => {
    try {
        const params = req.body;
        params.reply_count = 0;
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);

        if (params.parent_id) {
            const comment = await ProductComment.get(
                params.parent_id
            );
            if (comment.is_reply) {
                throw new APIError({
                    status: httpStatus.BAD_REQUEST,
                    message: 'Bình luận này đã là bình luận trả lời!'
                });
            }
            params.is_reply = true;
            params.product.id = comment.product.id;
        } else {
            params.is_reply = false;
        }
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Check Comment Valid
 */
exports.valid = async (req, res, next) => {
    try {
        const { comment } = req.locals;
        const { user } = req;
        if (
            comment.created_by.id !== user.id &&
            user.service !== 'staff'
        ) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Bạn không có quyền cập nhật hoặc xóa bình luận này!'
            });
        }
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
