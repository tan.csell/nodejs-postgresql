import { isNil } from 'lodash';
import { handler as ErrorHandler } from './error';
import Post from '../../common/models/post.model';

/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const data = await Post.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.post = data;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Post.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Prepare Params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = req.body;
        if (params.categories) {
            params.categories = params.categories.map(c => Post.filterParams(c));
            params.normalize_category = params.categories.map(x => x.id);
        }

        if (
            params.name &&
            isNil(params.meta_title)
        ) {
            params.meta_title = params.name;
        }
        if (
            params.avatar &&
            isNil(params.meta_image)
        ) {
            params.meta_image = params.avatar;
        }
        if (params.name &&
            isNil(params.meta_keyword)
        ) {
            params.meta_keyword = params.name;
        }
        if (
            params.description &&
            isNil(params.meta_description)
        ) {
            params.meta_description = params.description;
        }
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
