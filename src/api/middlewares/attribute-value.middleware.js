import { isNil, omitBy, pick } from 'lodash';
import { handler as ErrorHandler } from './error';
import Redis from '../../config/redis';
import Attribute from '../../common/models/attribute.model';
import AttributeValue from '../../common/models/attribute-value.model';

/**
 * Load product attribute add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await AttributeValue.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.attributeValue = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load cache attribute add to req locals.
 */
exports.loadCache = async (req, res, next) => {
    try {
        const day = new Date().getDay();
        req.locals = req.locals ? req.locals : {};
        const cache = await Redis.client.get(req.originalUrl);
        const data = cache ? JSON.parse(cache) : null;
        if (data && data.expired !== day) {
            await Redis.client.del(req.originalUrl);
            data.value = null;
        }
        req.locals.includeCacheFields = {
            key: req.originalUrl,
            count: data ? data.count : 0,
            data: data ? data.value : null
        };
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        // const { includeCacheFields } = req.locals;
        // if (includeCacheFields && includeCacheFields.data) {
        //     req.totalRecords = includeCacheFields.count;
        //     return next();
        // }

        // Calculate count
        req.totalRecords = await AttributeValue.totalRecords(
            req.query
        );
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Load attribute add to req locals.
 */
exports.prepareAttribute = async (req, res, next) => {
    try {
        const data = await Attribute.findOne({
            where: {
                code: req.body.attribute_code,
                is_active: true
            }
        });
        if (data) {
            req.body.attribute_group = data.group;
            req.body.attribute_code = data.code;
            req.body.attribute_id = data.id;
        }
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = AttributeValue.filterParams(req.body);
        params.device_id = req.headers['user-agent'];
        // params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);
        req.body = params;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare update
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { attributeValue: oldModel } = req.locals;
        const params = AttributeValue.filterParams(
            req.body
        );
        const dataChanged = AttributeValue.getChangedProperties({
            oldModel,
            newModel: params
        });
        const paramChanged = pick(params, dataChanged);
        paramChanged.updated_by = pick(req.user, ['id', 'name']);
        paramChanged.updated_at = new Date();
        req.body = paramChanged;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

