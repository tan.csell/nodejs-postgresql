import { isNil, omitBy } from 'lodash';
import { handler as ErrorHandler } from './error';
import Voucher from '../../common/models/voucher.model';
/**
 * Load voucher by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Voucher.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.voucher = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Voucher.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};
