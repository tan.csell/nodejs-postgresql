import httpStatus from 'http-status';
import { isNil, omitBy, pick } from 'lodash';
import { handler as ErrorHandler } from './error';
import APIError from '../../common/utils/APIError';
import Banner from '../../common/models/banner.model';


/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Banner.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.banner = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Banner.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Perpare banner params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = Banner.filterParams(req.body);
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);
        // transform body
        req.body = params;

        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare banner update
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { banner: oldModel } = req.locals;
        const params = Banner.filterParams(req.body);

        const dataChanged = Banner.getChangedProperties({
            oldModel,
            newModel: params
        });
        // transform body
        if (dataChanged.length === 0) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Bạn chưa thay đổi gì để cập nhật!'
            });
        }
        const paramChanged = pick(params, dataChanged);
        paramChanged.updated_at = new Date();
        req.body = paramChanged;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
