import { pick } from 'lodash';
import { handler as ErrorHandler } from './error';

import Author from '../../common/models/author.model';
import AuthorAdapter from '../../common/services/adapters/author.adapter';


/**
 * Load store by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        const data = await Author.get(req.params.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.author = data;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Author.totalRecords(req.query);
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Prepare Params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = await AuthorAdapter.transformItem(req.body);
        params.created_by = pick(req.user, ['id', 'name']);
        req.body = params;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Perpare Author update
 */
exports.prepareUpdate = async (req, res, next) => {
    try {
        const { author: oldModel } = req.locals;
        const params = Author.filterParams(req.body);
        const dataChanged = Author.getChangedProperties({ oldModel, newModel: params });
        const paramChanged = pick(params, dataChanged);
        paramChanged.updated_by = pick(req.user, ['id', 'name']);
        paramChanged.updated_at = new Date();
        req.body = paramChanged;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
