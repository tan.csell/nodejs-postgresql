import { ConsumerGroups } from 'auth-adapter';
import { isNil, omitBy } from 'lodash';
import { handler as ErrorHandler } from './error';
import Order from '../../common/models/order.model';
/**
 * Load order by id appendd to locals.
 */
exports.load = async (req, res, next) => {
    try {
        const { dataValues } = await Order.get(req.params.id || req.body.id);
        req.locals = req.locals ? req.locals : {};
        req.locals.order = dataValues;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load total order appendd to locals.
 */
exports.count = async (req, res, next) => {
    try {
        const totalRecords = await Order.totalRecords(req.query);
        req.totalRecords = totalRecords;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load sum for filter.
 */
exports.sum = async (req, res, next) => {
    try {
        req.locals = req.locals ? req.locals : {};
        if (req.authInfo.accessLevel === ConsumerGroups.STAFF) {
            const sum = await Order.sumRecords(req.query);
            req.locals.sum = sum;
        }
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Pick params append to req
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    const includeRestrictedFields = req.authInfo.accessLevel !== ConsumerGroups.USER;
    params.user_code = includeRestrictedFields ? params.user_code : `${req.user.id}`;
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};
