import { Op } from 'sequelize';
import httpStatus from 'http-status';
import { isNil, omitBy, pick } from 'lodash';
import { handler as ErrorHandler } from './error';
import APIError from '../../common/utils/APIError';
import Category from '../../common/models/category.model';
import Product from '../../common/models/product.model';

/**
 * Load category by id add to req locals.
 */
exports.load = async (req, res, next) => {
    try {
        req.locals = {};
        const category = await Category.get(
            req.params.id
        );
        req.locals.category = category;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Load count for filter.
 */
exports.count = async (req, res, next) => {
    try {
        req.totalRecords = await Category.totalRecords(
            req.query
        );
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Check delete category
 */
exports.checkCate = async (req, res, next) => {
    try {
        const { category } = req.locals;
        const itemExist = await Product.findOne({
            where: {
                normalize_categories: {
                    [Op.overlap]: [category.id]
                }
            }
        });
        if (itemExist) {
            throw new APIError({
                status: httpStatus.BAD_REQUEST,
                message: 'Không thể xóa danh mục đang có sản phẩm!'
            });
        }
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Filter Query append to req query
 */
exports.filterQuery = (req, res, next) => {
    const params = omitBy(req.query, isNil);
    params.system_id = req.headers['x-system-id'];
    req.query = params;
    next();
};

/**
 * Perpare banner params
 */
exports.prepareParams = async (req, res, next) => {
    try {
        const params = Category.filterParams(req.body);
        params.device_id = req.headers['user-agent'];
        params.system_id = req.headers['x-system-id'];
        params.created_by = pick(req.user, ['id', 'name']);
        if (
            params.name &&
            isNil(params.meta_title)
        ) {
            params.meta_title = params.name;
        }
        if (
            params.image &&
            isNil(params.meta_image)
        ) {
            params.meta_image = params.image;
        }
        if (params.name &&
            isNil(params.meta_keyword)
        ) {
            params.meta_keyword = params.name;
        }
        if (
            params.content &&
            isNil(params.meta_description)
        ) {
            params.meta_description = params.content.slice(0, 155);
        }
        // transform body
        req.body = params;
        return next();
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
