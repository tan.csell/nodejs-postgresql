import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/collection.controller';
import middleware from '../../middlewares/collection.middleware';

import * as validation from '../../validations/collection.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(validation.listValidation),
        middleware.count,
        controller.list
    )
    .post(
        validate(validation.createValidation),
        authorize([Permissions.LOGGED_IN]),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        validate(validation.detailValidation),
        middleware.load,
        controller.detail
    )
    .put(
        validate(validation.updateValidation),
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        middleware.prepareUpdate,
        controller.update
    )
    .delete(
        validate(validation.deleteValidation),
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        controller.delete
    );
export default router;

