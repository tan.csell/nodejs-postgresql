import express from 'express';
import postRoutes from './post.route';
import postCategoryRoutes from './post-category.route';
import collectionRoutes from './collection.route';
import authorRoutes from './author.route';
import cartRoutes from './cart.route';
import orderRoutes from './order.route';
import promotionRoutes from './promotion.route';
import productRoutes from './product.route';
import productCommentRoutes from './product-comment.route';
import productOptionRoutes from './product-option.route';
import storeRouters from './store.route';
import attributeRoutes from './attribute.route';
import attributeValueRoutes from './attribute-value.route';
import categoryRoutes from './category.route';
import locationRoutes from './location.route';
import bannerRoutes from './banner.route';
import voucherRoutes from './voucher.route';

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));
router.get('/version/:service', (req, res) =>
    res.send(process.env.GIT_COMMIT_TAG || 'Not available')
);

router.use('/posts', postRoutes);
router.use('/post-categories', postCategoryRoutes);
router.use('/collections', collectionRoutes);
router.use('/authors', authorRoutes);
router.use('/carts', cartRoutes);
router.use('/orders', orderRoutes);
router.use('/vouchers', voucherRoutes);
router.use('/promotions', promotionRoutes);
router.use('/products', productRoutes);
router.use('/product-comments', productCommentRoutes);
router.use('/product-options', productOptionRoutes);
router.use('/stores', storeRouters);
router.use('/banners', bannerRoutes);
router.use('/locations', locationRoutes);
router.use('/categories', categoryRoutes);
router.use('/attributes', attributeRoutes);
router.use('/attribute-values', attributeValueRoutes);

export default router;
