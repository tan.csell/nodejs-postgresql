import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import controller from '../../controllers/v1/order.controller';
import middleware from '../../middlewares/order.middleware';
import Permissions from '../../../common/utils/Permissions';

import {
    listOrder
} from '../../validations/order.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listOrder),
        authorize([Permissions.USER, Permissions.ORDER_VIEW]),
        middleware.filterQuery,
        middleware.count,
        middleware.sum,
        controller.list
    );

router
    .route('/:id')
    .get(
        authorize([]),
        middleware.load,
        controller.detail
    );

export default router;
