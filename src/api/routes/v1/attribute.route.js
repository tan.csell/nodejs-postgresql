import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/attribute.controller';
import middleware from '../../middlewares/attribute.middleware';

import {
    listAttribute,
    createAttribute,
    updateAttribute
} from '../../validations/attribute.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listAttribute),
        middleware.filterQuery,
        middleware.count,
        controller.list
    )
    .post(
        validate(createAttribute),
        authorize([Permissions.LOGGED_IN]),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.detail
    )
    .put(
        validate(updateAttribute),
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        middleware.prepareUpdate,
        controller.update
    )
    .delete(
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        controller.delete
    );


export default router;
