import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import controller from '../../controllers/v1/voucher.controller';
import middleware from '../../middlewares/voucher.middleware';
import {
    listVoucher,
} from '../../validations/voucher.validation';

const router = express.Router();

router
    .route('/')
    .get(
        authorize([]),
        validate(listVoucher),
        middleware.filterQuery,
        middleware.count,
        controller.list
    );

router
    .route('/:id')
    .get(
        authorize([]),
        middleware.load,
        controller.detail
    );

export default router;
