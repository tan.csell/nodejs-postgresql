import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/cart.controller';
import middleware from '../../middlewares/cart.middleware';
import {
    listCart,
    createCart,
    updateCart,
    confirmCart,
    addItem,
    updateItem,
    removeItem,
    addPayment,
    addDelivery
} from '../../validations/cart.validation';

const router = express.Router();

router
    .route('/list')
    .get(
        authorize([Permissions.LOGGED_IN]),
        validate(listCart),
        middleware.count,
        controller.list
    );

router
    .route('/')
    .get(
        authorize([]),
        middleware.load,
        controller.detail
    )
    .post(
        authorize([]),
        validate(createCart),
        middleware.checkDuplicate,
        controller.create
    )
    .put(
        validate(updateCart),
        authorize([]),
        middleware.load,
        middleware.updateCart,
        middleware.prepareCart,
        controller.update
    );

router
    .route('/items')
    .post(
        validate(addItem),
        authorize([]),
        middleware.load,
        middleware.addItem,
        middleware.prepareCart,
        controller.update
    );

router
    .route('/items/:id')
    .put(
        validate(updateItem),
        authorize([]),
        middleware.load,
        middleware.updateItem,
        middleware.prepareCart,
        controller.update
    )
    .delete(
        validate(removeItem),
        authorize([]),
        middleware.load,
        middleware.removeItem,
        middleware.prepareCart,
        controller.update
    );

router
    .route('/payments')
    .post(
        validate(addPayment),
        authorize([]),
        middleware.load,
        middleware.addPayment,
        middleware.prepareCart,
        controller.update
    );

router
    .route('/payments/:id')
    .post(
        authorize([]),
        middleware.load,
        middleware.removePayment,
        middleware.prepareCart,
        controller.update
    );

router
    .route('/deliveries')
    .post(
        validate(addDelivery),
        authorize([]),
        middleware.load,
        middleware.addDelivery,
        middleware.prepareCart,
        controller.update
    );

router
    .route('/confirm')
    .post(
        validate(confirmCart),
        authorize([]),
        middleware.load,
        middleware.checkConfirm,
        middleware.prepareConfirm,
        middleware.renewStatus,
        controller.confirm
    );

export default router;
