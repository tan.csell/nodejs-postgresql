import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/post-category.controller';
import middleware from '../../middlewares/post-category.middleware';
import {
    listValidation,
    createValidation,
    updateValidation
} from '../../validations/post-category.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listValidation),
        middleware.count,
        controller.list
    )
    .post(
        validate(createValidation),
        authorize([Permissions.POST_CATEGORY_CREATE]),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.detail
    )
    .put(
        validate(updateValidation),
        authorize([Permissions.POST_CATEGORY_UPDATE]),
        middleware.load,
        controller.update
    )
    .delete(
        authorize([Permissions.POST_CATEGORY_DELETE]),
        middleware.load,
        middleware.prepareDelete,
        controller.delete
    );

export default router;
