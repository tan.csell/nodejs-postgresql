import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/product-comment.controller';
import middleware from '../../middlewares/product-comment.middleware';
import {
    listValidation,
    createValidation,
    updateValidation
} from '../../validations/product-comment.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listValidation),
        middleware.filterQuery,
        middleware.count,
        controller.list
    )
    .post(
        authorize([Permissions.LOGGED_IN, Permissions.USER]),
        validate(createValidation),
        middleware.prepareComment,
        controller.create
    );

router
    .route('/:id')
    .get(
        authorize([Permissions.LOGGED_IN, Permissions.USER]),
        middleware.load,
        controller.detail
    )
    .put(
        authorize([Permissions.LOGGED_IN, Permissions.USER]),
        validate(updateValidation),
        middleware.load,
        middleware.valid,
        controller.update
    )
    .delete(
        authorize([Permissions.LOGGED_IN, Permissions.USER]),
        middleware.load,
        middleware.valid,
        controller.delete
    );

export default router;
