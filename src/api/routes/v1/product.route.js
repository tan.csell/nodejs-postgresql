import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import middleware from '../../middlewares/product.middleware';
import controller from '../../controllers/v1/product.controller';
import { listProduct, updateProduct } from '../../validations/product.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listProduct),
        middleware.filterQuery,
        middleware.sum,
        middleware.count,
        controller.list
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.get
    )
    .put(
        validate(updateProduct),
        authorize([Permissions.PRODUCT_UPDATE]),
        middleware.load,
        middleware.checkDuplicate,
        middleware.prepareUpdate,
        controller.update
    );

module.exports = router;
