import express from 'express';
import validate from 'express-validation';
import controller from '../../controllers/v1/store.controller';
import middleware from '../../middlewares/store.middleware';

import {
    listValidation,
} from '../../validations/store.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listValidation),
        // authorize([Permissions.LOGGED_IN]),
        middleware.filterQuery,
        middleware.count,
        controller.list
    );

export default router;
