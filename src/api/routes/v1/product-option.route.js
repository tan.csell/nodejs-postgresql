import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';

import Permissions from '../../../common/utils/Permissions';
import middleware from '../../middlewares/product-option.middleware';
import controller from '../../controllers/v1/product-option.controller';
import {
    listProduct,
    updateProduct,
} from '../../validations/product-option.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listProduct),
        middleware.filterQuery,
        middleware.count,
        middleware.all,
        controller.list
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.get
    )
    .put(
        validate(updateProduct),
        authorize([Permissions.PRODUCT_UPDATE]),
        middleware.load,
        middleware.prepareUpdate,
        middleware.checkDuplicate,
        controller.update
    );

module.exports = router;
