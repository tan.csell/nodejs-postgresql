import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import controller from '../../controllers/v1/location.controller';
import middleware from '../../middlewares/location.middleware';
import Permissions from '../../../common/utils/Permissions';
import {
    listValidation,
} from '../../validations/location.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listValidation),
        authorize([Permissions.LOGGED_IN]),
        middleware.count,
        controller.list
    );

router
    .route('/:id')
    .get(
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        controller.detail
    );

export default router;
