import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import controller from '../../controllers/v1/promotion.controller';
import middleware from '../../middlewares/promotion.middleware';

import {
    listPromotion,
} from '../../validations/promotion.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listPromotion),
        authorize([]),
        middleware.filterQuery,
        middleware.count,
        controller.list
    );

router
    .route('/:id')
    .get(
        authorize([]),
        middleware.load,
        controller.detail
    );

export default router;
