import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/attribute-value.controller';
import middleware from '../../middlewares/attribute-value.middleware';

import {
    listAttributeValue,
    createAttributeValue,
    updateAttributeValue
} from '../../validations/attribute-value.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listAttributeValue),
        middleware.filterQuery,
        // middleware.loadCache,
        middleware.count,
        controller.list
    )
    .post(
        validate(createAttributeValue),
        authorize([Permissions.LOGGED_IN]),
        middleware.prepareAttribute,
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.detail
    )
    .put(
        validate(updateAttributeValue),
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        middleware.prepareUpdate,
        controller.update
    )
    .delete(
        authorize([Permissions.LOGGED_IN]),
        middleware.load,
        controller.delete
    );


export default router;
