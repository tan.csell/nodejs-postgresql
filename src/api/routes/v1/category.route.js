import express from 'express';
import validate from 'express-validation';
import { authorize } from 'auth-adapter';
import Permissions from '../../../common/utils/Permissions';
import controller from '../../controllers/v1/category.controller';
import middleware from '../../middlewares/category.middleware';

import {
    listCategory,
    createCategory,
    updateCategory
} from '../../validations/category.validation';

const router = express.Router();

router
    .route('/')
    .get(
        validate(listCategory),
        middleware.filterQuery,
        middleware.count,
        controller.list
    )
    .post(
        validate(createCategory),
        authorize([Permissions.CATEGORY_CREATE]),
        middleware.prepareParams,
        controller.create
    );

router
    .route('/:id')
    .get(
        middleware.load,
        controller.detail
    )
    .put(
        validate(updateCategory),
        authorize([Permissions.CATEGORY_UPDATE]),
        middleware.load,
        controller.update
    )
    .delete(
        authorize([Permissions.CATEGORY_DELETE]),
        middleware.load,
        controller.delete
    );

export default router;
