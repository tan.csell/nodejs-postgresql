import { Op } from 'sequelize';
import { pick } from 'lodash';
import { handler as ErrorHandler } from '../../middlewares/error';
import ProductOption from '../../../common/models/product-option.model';

/**
 * List
 *
 * @public
 * @param {*} query
 * @returns {Promise<ProductOption, APIError>}
 */
exports.list = async (req, res, next) => {
    ProductOption.list(
        req.query
    ).then(data => {
        res.json({
            all: req.all,
            code: 0,
            count: req.locals.count,
            data: data.map(i => ProductOption.transform(i))
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {*} id
 * @returns {Promise<ProductOption, APIError>}
 */
exports.get = (req, res) => res.json({ code: 0, data: ProductOption.transform(req.locals.productOption) });

/**
 * Create
 *
 * @public
 * @param {*} body
 * @returns {Promise<ProductOption, APIError>}
 */
exports.create = (req, res, next) => {
    ProductOption.create(
        req.body,
        req.body
    ).then((data) => {
        res.json({
            code: 0,
            message: 'Thêm mới sản phẩm thành công!',
            data: ProductOption.transform(data)
        });
    }).catch(error => {
        next(ProductOption.checkDuplicateSku(error));
    });
};

/**
 * Update
 *
 * @public
 * @param {*} body
 * @returns {Promise<ProductOption>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { productOption } = req.locals;
    return ProductOption.update(
        req.body, {
            where: { id: productOption.id },
            individualHooks: true,
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thông tin sản phẩm thành công!'
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param {*} id
 * @returns {Promise<, APIError}
 */
exports.delete = async (req, res, next) => {
    const { productOption } = req.locals;
    return ProductOption.update({
        sku: Date.now(),
        is_active: false,
        updated_at: new Date(),
        updated_by: pick(req.user, ['id', 'name'])
    }, {
        where: {
            [Op.or]: {
                id: productOption.id,
                master_id: productOption.id
            },
            is_active: true,

        },
        individualHooks: true
    }).then(() => {
        res.json({
            code: 0,
            message: 'Xoá sản phẩm thành công!'
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};
