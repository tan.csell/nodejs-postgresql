import { pick } from 'lodash';
import messages from '../../../config/messages';
import { handler as ErrorHandler } from '../../middlewares/error';
import AttributeValue from '../../../common/models/attribute-value.model';

/**
 * Create
 *
 * @public
 * @param body as AttributeValue
 * @returns {Promise<AttributeValue>, APIError>}
 */
exports.create = async (req, res, next) => {
    await AttributeValue.create(
        req.body
    ).then(data => {
        res.json({
            code: 0,
            message: messages.CREATE_SUCCESS,
            data: AttributeValue.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<AttributeValue[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    AttributeValue.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(a => AttributeValue.transform(a))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<AttributeValue>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: AttributeValue.transform(req.locals.attributeValue) });

/**
 * Update
 *
 * @public
 * @param {String} id
 * @param {AttributeValue} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { attributeValue } = req.locals;
    return AttributeValue.update(
        req.body, {
            where: {
                id: attributeValue.id,
                is_active: true
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.UPDATE_SUCCESS
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param {*} id
 * @returns {Promise<>, APIError}
 */
exports.delete = async (req, res, next) => {
    const { attributeValue } = req.locals;
    return AttributeValue.update({
        is_active: false,
        updated_at: new Date()
    }, {
        where: {
            id: attributeValue.id,
            is_active: true
        },
        individualHooks: true,
        user: pick(req.user, ['id', 'name'])
    }).then(() => {
        res.json({
            code: 0,
            message: messages.REMOVE_SUCCESS
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};
