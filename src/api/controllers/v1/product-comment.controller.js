import { pick } from 'lodash';
import messages from '../../../config/messages';
import { handler as ErrorHandler } from '../../middlewares/error';
import ProductComment from '../../../common/models/product-comment.model';

/**
 * Create
 *
 * @public
 * @param {ProductComment} body
 * @returns {Promise<ProductComment, APIError>}
 */
exports.create = async (req, res, next) => {
    req.body.created_by = pick(
        req.user, ['id', 'name']
    );
    await ProductComment.create(
        req.body
    ).then((data) => {
        res.json({
            code: 0,
            message: messages.CREATE_SUCCESS,
            data: ProductComment.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param {String} id
 * @returns {Promise<ProductComment, APIError>}
 */
exports.list = async (req, res, next) => {
    ProductComment.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(
                x => ProductComment.transform(x)
            )
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<ProductComment, APIError>}
 */
exports.detail = (req, res) => res.json({ code: 0, data: ProductComment.transform(req.locals.comment) });

/**
 * Update
 *
 * @public
 * @param {ProductComment} body
 * @returns {Promise<JSON, APIError>}
 */
exports.update = async (req, res, next) => {
    const { comment: oldModel } = req.locals;
    const dataChanged = ProductComment.getChangedProperties({
        oldModel: oldModel,
        newModel: req.body
    });

    // replace existing comment
    const paramChanged = pick(req.body, dataChanged);
    paramChanged.updated_at = new Date();
    return ProductComment.update(
        paramChanged,
        {
            where: {
                id: oldModel.id
            },
            user: pick(req.user, ['id', 'name']),
            individualHooks: true
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.UPDATE_SUCCESS
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param {String} id
 * @returns {Promise<null, APIError>}
 */
exports.delete = async (req, res, next) => {
    try {
        const { comment } = req.locals;
        await ProductComment.destroy({
            where: {
                id: comment.id
            },
            user: pick(req.user, ['id', 'name']),
            individualHooks: true
        });
        return res.json({
            code: 0,
            message: messages.REMOVE_SUCCESS
        });
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

