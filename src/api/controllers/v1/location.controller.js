import { handler as ErrorHandler } from '../../middlewares/error';
import Location from '../../../common/models/location.model';

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Location[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Location.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(s => Location.transform(s))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Location>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Location.transform(req.locals.location) });
