import { pick } from 'lodash';
import messages from '../../../config/messages';
import { handler as ErrorHandler } from '../../middlewares/error';
import Attribute from '../../../common/models/attribute.model';

/**
 * Create
 *
 * @public
 * @param body as Attribute
 * @returns {Promise<Attribute>, APIError>}
 */
exports.create = async (req, res, next) => {
    await Attribute.create(
        req.body
    ).then(data => {
        res.json({
            code: 0,
            message: messages.CREATE_SUCCESS,
            data: Attribute.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Attribute[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Attribute.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(a => Attribute.transform(a))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Attribute>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Attribute.transform(req.locals.attribute) });

/**
 * Update
 *
 * @public
 * @param {String} id
 * @param {Attribute} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { attribute } = req.locals;
    return Attribute.update(
        req.body, {
            where: {
                id: attribute.id,
                is_active: true
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.UPDATE_SUCCESS
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param {*} id
 * @returns {Promise<>, APIError}
 */
exports.delete = async (req, res, next) => {
    const { attribute } = req.locals;
    return Attribute.update({
        is_active: false,
        updated_at: new Date()
    }, {
        where: {
            id: attribute.id,
            is_active: true
        },
        individualHooks: true,
        user: pick(req.user, ['id', 'name'])
    }).then(() => {
        res.json({
            code: 0,
            message: messages.REMOVE_SUCCESS
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};
