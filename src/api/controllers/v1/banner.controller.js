import { pick } from 'lodash';
import messages from '../../../config/messages';
import { handler as ErrorHandler } from '../../middlewares/error';
import Banner from '../../../common/models/banner.model';

/**
 * Create
 *
 * @public
 * @param body as Banner
 * @returns {Promise<Banner>, APIError>}
 */
exports.create = async (req, res, next) => {
    await Banner.create(
        req.body
    ).then(data => {
        res.json({
            code: 0,
            message: messages.CREATE_SUCCESS,
            data: Banner.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Banner[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Banner.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(s => Banner.transform(s))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Banner>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Banner.transform(req.locals.banner) });

/**
 * Update
 *
 * @public
 * @param {String} id
 * @param {Banner} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { banner: data } = req.locals;
    // replace existing data
    return Banner.update(
        req.body, {
            where: {
                id: data.id
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.UPDATE_SUCCESS
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param {*} id
 * @returns {Promise<, APIError}
 */
exports.delete = async (req, res, next) => {
    const { banner: data } = req.locals;
    return Banner.update({
        is_active: false,
        updated_at: new Date()
    }, {
        where: {
            id: data.id,
            is_active: true
        },
        individualHooks: true,
        user: pick(req.user, ['id', 'name'])
    }).then(() => {
        res.json({
            code: 0,
            message: messages.REMOVE_SUCCESS
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};
