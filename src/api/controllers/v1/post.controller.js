import { pick } from 'lodash';
import { handler as ErrorHandler } from '../../middlewares/error';
import Post from '../../../common/models/post.model';

/**
 * Create
 *
 * @public
 * @param body as Post
 * @returns {Promise<Post>, APIError>}
 */
exports.create = async (req, res, next) => {
    // transform data
    req.body.created_by = pick(req.user, ['id', 'name']);

    // save data
    await Post.create(req.body)
        .then(data => {
            res.json({
                code: 0,
                message: 'Thêm mới thành công',
                data: Post.transform(data)
            });
        }).catch(ex => {
            ErrorHandler(ex, req, res, next);
        });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Post[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Post.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(
                x => Post.transform(x)
            )
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Post>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Post.transform(req.locals.post) });

/**
 * Update
 *
 * @public
 * @permission post_UPDATE
 * @param {String} id
 * @param {Post} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const {
        post
    } = req.locals;
    const dataChanged = Post.getChangedProperties({
        oldModel: post,
        newModel: req.body
    });
    const params = pick(req.body, dataChanged);
    params.updated_at = new Date();

    /** replace existing data */
    return Post.update(
        params, {
            where: {
                id: post.id
            },
            user: req.user,
            individualHooks: true
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thành công'
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Remove
 *
 * @public
 * @param {String} id
 * @returns {Promise<>, APIException>}
 */
exports.delete = (req, res, next) => {
    const { post } = req.locals;
    return Post.destroy({
        where: {
            id: post.id
        },
        user: req.user,
        individualHooks: true
    }).then(() => {
        res.json({
            code: 0,
            message: 'Xoá bài viết thành công'
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};
