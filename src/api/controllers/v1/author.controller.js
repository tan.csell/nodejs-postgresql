import { handler as ErrorHandler } from '../../middlewares/error';
import Author from '../../../common/models/author.model';

/**
 * Create
 *
 * @public
 * @param body as Author
 * @returns {Promise<Author>, APIError>}
 */
exports.create = async (req, res, next) => {
    // save data
    await Author.create(req.body)
        .then(data => {
            res.json({
                code: 0,
                message: 'Thêm mới thành công',
                data: Author.transform(data)
            });
        }).catch(error => {
            next(Author.checkDuplicate(error));
        });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Author[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Author.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(
                x => Author.transform(x)
            )
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Author>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Author.transform(req.locals.author) });

/**
 * Update
 *
 * @public
 * @permission Author_UPDATE
 * @param {String} id
 * @param {Author} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { author } = req.locals;
    return Author.update(
        req.body, {
            where: {
                id: author.id
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thành công'
        });
    }).catch(error => {
        next(Author.checkDuplicate(error));
    });
};

/**
 * Remove
 *
 * @public
 * @param {String} id
 * @returns {Promise<>, APIException>}
 */
exports.delete = (req, res, next) => {
    const { author } = req.locals;
    return Author.destroy({
        where: {
            id: author.id
        },
        user: req.user,
    }).then(() => {
        res.json({
            code: 0,
            message: 'Xoá trang thành công'
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};
