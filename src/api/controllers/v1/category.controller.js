import { pick, defaults } from 'lodash';
import { handler as ErrorHandler } from '../../middlewares/error';
import Category from '../../../common/models/category.model';

/**
 * Convert category array to nested category
 * @private
 */
function convertToNestedCategory(arr, rootParent = null) {
    const nodes = {};
    return arr.filter((obj) => {
        const currentId = obj.id;
        let { parent_id } = obj;
        const isRootParent = currentId === rootParent;

        nodes[currentId] = defaults(obj, nodes[currentId], {
            children: []
        });
        parent_id =
            parent_id &&
            (nodes[parent_id] = nodes[parent_id] || {
                children: []
            }).children.push(obj);

        return !parent_id || isRootParent;
    });
}

/**
 * Create
 *
 * @public
 * @param {Category} body
 * @returns {Promise<Category, APIError>}
 */
exports.create = (req, res, next) => {
    req.body.created_by = pick(
        req.user, ['id', 'name']
    );
    Category.create(
        req.body
    ).then((data) => {
        res.json({
            code: 0,
            message: 'Thêm mới thành công',
            data: Category.transform(data)
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param {String} id
 * @returns {Promise<Category[], APIError>}
 */
exports.list = async (req, res, next) => {
    try {
        let categories = await Category.list(req.query);

        // transform nested category
        const { nested, slug = null } = req.query;
        let transformedCategories = categories.map((category) =>
            Category.transform(category)
        );
        if (nested) {
            if (slug && categories.length === 1) {
                categories = await Category.list({
                    nested,
                    id: categories[0].id
                });
            }
            transformedCategories = convertToNestedCategory(
                categories.map((category) => Category.transform(category)),
                null
            );
        }

        return res.json({
            data: transformedCategories
        });
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Category, APIError>}
 */
exports.detail = (req, res) => res.json({ code: 0, data: Category.transform(req.locals.category) });

/**
 * Update
 *
 * @public
 * @param {Category} body
 * @returns {Promise<JSON, APIError>}
 */
exports.update = async (req, res, next) => {
    const { category: oldModel } = req.locals;
    const dataChanged = Category.getChangedProperties({
        oldModel: oldModel,
        newModel: req.body
    });

    /**  replace existing category */
    const operations = pick(req.body, dataChanged);
    operations.updated_at = new Date();
    return Category.update(
        operations, {
            where: {
                id: oldModel.id
            },
            params: {
                updated_by: pick(
                    req.user, ['id', 'name']
                )
            },
            individualHooks: true
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thành công'
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param  {String} id
 * @returns {Promise<null, APIError}
 */
exports.delete = async (req, res, next) => {
    try {
        const { category } = req.locals;
        await Category.destroy({
            where: {
                id: category.id
            },
            params: {
                updated_by: req.user
            },
            individualHooks: true
        });
        return res.json({
            code: 0,
            message: 'Xoá danh mục thành công'
        });
    } catch (error) {
        return ErrorHandler(error, req, res, next);
    }
};
