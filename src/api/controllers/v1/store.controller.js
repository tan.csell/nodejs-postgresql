import { handler as ErrorHandler } from '../../middlewares/error';
import Store from '../../../common/models/store.model';

/**
 * List
 *
 * @public
 * @param {String}
 * @returns {Promise<Supplier, APIError>}
 */
exports.list = async (req, res, next) => {
    Store.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(x => Store.transform(x))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};
