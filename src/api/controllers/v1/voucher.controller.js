import { handler as ErrorHandler } from '../../middlewares/error';
import Voucher from '../../../common/models/voucher.model';

/**
 * List
 *
 * @public
 * @param {String} id
 * @returns {Promise<Voucher, APIError>}
 */
exports.list = async (req, res, next) => {
    Voucher.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(
                x => Voucher.transform(x)
            )
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Voucher, APIError>}
 */
exports.detail = (req, res) => res.json({ code: 0, data: Voucher.transform(req.locals.voucher) });
