import { handler as ErrorHandler } from '../../middlewares/error';
import Collection from '../../../common/models/collection.model';

/**
 * Create
 *
 * @public
 * @param body as collection
 * @returns {Promise<Collection>, APIError>}
 */
exports.create = async (req, res, next) => {
    // save data
    await Collection.create(req.body)
        .then(data => {
            res.json({
                code: 0,
                message: 'Thêm mới thành công',
                data: Collection.transform(data)
            });
        }).catch(ex => {
            ErrorHandler(ex, req, res, next);
        });
};

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<collection[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Collection.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(
                x => Collection.transform(x)
            )
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Collection>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Collection.transform(req.locals.collection) });

/**
 * Update
 *
 * @public
 * @permission collection_UPDATE
 * @param {String} id
 * @param {Collection} body
 * @returns {Promise<>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { collection } = req.locals;

    return Collection.update(
        req.body, {
            where: {
                id: collection.id
            },
            user: req.user,
            individualHooks: true
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thành công'
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Remove
 *
 * @public
 * @param {String} id
 * @returns {Promise<>, APIException>}
 */
exports.delete = (req, res, next) => {
    const { collection } = req.locals;
    return Collection.destroy({
        where: {
            id: collection.id
        },
        user: req.user,
        individualHooks: true
    }).then(() => {
        res.json({
            code: 0,
            message: 'Xoá bài viết thành công'
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};
