import { pick, defaults } from 'lodash';
import { handler as ErrorHandler } from '../../middlewares/error';
import PostCategory from '../../../common/models/post-category.model';

/**
 * Convert category array to nested category
 * @private
 */
function convertToNestedCategory(arr, rootParent = null) {
    const nodes = {};
    return arr.filter((obj) => {
        const currentId = obj.id;
        let { parent_id } = obj;
        const isRootParent = currentId === rootParent;

        nodes[currentId] = defaults(obj, nodes[currentId], {
            children: []
        });
        parent_id =
            parent_id &&
            (nodes[parent_id] = nodes[parent_id] || {
                children: []
            }).children.push(obj);

        return !parent_id || isRootParent;
    });
}

/**
 * Create
 *
 * @public
 * @param {PostCategory} body
 * @returns {Promise<PostCategory, APIError>}
 */
exports.create = (req, res, next) => {
    req.body.created_by = pick(
        req.user, ['id', 'name']
    );
    PostCategory.create(
        req.body
    ).then((data) => {
        res.json({
            code: 0,
            message: 'Thêm mới thành công',
            data: PostCategory.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * List
 *
 * @public
 * @param {String} id
 * @returns {Promise<PostCategory[], APIError>}
 */
exports.list = async (req, res, next) => {
    try {
        let categories = await PostCategory.list(req.query);

        // transform nested category
        const { nested, slug = null } = req.query;
        let transformedCategories = categories.map((category) =>
            PostCategory.transform(category)
        );

        if (nested) {
            if (slug && categories.length === 1) {
                categories = await PostCategory.list({
                    id: categories[0].id,
                    nested
                });
            }
            transformedCategories = convertToNestedCategory(
                categories.map((category) => PostCategory.transform(category)),
                null
            );
        }

        return res.json({
            data: transformedCategories
        });
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<PostCategory, APIError>}
 */
exports.detail = (req, res) => res.json({ code: 0, data: PostCategory.transform(req.locals.category) });

/**
 * Update
 *
 * @public
 * @param {PostCategory} body
 * @returns {Promise<JSON, APIError>}
 */
exports.update = async (req, res, next) => {
    const { category: oldModel } = req.locals;
    const dataChanged = PostCategory.getChangedProperties({
        oldModel: oldModel,
        newModel: req.body
    });

    /**  replace existing category */
    const operations = pick(req.body, dataChanged);
    operations.updated_at = new Date();
    return PostCategory.update(
        operations, {
            where: {
                id: oldModel.id
            },
            params: {
                updated_by: pick(
                    req.user, ['id', 'name']
                )
            },
            individualHooks: true
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thành công'
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Delete
 * @public
 * @param  {String} id
 * @returns {Promise<null, APIError}
 */
exports.delete = async (req, res, next) => {
    try {
        const { category } = req.locals;
        await PostCategory.destroy({
            where: {
                id: category.id
            },
            individualHooks: true
        });
        return res.json({
            code: 0,
            message: 'Xoá danh mục thành công'
        });
    } catch (ex) {
        return ErrorHandler(ex, req, res, next);
    }
};
