import { ConsumerGroups } from 'auth-adapter';
import { handler as ErrorHandler } from '../../middlewares/error';
// Models
import Promotion from '../../../common/models/promotion.model';

/**
 * List
 *
 * @public
 * @param query
 * @returns {Promise<Promotion[]>, APIError>}
 */
exports.list = async (req, res, next) => {
    Promotion.list(
        req.query
    ).then(result => {
        res.json({
            code: 0,
            count: req.totalRecords,
            data: result.map(a => Promotion.transform(a, req.authInfo.accessLevel === ConsumerGroups.STAFF))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {String} id
 * @returns {Promise<Promotion>, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Promotion.transform(req.locals.promotion, req.authInfo.accessLevel === ConsumerGroups.STAFF) });
