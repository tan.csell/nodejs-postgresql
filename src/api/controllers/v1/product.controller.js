import { ConsumerGroups } from 'auth-adapter';
import { handler as ErrorHandler } from '../../middlewares/error';
import Product from '../../../common/models/product.model';

/**
 * List
 *
 * @public
 * @param {*} query
 * @returns {Promise<Product, APIError>}
 */
exports.list = async (req, res, next) => {
    Product.list(
        req.query
    ).then(data => {
        res.json({
            code: 0,
            sum: req.locals.sum,
            count: req.locals.count,
            data: data.map(i => Product.transform(i.dataValues, req.authInfo.accessLevel !== ConsumerGroups.USER))
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {*} id
 * @returns {Promise<Product, APIError>}
 */
exports.get = (req, res) => res.json({ code: 0, data: Product.transform(req.locals.product, req.authInfo.accessLevel === ConsumerGroups.STAFF) });

/**
 * Update
 *
 * @public
 * @param {*} body
 * @returns {Promise<Product>, APIError>}
 */
exports.update = async (req, res, next) => {
    const { product } = req.locals;

    return Product.update(
        req.body, {
            where: { id: product.id },
            individualHooks: true,
        }
    ).then(() => {
        res.json({
            code: 0,
            message: 'Cập nhật thông tin sản phẩm thành công'
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};
