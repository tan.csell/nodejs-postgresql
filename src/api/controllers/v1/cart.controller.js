import { ConsumerGroups } from 'auth-adapter';
import messages from '../../../config/messages';
import { handler as ErrorHandler } from '../../middlewares/error';
import Cart from '../../../common/models/cart.model';
import Order from '../../../common/models/order.model';

/**
 * List
 *
 * @public
 * @param {*} body
 * @returns {Promise<Cart[], APIError>}
 */
exports.list = async (req, res, next) => {
    await Cart.list(
        req.query
    ).then(data => {
        res.json({
            code: 0,
            count: req.locals.totalRecords,
            data: data.map(i => Cart.transform(i, true))
        });
    }).catch(error => {
        ErrorHandler(error, req, res, next);
    });
};

/**
 * Generate
 *
 * @public
 * @param {*} body
 * @returns {Promise<Cart, APIError>}
 */
exports.create = async (req, res, next) => {
    await Cart.create(
        req.body
    ).then(data => {
        res.json({
            code: 0,
            message: messages.CREATE_SUCCESS,
            data: Cart.transform(data)
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {*} body
 * @returns {Promise<Cart, APIError>}
 */
exports.detail = async (req, res, next) => res.json({ code: 0, data: Cart.transform(req.locals.order, req.authInfo.accessLevel !== ConsumerGroups.USER) });

/**
 * Update
 *
 * @public
 * @param {*} body
 * @returns {Promise<null, APIError>}
 */
exports.update = async (req, res, next) => {
    const { order } = req.locals;
    order.updated_at = new Date();

    return Cart.update(
        Cart.filterParams(
            order
        ), {
            where: {
                status: Cart.Statuses.PICKING,
                client_id: req.headers['x-client-id']
            }
        }
    ).then(() => {
        res.json({
            code: 0,
            message: messages.UPDATE_SUCCESS
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Confirm Cart
 * @param {*} body
 * @returns {Promise<Order, APIError>}
 */
exports.confirm = async (req, res, next) => {
    const { order } = req.locals;
    const { payments, deliveries } = order;
    await Order.create(
        order, {
            payments,
            deliveries
        }
    ).then(data => {
        res.json({
            code: 0,
            data: Order.transform(data),
            message: messages.CREATE_SUCCESS,
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};
