import { ConsumerGroups } from 'auth-adapter';
import { handler as ErrorHandler } from '../../middlewares/error';
import Order from '../../../common/models/order.model';

/**
 * List
 *
 * @public
 * @param {*} queryParams
 * @returns {Promise<Order[], APIError>}
 */
exports.list = async (req, res, next) => {
    Order.list(
        req.query
    ).then(orders => {
        res.json({
            code: 0,
            sum: req.locals.sum,
            count: req.totalRecords,
            data: orders.map(x => Order.transform(x))
        });
    }).catch(ex => {
        ErrorHandler(ex, req, res, next);
    });
};

/**
 * Detail
 *
 * @public
 * @param {*} body
 * @returns {Promise<Order, APIError>}
 */
exports.detail = (req, res) => res.json({ code: 0, data: Order.transform(req.locals.order, req.authInfo.accessLevel === ConsumerGroups.STAFF) });
