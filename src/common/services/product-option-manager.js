/* eslint-disable no-param-reassign */
import { LogEvent } from 'rabbit-event-source';
import { isEqual, isNil, pick } from 'lodash';
import eventBus from './event-bus';
import ProductOption from '../models/product-option.model';
import ProductPrice from '../models/product-price.model';
import Product from '../models/product.model';

function registerProductEvent() {
    /**
     * Converter
     * @param {*} str
     */
    function convertToEn(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
        str = str.replace(/Đ/g, 'D');
        str = str.toLowerCase();
        return str;
    }
    /**
     * Register updated event
     */
    eventBus.on(Product.Events.PRODUCT_UPDATED, async (data) => {
        try {
            const { dataValues: newModel, _previousDataValues: oldModel } = data;
            if (oldModel.variations.length > 0 && !oldModel.variations[0].values) {
                // case thêm mới và chỉnh sửa
                if (newModel.variations && oldModel.variations) {
                    const operations = [];
                    if (newModel.variations && newModel.variations.length >= oldModel.variations.length) {
                        newModel.variations.forEach((productOption, i) => {
                            const params = {};
                            // ko nó ở old thêm mới
                            if (!oldModel.variations[i] && (productOption.is_active === true || !productOption.is_active)) {
                                const price = ProductPrice.DefaultValues;
                                params.parent_id = data.id;
                                params.type = data.type;
                                params.name = productOption.name;
                                params.sku = productOption.sku.toString().toLowerCase().trim();
                                params.variations = productOption.variations;
                                params.barcode = productOption.barcode;
                                params.unit = productOption.unit;
                                params.units = productOption.units ? productOption.units : data.units;
                                params.options = productOption.options;
                                params.indexes = productOption.indexes;
                                params.brand = productOption.brand;
                                params.stock_address = productOption.stock_address;
                                params.images = productOption.images;
                                params.price_books = [Object.assign(price, { price: productOption.price, normal_price: productOption.price })];
                                params.original_price = isNil(productOption.original_price) ? data.original_price : productOption.original_price;
                                params.normal_price = isNil(productOption.normal_price) ? data.normal_price : productOption.normal_price;
                                params.price = isNil(productOption.price) ? data.price : productOption.price;
                                if (!params.option_name) {
                                    params.option_name = 'DEFAULT';
                                }
                                params.normalize_name = convertToEn(`${productOption.name} - ${productOption.sku}`);
                                params.option_name = productOption.option_name;
                                params.master_id = productOption.master_id;
                                params.device_id = data.device_id;
                                params.system_id = data.system_id;
                                params.category_path = data.category_path;
                                params.attribute_path = data.attribute_path;
                                params.price_book_path = [`${price.id}`];
                                params.normalize_category = data.normalize_category;
                                params.normalize_attribute = data.normalize_attribute;
                                params.normalize_variation = data.normalize_variation;
                                params.created_by = productOption.created_by ? productOption.created_by : data.created_by;
                                operations.push(
                                    ProductOption.create(
                                        params
                                    )
                                );
                            }
                            // có ở old update
                            if (oldModel.variations[i] && !isEqual(productOption, oldModel.variations[i])) {
                                if (productOption.is_active !== false) {
                                    const oldProductOption = oldModel.variations[i];
                                    let dataChanged = ProductOption.getChangedProperties({
                                        oldModel: oldProductOption,
                                        newModel: productOption
                                    });
                                    if (dataChanged.includes('option_name') && productOption.option_name === 'DEFAULT') {
                                        dataChanged = dataChanged.filter(o => o !== 'option_name');
                                    }

                                    const paramChanged = pick(
                                        productOption, dataChanged
                                    );
                                    if (dataChanged.includes('name') || dataChanged.includes('sku')) {
                                        paramChanged.name_path = productOption.name.split(' ');
                                        paramChanged.normalize_name = convertToEn(`${productOption.name} - ${productOption.sku}`);
                                    }
                                    paramChanged.updated_by = productOption.updated_by || data.updated_by;
                                    if (oldProductOption.sku) {
                                        operations.push(
                                            ProductOption.update(
                                                paramChanged, {
                                                    where: {
                                                        sku: oldProductOption.sku.toLowerCase(),
                                                        parent_id: data.id
                                                    },
                                                    individualHooks: true
                                                }
                                            )
                                        );
                                    }
                                }
                            }
                            if (productOption.is_active === false) {
                                ProductOption.update({
                                    sku: Date.now(),
                                    is_active: false,
                                    updated_at: new Date(),
                                    updated_by: pick(data.updated_by, ['id', 'name'])
                                }, {
                                    where: {
                                        sku: productOption.sku.toLowerCase(),
                                        parent_id: data.id
                                    }
                                });
                            }
                        });
                    }
                    if (newModel.variations) {
                        const variations = newModel.variations.filter(o => (o.is_active !== false));
                        if (newModel.variations && newModel.variations.length !== variations.length) {
                            operations.push(
                                Product.update({ variations }, { where: { id: newModel.id } })
                            );
                        }
                    }
                    await Promise.all(operations);
                }
            } else {
                // case handle data cũ
                const operations = [];
                newModel.variations.forEach((productOption) => {
                    operations.push(
                        ProductOption.update({ variations: productOption.variations }, { where: { sku: productOption.sku.toLowerCase() } })
                    );
                });
                await Promise.all(operations);
            }
            // remove product
            if (
                oldModel.is_active &&
                newModel.is_active === false
            ) {
                await ProductOption.update({
                    sku: Date.now(),
                    updated_at: new Date(),
                    updated_by: newModel.updated_by,
                    is_active: false
                }, {
                    where: {
                        parent_id: oldModel.id
                    }
                });
            }
            // update status
            if (
                data._changed.has('status')
            ) {
                await ProductOption.update({
                    updated_at: new Date(),
                    status: newModel.status,
                    status_name: newModel.status_name
                }, {
                    where: {
                        parent_id: oldModel.id
                    }
                });
            }
        } catch (error) {
            const logEvent = new LogEvent({
                code: error.code || 500,
                message: `Cannot remove sku product option for product: ${data.id}`,
                errors: error.errors,
                stack: error.stack
            });
            await logEvent.save();
        }
    });
}

module.exports = {
    registerProductEvent
};
