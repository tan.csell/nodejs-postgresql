/* eslint-disable no-param-reassign */
import { cloneDeep, isNil } from 'lodash';

// Models
import Product from '../../models/product.model';
import ProductPrice from '../../models/product-price.model';
import ProductOption from '../../models/product-option.model';

/**
 * Converter
 * @param {*} str
 */
function convertToEn(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    str = str.toLowerCase();
    return str;
}

/**
 * Get Part
 * @param {*} part
 */
async function getPart(part) {
    const returnPart = {
        id: part.id,
        sku: null,
        type: null,
        name: null,
        unit: null,
        brand: null,
        price: 0,
        normal_price: 0,
        original_price: 0,
        options: part.options,
        option_id: part.option_id,
        thumbnail_url: null,
    };
    const product = await Product.get(
        part.id
    );
    returnPart.sku = product.sku;
    returnPart.type = product.type;
    returnPart.name = product.name;
    returnPart.unit = product.unit;
    returnPart.price = product.price;
    returnPart.normal_price = product.normal_price;
    returnPart.original_price = product.original_price;
    returnPart.thumbnail_url = product.thumbnail_url;
    return returnPart;
}

function parseParts(parts) {
    const promise = parts.map(getPart);
    return Promise.all(promise);
}

function parseOptions(data) {
    let operations = [];
    operations = [{
        sku: data.sku,
        name: data.name,
        images: [],
        barcode: data.barcode,
        indexes: [],
        option_name: 'DEFAULT',
        name_path: data.name.split(' '),
        price: data.price,
        unit: data.unit,
        normal_price: data.normal_price,
        original_price: data.original_price,
        price_book_path: [`${ProductPrice.DefaultValues.id}`],
        normalize_name: convertToEn(`${data.name} - ${data.sku}`),
        price_books: [Object.assign(ProductPrice.DefaultValues, { price: data.price, normal_price: data.normal_price })]
    }];

    return operations;
}

function parseShopeeOptions(data) {
    return [{
        sku: data.sku,
        name: data.name,
        images: [],
        barcode: null,
        indexes: [],
        option_name: 'DEFAULT',
        name_path: data.name.split(' '),
        price: data.price,
        normal_price: data.normal_price,
        original_price: null,
        price_book_path: data.formula_price_book ?
            [`${data.formula_price_book.id}`] :
            [],
        normalize_name: convertToEn(`${data.name} - ${data.sku}`),
        price_books: data.formula_price_book ?
            [Object.assign(data.formula_price_book, { price: data.price, normal_price: data.normal_price })] :
            []
    }];
}

function parseData(data) {
    const params = cloneDeep(
        data
    );
    if (params.variations && params.variations.length) {
        params.variations = params.variations.map(v => {
            const productOptions = ProductOption.filterParams(v);
            return productOptions;
        });
    }
    if (
        params.name &&
        isNil(params.meta_title)
    ) {
        params.meta_title = params.name;
    }
    if (
        params.thumbnail_url &&
        isNil(params.meta_image)
    ) {
        params.meta_image = params.thumbnail_url;
    }
    if (params.name &&
        isNil(params.meta_keyword)
    ) {
        params.meta_keyword = params.name;
    }
    if (
        params.name &&
        isNil(params.meta_description)
    ) {
        params.meta_description = params.name;
    }
    if (
        params.name &&
        isNil(params.slug)
    ) {
        params.slug = convertToEn(`${data.name.split(' ').join('-')}-i.${data.sku}`);
    }
    if (
        params.name || params.sku
    ) {
        params.name_path = data.name.split(' ');
        params.normalize_name = convertToEn(`${data.name} - ${data.sku}`);
    }
    if (
        data.categories &&
        data.categories.length >= 0
    ) {
        params.category_path = data.categories.map(c => c.id);
        params.normalize_category = params.category_path.join(',');
    }
    if (
        data.suppliers &&
        data.suppliers.length >= 0
    ) {
        params.supplier_path = data.suppliers.map(c => c.id);
    }
    if (
        data.attributes &&
        data.attributes.length >= 0
    ) {
        const attributePath = [];
        data.attributes.forEach(a => attributePath.push(`${a.id}:${a.value.id}`));
        params.normalize_attribute = attributePath.join(',');
        params.attribute_path = attributePath;
    }
    if (
        data.attributes &&
        data.attributes.length >= 0
    ) {
        const brand = data.attributes.find(a => a.name === 'Thương hiệu');
        params.brand = brand ? brand.value.name : null;
    }
    if (
        data.variations &&
        data.variations.length >= 0
    ) {
        data.variations.forEach(item => {
            params.normalize_name += `- ${item.sku}:${item.option_name}`;
        });
        const variationPath = new Set();
        data.variations.forEach(v => {
            if (v.options) {
                v.options.map(value => variationPath.add(value));
            }
        });
        params.normalize_variation = [...variationPath].join(',');
        params.variation_path = [...variationPath];
    }
    return params;
}

function parseItems(data) {
    const promises = data.map(parseData);
    return promises;
}

export default {
    parseData,
    parseItems,
    parseParts,
    parseOptions,
    parseShopeeOptions
};
