import httpStatus from 'http-status';
import { parseInt } from 'lodash';
import APIError from '../../utils/APIError';
import Product from '../../models/product.model';
import ProductOption from '../../models/product-option.model';
import Promotion from '../../models/promotion.model';
import Delivery from '../../models/delivery.model';


async function getItem(item) {
    const returnOption = {
        id: item.id,
        type: null,
        sku: null,
        name: null,
        unit: null,
        brand: null,
        campaign: null,
        discount: null,
        flash_deal: null,
        hot_deal: null,
        thumbnail_url: null,
        categories: null,
        product_parts: [],
        product_stocks: [],
        option_id: item.option_id,
        option_name: null,
        weight: 0,
        price: 0,
        normal_price: 0,
        total_price: 0,
        total_price_before_discount: 0,
        total_discount_value: 0,
        total_quantity: item.total_quantity,
        total_return_quantity: 0
    };
    if (item.option_id) {
        const product = await ProductOption.get(
            item.option_id
        );
        returnOption.sku = product.sku;
        returnOption.name = product.name;
        returnOption.flash_deal = product.flash_deal;
        returnOption.hot_deal = product.hot_deal;
        returnOption.option_name = product.option_name;
        returnOption.product_stocks = product.stocks;

        if (product.hot_deal && !product.flash_deal) {
            returnOption.normal_price = Math.ceil(product.hot_deal.normal_price);
            const hot_deal = await Promotion.get(
                product.hot_deal.id
            );
            if (product.hot_deal) {
                if (Number(item.total_quantity) > Number(product.hot_deal.total_available_quantity)) {
                    throw new APIError({
                        status: httpStatus.BAD_REQUEST,
                        message: `Sản phẩm này số lượng khuyến mãi chỉ còn ${product.hot_deal.total_available_quantity}`
                    });
                }
            }
            if (hot_deal && hot_deal.id) {
                await Promotion.checkValid({
                    item,
                    discount: hot_deal
                });
            }
            returnOption.price = Math.ceil(
                returnOption.normal_price - product.hot_deal.value
            );
        }
        if (product.flash_deal) {
            returnOption.normal_price = Math.ceil(product.flash_deal.normal_price);
            const flash_deal = await Promotion.get(
                product.flash_deal.id
            );
            if (product.flash_deal) {
                if (Number(item.total_quantity) > Number(product.flash_deal.total_available_quantity)) {
                    throw new APIError({
                        status: httpStatus.BAD_REQUEST,
                        message: `Sản phẩm này số lượng khuyến mãi chỉ còn ${product.flash_deal.total_available_quantity}`
                    });
                }
            }
            if (flash_deal && flash_deal.id) {
                await Promotion.checkValid({
                    item,
                    discount: flash_deal
                });
            }
            returnOption.price = Math.ceil(
                returnOption.normal_price - product.flash_deal.value
            );
        }

        if (!product.flash_deal && !product.hot_deal) {
            returnOption.price = Math.ceil(product.price);
            returnOption.normal_price = Math.ceil(product.normal_price);
        }
    }
    if (item.id) {
        const product = await Product.get(
            item.id
        );
        returnOption.type = product.type;
        returnOption.unit = product.unit;
        returnOption.brand = product.brand;
        returnOption.weight = Math.ceil(product.weight);
        returnOption.categories = product.categories;
        returnOption.thumbnail_url = product.thumbnail_url;
        if (
            product.parts &&
            product.parts.length &&
            returnOption.type === Product.Types.COMBO
        ) {
            product.parts.forEach(part => {
                const option = part.options.find(
                    o => o.name === returnOption.option_name
                );
                returnOption.product_parts.push({
                    id: part.id,
                    name: part.name,
                    option_id: part.option_id,
                    product_id: returnOption.id,
                    product_option_id: returnOption.option_id,
                    total_quantity: Math.ceil(option.quantity * returnOption.total_quantity),
                    total_price: Math.ceil(part.price * (option.quantity * returnOption.total_quantity)) || 0
                });
            });
        }

        if (returnOption.type === Product.Types.ITEM) {
            returnOption.product_parts.push({
                id: returnOption.id,
                name: returnOption.name,
                option_id: returnOption.option_id,
                product_id: returnOption.id,
                product_option_id: returnOption.option_id,
                total_quantity: returnOption.total_quantity,
                total_price: Math.ceil(returnOption.total_quantity * returnOption.normal_price)
            });
        }
    }
    if (item.total_quantity) {
        await ProductOption.checkStockIsValid(
            returnOption
        );
    }
    returnOption.total_price = Math.ceil(
        item.total_quantity * returnOption.price
    );
    returnOption.total_price_before_discount = Math.ceil(
        item.total_quantity * returnOption.normal_price
    );
    returnOption.total_discount_value = Math.ceil(
        returnOption.total_price_before_discount - returnOption.total_price
    );
    return returnOption;
}

/**
 * Parse item to import item
 * @param {*} items
 */
async function parseItems(items) {
    const promises = items.map(
        (i) => getItem(i)
    );
    return Promise.all(promises);
}

/**
 * Calculate amount
 * @param {*} data
 */
async function calTotalPrice(data) {
    const returnAmount = {
        total_quantity_valid: 0,
        total_quantity: 0,
        total_product: 0,
        total_point: 0,
        total_shipping_fee: 0,
        total_price_before_discount: 0,
        total_price_after_discount: 0,
        total_discount_value: 0,
        total_price: 0,
        total_paid: 0,
        total_unpaid: 0
    };
    if (data.products && data.products.length) {
        data.products.forEach((product) => {
            returnAmount.total_product += 1;
            returnAmount.total_quantity += Math.ceil(product.total_quantity);
            returnAmount.total_quantity_valid += product.total_price > 99000 ? 1 : 0;
            returnAmount.total_price_after_discount += Math.ceil(product.total_price);
            returnAmount.total_price_before_discount += Math.ceil(product.total_price);
        });
    }
    if (data.discounts && data.discounts.length) {
        data.discounts.forEach((discount) => {
            // percent discount
            if (discount.type === 1) {
                returnAmount.total_price_after_discount *= [(100 - discount.value) / 100];
            }

            // cash discount
            if (discount.type === 2) {
                returnAmount.total_price_after_discount -= discount.value;
            }
        });
    }
    if (data.payments && data.payments.length) {
        data.payments.forEach((payment) => {
            returnAmount.total_paid += payment.value;
        });
    }
    if (data.deliveries && data.deliveries.length) {
        data.deliveries.forEach(delivery => {
            returnAmount.total_shipping_fee += delivery.payment_by === Delivery.PaymentBys.NGUOINHAN ?
                Math.ceil(delivery.total_shipping_fee) :
                0;
        });
    }
    returnAmount.total_discount_value = Math.ceil(
        returnAmount.total_price_before_discount - returnAmount.total_price_after_discount
    );
    returnAmount.total_price_after_discount = Math.ceil(
        returnAmount.total_price_after_discount + returnAmount.total_shipping_fee
    );
    returnAmount.total_price = Math.ceil(
        returnAmount.total_price_after_discount - (data.total_exchange_price || 0)
    );
    returnAmount.total_unpaid = returnAmount.total_price >= returnAmount.total_paid ?
        returnAmount.total_price - returnAmount.total_paid :
        0;
    // calculate loyalty = 1%
    returnAmount.total_point = parseInt(
        returnAmount.total_unpaid * (1 / 100), 0
    );
    return returnAmount;
}

export default {
    parseItems,
    calTotalPrice
};
