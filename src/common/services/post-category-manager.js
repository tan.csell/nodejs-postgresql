import { Op } from 'sequelize';
import { LogEvent } from 'rabbit-event-source';

// Models
import eventBus from './event-bus';
import PostCategory from '../models/post-category.model';

function registerPostCategoryEvent() {
    /**
     * Nếu xóa category cha thì sẽ xóa các categories con nếu như categories không có sản phẩm
     */
    eventBus.on(PostCategory.Events.POST_CATEGORY_DELETED, async (data) => {
        try {
            const { id } = data.dataValues;
            await PostCategory.destroy({
                where: {
                    path: {
                        [Op.contains]: [id]
                    }
                }
            });
        } catch (error) {
            const logEvent = new LogEvent({
                code: error.code || 500,
                message: `Cannot delete children categories of parent category ${data.id}`,
                errors: error.errors,
                stack: error.stack
            });
            await logEvent.save();
        }
    });
}

module.exports = {
    registerPostCategoryEvent
};
