/* eslint-disable no-param-reassign */
import { LogEvent } from 'rabbit-event-source';
import { isNil } from 'lodash';
import eventBus from './event-bus';
import Product from '../models/product.model';
import ProductOption from '../models/product-option.model';
import ProductComment from '../models/product-comment.model';
import ProductPrice from '../models/product-price.model';

function registerProductEvent() {
    /**
     * Converter
     * @param {*} str
     */
    function convertToEn(str) {
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
        str = str.replace(/đ/g, 'd');
        str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
        str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
        str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
        str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
        str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
        str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
        str = str.replace(/Đ/g, 'D');
        str = str.toLowerCase();
        return str;
    }

    // /**
    //  * Register created event
    //  */
    eventBus.on(Product.Events.PRODUCT_CREATED, async (data) => {
        try {
            const price = ProductPrice.DefaultValues;
            const operations = data.variations.map((product, index) => {
                const element = ProductOption.filterParams(product);
                element.parent_id = data.id;
                element.type = data.type;
                element.units = data.units;
                if (index !== 0) {
                    element.sku = product.sku.toString().toLowerCase().trim();
                } else {
                    element.sku = data.sku;
                }
                element.variations = product.variations;
                element.name = product.name;
                element.unit = product.unit;
                element.brand = data.brand;
                element.barcode = product.barcode;
                if (!element.option_name) {
                    element.option_name = 'DEFAULT';
                }
                element.providers = product.providers;
                element.price_books = [Object.assign(price, { price: product.price, normal_price: product.price })];
                element.original_price = isNil(product.original_price) ? data.original_price : product.original_price;
                element.normal_price = isNil(product.normal_price) ? data.normal_price : product.normal_price;
                element.price = isNil(product.price) ? data.price : product.price;
                // parent and child diffirent price
                element.price = data.price;
                element.normal_price = data.normal_price;
                element.original_price = data.original_price;
                element.price_books = [Object.assign(price, { price: data.price, normal_price: data.price })];
                element.normalize_name = convertToEn(`${element.name} - ${element.sku}`);
                element.name_path = element.name.split(' ');
                element.is_default = index === 0;
                element.is_visible = product.is_visible;
                element.device_id = data.device_id;
                element.system_id = data.system_id;
                element.category_path = data.category_path;
                element.attribute_path = data.attribute_path;
                element.price_book_path = [`${price.id}`];
                element.normalize_category = data.normalize_category;
                element.normalize_attribute = data.normalize_attribute;
                element.normalize_variation = element.options && element.options.length ? element.options.join(',') : null;
                element.created_by = product.created_by ? product.created_by : data.created_by;
                return element;
            });

            await ProductOption.bulkCreate(
                operations, { individualHooks: true }
            );
        } catch (error) {
            const logEvent = new LogEvent({
                code: error.code || 500,
                message: `Cannot add list product option to parent: ${data.id}`,
                errors: error.errors,
                stack: error.stack
            });
            await logEvent.save();
        }
    });
}

function registerCommentEvent() {
    /**
     * Register created event
     * @param {*} model
     */
    eventBus.on(ProductComment.Events.COMMENT_CREATED, async (model) => {
        try {
            const comment = model.dataValues;

            // Increment reply count
            if (comment.parent_id && comment.is_reply) {
                await ProductComment.increment({
                    reply_count: 1
                }, {
                    where: {
                        id: comment.parent_id,
                        is_active: true
                    }
                });
            }

            // Increment product count
            if (comment.product) {
                await Product.increment({
                    comment_count: 1
                }, {
                    where: {
                        id: comment.product.id,
                        is_active: true
                    }
                });
            }
        } catch (error) {
            const logEvent = new LogEvent({
                code: error.code || 500,
                message: `Cannot update comment_count for product from comment: ${model.id}`,
                errors: error.errors,
                stack: error.stack
            });
            await logEvent.save();
        }
    });
}

module.exports = {
    registerProductEvent,
    registerCommentEvent,
};
