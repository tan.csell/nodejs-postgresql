import productEvent from '../services/product-manager';

export default {
    register: () => {
        productEvent.registerProductEvent();
    }
};
