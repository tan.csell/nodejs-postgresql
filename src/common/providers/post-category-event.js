import postCategoryEvent from '../services/post-category-manager';

export default {
    register: () => {
        postCategoryEvent.registerPostCategoryEvent();
    }
};
