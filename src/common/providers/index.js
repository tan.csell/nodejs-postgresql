import postCategoryEvent from './post-category-event';
import collectionEvent from './collection-event';
import postEvent from './post-event';
import productEvent from './product-event';
import productOptionEvent from './product-option-event';

export default {
    register: () => {
        // register any event emitter || event rabbitmq here
        postCategoryEvent.register();
        collectionEvent.register();
        postEvent.register();
        productEvent.register();
        productOptionEvent.register();
    }
};
