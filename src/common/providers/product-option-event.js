import productOptionEvent from '../services/product-option-manager';

export default {
    register: () => {
        productOptionEvent.registerProductEvent();
    }
};
