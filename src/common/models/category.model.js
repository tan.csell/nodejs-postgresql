/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { Model, DataTypes, Op } from 'sequelize';
import { isNil, omitBy, isEqual, pick, includes } from 'lodash';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../../common/utils/APIError';

/**
 * Create connection
 */
class Category extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'name',
    'slug',
    'logo',
    'group',
    'image',
    'content',
    'position',

    // social seo
    'meta_url',
    'meta_title',
    'meta_image',
    'meta_keyword',
    'meta_description',

    // config
    'path',
    'parent_id',
    'is_visible',
    'is_home_visible',
];

Category.Groups = {
    ITEM: 'item',
    PART: 'part'
};

/**
 * Product Part Schema
 * @private
 */
Category.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    slug: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    logo: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    group: {
        type: DataTypes.STRING(20),
        defaultValue: Category.Groups.ITEM
    },
    image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    content: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    path: {
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    position: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    parent_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    normalize_path: {
        type: DataTypes.STRING(255),
        defaultValue: ''
    },

    // seo
    meta_url: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_title: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_keyword: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_description: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_home_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    tableName: 'tbl_categories'
});


/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Category.addHook('beforeCreate', async (model) => {
    model.parent_id = model.parent_id ?
        model.parent_id :
        null;

    if (model.parent_id !== null) {
        model.path = await Category.getParentPath(model.parent_id);
        model.normalize_path = model.path.join(',');
    }

    return model;
});

/**
 * Register event name
 */
Category.Events = {
    CATEGORY_CREATED: `${serviceName}.category.created`,
    CATEGORY_UPDATED: `${serviceName}.category.updated`,
    CATEGORY_DELETED: `${serviceName}.category.deleted`
};
Category.EVENT_SOURCE = `${serviceName}.category`;

Category.addHook('afterCreate', () => {});

Category.addHook('afterUpdate', () => {});

Category.addHook('afterDestroy', () => {});

/**
 * Register event name
 */
Category.Events = {
    CATEGORY_CREATED: `${serviceName}.category.created`,
    CATEGORY_UPDATED: `${serviceName}.category.updated`,
    CATEGORY_DELETED: `${serviceName}.category.deleted`
};
Category.EVENT_SOURCE = `${serviceName}.category`;

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);

    // TODO: load condition
    const { id, keyword, nested } = options;

    if (id && nested) {
        options[Op.or] = [
            { id: id },
            // { path: id }
            {
                path: {
                    [Op.contains]: [id]
                }
            }
        ];
    }
    delete options.id;
    delete options.nested;

    if (keyword) {
        // set keyword option
        options.name = {
            [Op.iLike]: `%${keyword}%`
        };
    }
    delete options.keyword;

    if (options.system_id) {
        options.name = options.system_id;
    }

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'name':
            sort = [
                ['name', order_by]
            ];
            break;
        case 'position':
            sort = [
                ['position', order_by]
            ];
            break;
        case 'created_at':
            sort = [
                ['created_at', order_by]
            ];
            break;
        case 'updated_at':
            sort = [
                ['updated_at', order_by]
            ];
            break;
        default:
            sort = [
                ['created_at', 'ASC']
            ];
            break;
    }
    // sort.push(['normalize_path', 'ASC']);
    // sort.push(['position', 'ASC']);
    return sort;
}

/**
 * Transform postgres model to expose object
 * @param {*} model
 */
Category.transform = (model) => {
    const transformed = {};
    const fields = [
        // attribute
        'id',
        'name',
        'path',
        'slug',
        'logo',
        'group',
        'image',
        'content',
        'position',
        'parent_id',
        'is_visible',
        'is_home_visible',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',

        // manager
        'is_active',
        'created_by'
    ];

    fields.forEach((field) => {
        transformed[field] = model[field];
    });

    // add additional field
    transformed.children = [];

    transformed.created_at = moment(model.created_at).unix();
    transformed.updated_at = moment(model.updated_at).unix();

    return transformed;
};

/**
 * Get all changed properties
 */
Category.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'name',
        'slug',
        'logo',
        'image',
        'content',
        'position',
        'is_visible',
        'is_home_visible',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',
    ];

    // get all changable properties
    Object.keys(newModel).forEach((field) => {
        if (includes(allChangableProperties, field)) {
            changedProperties.push(field);
        }
    });

    // get data changed
    const dataChanged = [];
    changedProperties.forEach(field => {
        if (!isEqual(newModel[field], oldModel[field])) {
            dataChanged.push(field);
        }
    });
    return dataChanged;
};

/**
 * Get attribute
 *
 * @param {ObjectId} id - The id of attribute.
 * @returns {Promise<Attribute, APIError>}
 */
Category.get = async (id) => {
    try {
        const category = await Category.findByPk(
            id
        );
        if (!category) {
            throw new APIError({
                message: `Không tìm thấy danh mục sản phẩm ${id}!`,
                status: httpStatus.NOT_FOUND
            });
        }
        return category;
    } catch (error) {
        throw error;
    }
};

/**
 * List categories in descending order of 'created_at' timestamp.
 *
 * @param {number} skip - Number of categories to be skipped.
 * @param {number} limit - Limit number of categories to be returned.
 * @returns {Promise<Attribute[]>}
 */
Category.list = async ({
    id,
    group,
    nested,
    keyword,
    system_id,
    is_active,
    is_visible,
    is_home_visible,

    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        id,
        group,
        nested,
        keyword,
        system_id,
        is_active,
        is_visible,
        is_home_visible
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Category.findAll({
        where: options,
        order: sort,
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of attrs to be skipped.
 * @param {number} limit - Limit number of attrs to be returned.
 * @returns {Promise<Number>}
 */
Category.totalRecords = ({
    id,
    group,
    nested,
    keyword,
    system_id,
    is_active,
    is_visible,
    is_home_visible
}) => {
    const options = filterConditions({
        id,
        group,
        nested,
        keyword,
        system_id,
        is_active,
        is_visible,
        is_home_visible
    });

    return Category.count({
        where: options
    });
};

/**
 * Get path from parent
 * @param {*} parentId
 */
Category.getParentPath = async (parent_id) => {
    if (!parent_id) {
        return [];
    }
    const parentCategory = await Category.get(parent_id);
    const path = parentCategory.path.slice(0);
    path.push(parentCategory.id);
    return path;
};

/**
 * Expose only public field
 *
 * @param {Object} params
 */
Category.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Attribute
 */
export default Category;
