/* eslint-disable no-restricted-globals */
/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { pick, isEqual, values, omitBy, isNil, isUndefined, parseInt } from 'lodash';
import moment from 'moment-timezone';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import eventBus from '../services/event-bus';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
class Voucher extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'code',
    'name',
    'content',
    'status',
    'status_name',
    'thumbnail_url',
    'applied_users',
    'applied_stores',
    'applied_members',
    'applied_categories',
    'applied_user_types',
    'applied_user_groups',
    'applied_for_items',
    'applied_for_months',
    'applied_for_days',
    'applied_for_hours',
    'applied_available_quantity',
    'applied_max_quantity',
    'applied_order_value',
    'applied_start_time',
    'applied_stop_time',
    'applied_sources',
    'max_discount_value',
    'discount_value',
    'discount_type',
    'is_visible',
    'normalize_item',
    'normalize_member',
    'normalize_member'
];

Voucher.Statuses = {
    ACTIVE: 'active',
    INACTIVE: 'inactive',
    CANCELLED: 'cancelled'
};

Voucher.StatusNames = {
    ACTIVE: 'Đang hoạt động',
    INACTIVE: 'Ngừng hoạt động',
    CANCELLED: 'Đã huỷ'
};

/**
 * Voucher Schema
 * @public
 */
Voucher.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    code: {
        type: DataTypes.STRING(250),
        defaultValue: null
    },
    name: {
        type: DataTypes.STRING(250),
        allowNull: false
    },
    content: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    status: {
        type: DataTypes.STRING(10),
        values: values(Voucher.Statuses),
        defaultValue: Voucher.Statuses.ACTIVE
    },
    status_name: {
        type: DataTypes.STRING(50),
        values: values(Voucher.StatusNames),
        defaultValue: Voucher.StatusNames.ACTIVE
    },
    thumbnail_url: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    applied_users: {
        // name: Áp dụng theo người dùng
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_stores: {
        // name: Áp dụng theo chi nhánh
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_members: {
        // name: Áp dụng theo nhân viên
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_categories: {
        // name: Áp dụng theo nhóm hàng
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_user_types: {
        // name: Áp dụng theo loại khách hàng
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_user_groups: {
        // name: Áp dụng theo nhóm khách hàng
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_for_items: {
        // name: Áp dụng với các mặt hàng
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_for_months: {
        // name: Áp dụng theo các tháng
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    applied_for_days: {
        // name: Áp dụng theo các ngày
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    applied_for_hours: {
        // name: Áp dụng theo khung giờ
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    applied_max_quantity: {
        // name: Giới hạn tổng số lượt sử dụng
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    applied_available_quantity: {
        // name: Giới hạn số lượt còn lại để sử dụng
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    applied_order_value: {
        // name: Áp dụng cho đơn hàng tối thiểu
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    applied_start_time: {
        // name: Thời gian bắt đầu
        type: DataTypes.DATE,
        defaultValue: null
    },
    applied_stop_time: {
        // name: Thời gian kết thúc
        type: DataTypes.DATE,
        defaultValue: null
    },
    applied_sources: {
        // name: App dụng cho nền tảng
        type: DataTypes.ARRAY(DataTypes.STRING),
        defaultValue: []
    },
    discount_type: {
        // name: Loại giá trị khuyến mại: 1 (%) | 2 (cash)
        type: DataTypes.INTEGER,
        defaultValue: 1
    },
    discount_value: {
        // name: Giá trị khuyến mại
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    max_discount_value: {
        // name: Giá trị khuyến mại tối đa
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },

    // metric
    user_has_used: {
        // name: Các khách hàng đã sử dụng
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    order_has_used: {
        // name: Các đơn hàng đã sử dụng
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    member_has_used: {
        // name: Các nhân viên đã sử dụng
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },

    // manager
    normalize_item: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    normalize_member: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    normalize_user: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    cancelled_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    cancelled_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'voucher',
    tableName: 'tbl_vouchers'
});

/**
 * Register event emiter
 */
Voucher.Events = {
    VOUCHER_CREATED: `${serviceName}.voucher.created`,
    VOUCHER_UPDATED: `${serviceName}.voucher.updated`,
    VOUCHER_DELETED: `${serviceName}.voucher.deleted`
};
Voucher.EVENT_SOURCE = `${serviceName}.voucher`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Voucher.addHook('beforeCreate', (data) => {
    if (data.applied_max_quantity) {
        data.applied_available_quantity = Math.ceil(
            data.applied_max_quantity
        );
    }
    return data;
});

Voucher.addHook('afterCreate', (data) => {
    eventBus.emit(Voucher.Events.VOUCHER_CREATED, data);
});

Voucher.addHook('afterUpdate', (data) => {
    eventBus.emit(Voucher.Events.VOUCHER_UPDATED, data);
});

Voucher.addHook('afterDestroy', (data) => {
    eventBus.emit(Voucher.Events.VOUCHER_DELETED, data);
});

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.name) {
        options.name = {
            [Op.iLike]: `%${options.name}%`
        };
    }

    if (options.code) {
        options.code = {
            [Op.iLike]: `%${options.code}%`
        };
    }
    if (options.codes) {
        options.code = {
            [Op.in]: options.codes.split(',')
        };
    }
    delete options.codes;

    if (options.keyword) {
        options[Op.or] = [{
            code: {
                [Op.iLike]: `%${options.keyword}%`
            }
        },
        {
            name: {
                [Op.iLike]: `%${options.keyword}%`
            }
        }
        ];
    }
    delete options.keyword;

    if (options.categories) {
        options[Op.or] = [{
            applied_categories: {
                [Op.overlap]: [options.categories.split(',')]
            }
        },
        {
            applied_categories: []
        }
        ];
    }
    delete options.categories;
    if (options.product_id) {
        options[Op.or] = [{
            normalize_item: {
                [Op.iLike]: `%${options.product_id}%`
            }
        },
        {
            applied_for_items: []
        }
        ];
    }
    delete options.product_id;
    if (options.members) {
        options[Op.or] = [{
            applied_members: {
                [Op.overlap]: [options.members.split(',')]
            }
        },
        {
            applied_members: []
        }
        ];
    }
    delete options.members;

    if (options.stores) {
        options[Op.or] = [{
            applied_stores: {
                [Op.overlap]: [options.stores.split(',')]
            }
        },
        {
            applied_stores: []
        }
        ];
    }
    delete options.stores;

    if (options.users) {
        options[Op.or] = [{
            applied_users: {
                [Op.overlap]: [options.users.split(',')]
            }
        },
        {
            applied_users: []
        }
        ];
    }
    delete options.users;


    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;
    if (options.is_expired) {
        options.applied_stop_time = {
            [Op.gte]: new Date()
        };
    }
    delete options.is_expired;
    // Date range filter
    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'name':
            sort = ['name', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Voucher.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'code',
        'name',
        'content',
        'status',
        'status_name',
        'thumbnail_url',
        'applied_users',
        'applied_stores',
        'applied_members',
        'applied_categories',
        'applied_user_types',
        'applied_user_groups',
        'applied_for_items',
        'applied_for_months',
        'applied_for_days',
        'applied_for_hours',
        'applied_available_quantity',
        'applied_max_quantity',
        'applied_order_value',
        'applied_total_price',
        'applied_start_time',
        'applied_stop_time',
        'applied_sources',
        'discount_type',
        'discount_value',
        'max_discount_value',
        'is_visible',
        'created_by',
        'created_at',
        'updated_at',
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe decimal
    const decimalFields = [
        'applied_order_value',
        'discount_value',
        'max_discount_value'
    ];
    decimalFields.forEach((field) => {
        transformed[field] = parseInt(params[field], 0);
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',
        'applied_start_time',
        'applied_stop_time'
    ];
    dateFields.forEach((field) => {
        transformed[field] = moment(params[field]).unix();
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Voucher.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'code',
        'name',
        'content',
        'status',
        'status_name',
        'applied_users',
        'applied_stores',
        'applied_members',
        'applied_categories',
        'applied_user_types',
        'applied_user_groups',
        'applied_for_items',
        'applied_for_months',
        'applied_for_days',
        'applied_for_hours',
        'applied_total_price',
        'applied_start_time',
        'applied_order_value',
        'applied_available_quantity',
        'applied_max_quantity',
        'applied_stop_time',
        'max_discount_value',
        'applied_sources',
        'discount_value',
        'discount_type',
        'is_visible',
        'normalize_item',
        'normalize_member',
        'normalize_member'
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Get Voucher By Id
 *
 * @public
 * @param {String} id
 */
Voucher.get = async (id) => {
    try {
        const voucher = await Voucher.findOne({
            where: {
                [Op.or]: [{
                    id: !isNaN(id) ? +id : null
                },
                { code: id.toUpperCase() }
                ],
                is_active: true
            }
        });
        if (!voucher) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy voucher: ${id}!`
            });
        }
        return voucher;
    } catch (error) {
        throw (error);
    }
};

/**
 * List users in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Supplider[]>}
 */
Voucher.list = async ({
    // page
    sort_by,
    order_by,
    skip = 0,
    limit = 20,

    // query
    name,
    code,
    codes,
    keyword,
    statuses,
    system_id,
    is_visible,
    is_active,
    min_created_at,
    max_created_at,
    categories,
    product_id,
    members,
    stores,
    users,
    is_expired
}) => {
    const options = filterConditions({
        name,
        code,
        codes,
        keyword,
        statuses,
        system_id,
        is_visible,
        is_active,
        min_created_at,
        max_created_at,
        categories,
        product_id,
        members,
        stores,
        users,
        is_expired
    });
    const sort = sortConditions({ sort_by, order_by });
    return Voucher.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
Voucher.totalRecords = ({
    name,
    code,
    codes,
    keyword,
    statuses,
    system_id,
    is_visible,
    is_active,
    min_created_at,
    max_created_at,
    categories,
    product_id,
    members,
    stores,
    users,
    is_expired
}) => {
    const options = filterConditions({
        name,
        code,
        codes,
        keyword,
        statuses,
        system_id,
        is_visible,
        is_active,
        min_created_at,
        max_created_at,
        categories,
        product_id,
        members,
        stores,
        users,
        is_expired
    });

    return Voucher.count({ where: options });
};

/**
 * Check Duplicate code
 *
 * @public
 * @param {String} code
 */
Voucher.checkDuplicate = async (code, id) => {
    try {
        const params = {
            code,
            is_active: true,
        };
        if (id) {
            params.id = {
                [Op.ne]: id
            };
        }
        const voucher = await Voucher.findOne({
            where: params
        });
        if (voucher) {
            throw new APIError({
                status: httpStatus.CONFLICT,
                message: `Voucher đã bị trùng mã: ${code}!`
            });
        }
        return null;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * Filter only allowed fields from voucher
 *
 * @param {Object} params
 */
Voucher.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Voucher
 */
export default Voucher;
