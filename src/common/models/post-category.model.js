/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { DataTypes, Model, Op } from 'sequelize';
import { isEqual, includes, omitBy, isNil } from 'lodash';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';
import eventBus from '../services/event-bus';

/**
 * Converter
 * @param {*} str
 */
function convertToEn(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    str = str.toLowerCase();
    return str;
}

/**
 * Create connection
 */
const sequelize = postgres.connect();
class PostCategory extends Model {}

/**
 * PostCategory Schema
 * @public
 */
PostCategory.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    note: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    slug: {
        type: DataTypes.STRING(255),
        unique: true,
        required: true
    },
    logo: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    path: {
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    position: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    parent_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    normalize_path: {
        type: DataTypes.STRING(255),
        defaultValue: ''
    },

    // seo
    meta_url: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_title: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_keyword: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_description: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },


    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'post_category',
    tableName: 'tbl_post_categories'
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
PostCategory.addHook('beforeCreate', async (model) => {
    model.parent_id = model.parent_id ?
        model.parent_id :
        null;

    if (model.parent_id !== null) {
        model.path = await PostCategory.getParentPath(model.parent_id);
        model.normalize_path = model.path.join(',');
    }

    return model;
});

/**
 * Register event emiter
 */
PostCategory.Events = {
    POST_CATEGORY_CREATED: `${serviceName}.post_category.created`,
    POST_CATEGORY_UPDATED: `${serviceName}.post_category.updated`,
    POST_CATEGORY_DELETED: `${serviceName}.post_category.deleted`,
};

PostCategory.addHook('afterCreate', async (model) => {
    await model.update({ slug: `${convertToEn(model.name.split(' ').join('-'))}-i.${model.id}` });
});

PostCategory.addHook('afterUpdate', () => {});

PostCategory.addHook('afterDestroy', (data) => {
    eventBus.emit(PostCategory.Events.POST_CATEGORY_DELETED, data);
});

/**
 * Register event name
 */
PostCategory.Events = {
    CATEGORY_CREATED: `${serviceName}.category.created`,
    CATEGORY_UPDATED: `${serviceName}.category.updated`,
    CATEGORY_DELETED: `${serviceName}.category.deleted`
};
PostCategory.EVENT_SOURCE = `${serviceName}.category`;

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);

    // TODO: load condition
    const { id, keyword, nested, is_visible, } = options;

    if (id && nested) {
        options[Op.or] = [
            { id: id },
            {
                path: {
                    [Op.contains]: [id]
                }
            }
        ];
    }
    delete options.id;
    delete options.nested;

    if (keyword) {
        // set keyword option
        options.name = {
            [Op.iLike]: `%${keyword}%`
        };
    }
    delete options.keyword;


    if (is_visible) {
        // set keyword option
        options.is_visible = {
            [Op.eq]: is_visible
        };
    }

    checkMinMaxOfConditionFields(options, 'created_at', 'Date');


    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'name':
            sort = ['name', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        case 'position':
            sort = ['position', order_by];
            break;
        default:
            sort = ['created_at', 'ASC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 * @param {*} model
 */
PostCategory.transform = (model) => {
    const transformed = {};
    const fields = [
        // attribute
        'id',
        'name',
        'note',
        'slug',
        'logo',
        'image',
        'path',
        'position',
        'parent_id',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',

        // manager
        'is_active',
        'is_visible',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    fields.forEach((field) => {
        transformed[field] = model[field];
    });

    // add additional field
    transformed.children = [];

    transformed.created_at = moment(model.created_at).unix();
    transformed.updated_at = moment(model.updated_at).unix();

    return transformed;
};

/**
 * Get all changed properties
 */
PostCategory.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        // attribute
        'name',
        'note',
        'slug',
        'logo',
        'image',
        'position',
        'is_visible'
    ];

    /** get all changable properties */
    Object.keys(newModel).forEach((field) => {
        if (includes(allChangableProperties, field)) {
            changedProperties.push(field);
        }
    });

    /** get data changed */
    const dataChanged = [];
    changedProperties.forEach(field => {
        if (!isEqual(newModel[field], oldModel[field])) {
            dataChanged.push(field);
        }
    });
    return dataChanged;
};

/**
 * Get attribute
 *
 * @param {ObjectId} id - The id of attribute.
 * @returns {Promise<Attribute, APIError>}
 */
PostCategory.get = async (id) => {
    try {
        const category = await PostCategory.findByPk(id);
        if (!category) {
            throw new APIError({
                message: `Không tìm thấy danh mục bài viết ${id}`,
                status: httpStatus.NOT_FOUND
            });
        }
        return category;
    } catch (error) {
        throw error;
    }
};

/**
 * List categories in descending order of 'created_at' timestamp.
 *
 * @param {number} skip - Number of categories to be skipped.
 * @param {number} limit - Limit number of categories to be returned.
 * @returns {Promise<Attribute[]>}
 */
PostCategory.list = async ({
    id,
    nested,
    keyword,
    is_active,
    is_visible,
    min_created_at,
    max_created_at,
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        id,
        nested,
        keyword,
        min_created_at,
        max_created_at,
        is_active,
        is_visible,
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return PostCategory.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of attrs to be skipped.
 * @param {number} limit - Limit number of attrs to be returned.
 * @returns {Promise<Number>}
 */
PostCategory.totalRecords = ({
    id,
    nested,
    keyword,
    is_active,
    is_visible,
    min_created_at,
    max_created_at,
}) => {
    const options = filterConditions({
        id,
        nested,
        keyword,
        is_active,
        is_visible,
        min_created_at,
        max_created_at,
    });

    return PostCategory.count({
        where: options
    });
};

/**
 * Get path from parent
 * @param {*} parentId
 */
PostCategory.getParentPath = async (parent_id) => {
    if (!parent_id) {
        return [];
    }
    const parentCategory = await PostCategory.get(parent_id);
    const path = parentCategory.path.slice(0);
    path.push(parentCategory.id);
    return path;
};

/**
 * @typedef PostCategory
 */
export default PostCategory;
