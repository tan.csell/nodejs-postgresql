/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';

import { pick, isEqual, values, omitBy, isNil, isUndefined } from 'lodash';
import moment from 'moment-timezone';

import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';
import eventBus from '../services/event-bus';

/**
 * Promotion Schema
 * @public
 */
class Promotion extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'code',
    'name',
    'type',
    'status',
    'content',
    'position',
    'products',
    'image_layer',
    'thumbnail_url',
    'applied_stop_time',
    'applied_start_time',
    'applied_discount_condition'
];

const CONDITION_FIELDS = [
    'min_quantity',
    'max_quantity',
    'discount_type',
    'discount_rate',
    'discount_value'
];

const PRODUCT_FIELDS = [
    'id',
    'sku',
    'type',
    'name',
    'position',
    'option_id',
    'thumbnail_url',
    'image_layer',
    'min_quantity',
    'max_quantity',
    'total_quantity',
    'discount_rate',
    'discount_value',
    'original_price',
    'normal_price',
    'price',
    'discount_status',
    'total_stock_quantity',
    'total_available_quantity',
];

Promotion.Types = {
    COMBO_DEAL: 'combo_deal',
    FLASH_DEAL: 'flash_deal',
    HOT_DEAL: 'hot_deal'
};

Promotion.Statuses = {
    DRAF: 'darf',
    PENDING: 'pending',
    STARTING: 'starting',
    STOPPING: 'stopping',
    FINISHED: 'finished',
    FAILURED: 'failured',
    CANCELLED: 'cancelled'
};

Promotion.NameStatuses = {
    DRAF: 'Lưu tạm',
    PENDING: 'Sắp diễn ra',
    STARTING: 'Đang diễn ra',
    STOPPING: 'Đang tạm dừng',
    FINISHED: 'Đã kết thúc',
    FAILURED: 'Đã gặp sự cố',
    CANCELLED: 'Đã huỷ'
};

/**
 * Init Schema
 */
Promotion.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    type: {
        // name: 'Lọai hình khuyến mãi'
        type: DataTypes.STRING(255),
        values: values(Promotion.Types),
        allowNull: false
    },
    code: {
        type: DataTypes.STRING(25),
        defaultValue: null
    },
    name: {
        type: DataTypes.STRING(155),
        allowNull: false
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    position: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    },
    status: {
        type: DataTypes.STRING(10),
        values: values(Promotion.Statuses),
        defaultValue: Promotion.Statuses.PENDING
    },
    status_name: {
        type: DataTypes.STRING(50),
        values: values(Promotion.NameStatuses),
        defaultValue: Promotion.NameStatuses.PENDING
    },
    products: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    thumbnail_url: {
        // name: 'Hình ảnh quảng cáo'
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    image_layer: {
        // name: 'Hình ảnh'
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    applied_start_time: {
        // name: 'Thời gian bắt đầu'
        type: DataTypes.DATE,
        allowNull: false
    },
    applied_stop_time: {
        // name: 'Thời gian kết thúc'
        type: DataTypes.DATE,
        allowNull: false
    },
    applied_discount_condition: {
        // name: 'Điều kiện khuyến mại'
        type: DataTypes.JSONB,
        allowNull: true
    },
    normalize_product: {
        type: DataTypes.TEXT,
        allowNull: true
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    cancelled_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    cancelled_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'promotion',
    tableName: 'tbl_promotions'
});

/**
 * Register event emiter
 */
Promotion.Events = {
    PROMOTION_CREATED: `${serviceName}.promotion.created`,
    PROMOTION_UPDATED: `${serviceName}.promotion.updated`,
    PROMOTION_DELETED: `${serviceName}.promotion.deleted`
};
Promotion.EVENT_SOURCE = `${serviceName}.promotion`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Promotion.addHook('afterCreate', async (model) => {
    await model.update({ code: `KM000000${model.id}` });
});

Promotion.addHook('afterUpdate', (data) => {
    eventBus.emit(Promotion.Events.PROMOTION_UPDATED, data);
});

Promotion.addHook('afterDestroy', () => {});


/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type) {
    console.log(field, type);
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }
    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.type) {
        options.type = {
            [Op.like]: `${options.type}`
        };
    }

    if (options.keyword) {
        options[Op.or] = [{
            code: {
                [Op.iLike]: `%${options.keyword}%`
            }
        },
        {
            name: {
                [Op.iLike]: `%${options.keyword}%`
            }
        }
        ];
    }
    delete options.keyword;

    if (options.product_sku) {
        options.products = {
            [Op.contains]: [{
                sku: options.product_sku
            }]
        };
    }
    delete options.product_sku;

    if (options.product_name) {
        options.products = {
            [Op.contains]: [{
                name: options.product_name
            }]
        };
    }
    delete options.product_name;

    if (options.product_keyword) {
        options.normalize_product = {
            [Op.iLike]: `%${options.product_keyword}%`
        };
    }
    delete options.product_keyword;

    if (options.min_start_time) {
        options.applied_stop_time = {
            [Op.gte]: options.min_start_time
        };
    }
    delete options.min_start_time;

    if (options.max_start_time) {
        options.applied_start_time = {
            [Op.lte]: options.max_start_time
        };
    }
    delete options.max_start_time;

    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;

    // date filters
    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'name':
            sort = ['name', order_by];
            break;
        case 'position':
            sort = ['position', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Promotion.transform = (params, includeRestrictedFields = true) => {
    const transformed = {};
    const fields = [
        'id',
        'code',
        'name',
        'content',
        'thumbnail_url',
        'image_layer',
        'applied_stop_time',
        'applied_start_time'
    ];

    if (includeRestrictedFields) {
        const privateFields = [
            'type',
            'status',
            'products',
            'position',
            'status_name',
            'created_by',
            'created_at',
            'updated_at',
            'updated_by',
            'applied_discount_condition',
        ];
        fields.push(...privateFields);
    }

    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',
        'applied_stop_time',
        'applied_start_time'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Promotion.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'name',
        'content',
        'position',
        'products',
        'image_layer',
        'thumbnail_url',
        'applied_stop_time',
        'applied_start_time',
        'applied_discount_condition',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Get Promotion By Id
 *
 * @public
 * @param {String} id
 */
Promotion.get = async (id) => {
    try {
        const promotion = await Promotion.findByPk(
            id
        );
        if (!promotion) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy chương trình khuyến mại: ${id}`
            });
        }
        return promotion;
    } catch (error) {
        throw (error);
    }
};

/**
 * List records in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Supplider[]>}
 */
Promotion.list = async ({
    // page
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
    type,
    code,
    keyword,
    statuses,
    is_active,
    is_visible,
    product_sku,
    product_name,
    min_created_at,
    max_created_at,
    min_start_time,
    max_start_time,
    product_keyword
}) => {
    const options = filterConditions({
        type,
        code,
        keyword,
        statuses,
        is_active,
        is_visible,
        product_sku,
        product_name,
        min_created_at,
        max_created_at,
        min_start_time,
        max_start_time,
        product_keyword
    });
    const sort = sortConditions({ sort_by, order_by });
    return Promotion.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
Promotion.totalRecords = ({
    type,
    code,
    keyword,
    statuses,
    is_active,
    is_visible,
    product_sku,
    product_name,
    min_created_at,
    max_created_at,
    min_start_time,
    max_start_time,
    product_keyword
}) => {
    const options = filterConditions({
        type,
        code,
        keyword,
        statuses,
        is_active,
        is_visible,
        product_sku,
        product_name,
        min_created_at,
        max_created_at,
        min_start_time,
        max_start_time,
        product_keyword
    });
    return Promotion.count({ where: options });
};

/**
 * Filter only allowed fields from promotion
 * @param {String} collum
 * @param {Object} params
 */
Promotion.filterParams = (collum, params) => {
    if (collum === 'PRODUCT') {
        return pick(params, PRODUCT_FIELDS);
    }
    if (collum === 'CONDITION') {
        return pick(params, CONDITION_FIELDS);
    }
    return pick(params, PUBLIC_FIELDS);
};

/**
 * @typedef Promotion
 */
export default Promotion;
