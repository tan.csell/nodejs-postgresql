/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isNaN, isNil, isUndefined, omitBy, parseInt, pick } from 'lodash';
import moment from 'moment-timezone';
import APIError from '../utils/APIError';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';

/**
 * Create connection
 */
class Author extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'slug',
    'title',
    'content',
    'meta_url',
    'meta_title',
    'meta_image',
    'meta_keyword',
    'meta_description',
    'is_visible',
];

/**
 * Author Schema
 * @public
 */
Author.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    slug: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: false
    },

    // seo
    meta_url: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_title: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_keyword: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_description: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },

    // manager
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'author',
    tableName: 'tbl_authors'
});

/**
 * Register event emiter
 */
Author.Events = {
    AUTHOR_CREATED: `${serviceName}.author.created`,
    AUTHOR_UPDATED: `${serviceName}.author.updated`,
    AUTHOR_DELETED: `${serviceName}.author.deleted`,
};
Author.EVENT_SOURCE = `${serviceName}.author`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Author.addHook('afterCreate', () => {});

Author.addHook('afterUpdate', () => {});

Author.addHook('afterDestroy', () => {});

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.keyword) {
        options.title = {
            [Op.iLike]: `%${options.keyword}%`
        };
    }
    delete options.keyword;

    checkMinMaxOfConditionFields(options, 'created_at', 'Date');
    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Author.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'slug',
        'title',
        'content',
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',
        'is_visible',
        'is_active',
        'created_by',
        'updated_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Author.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'slug',
        'title',
        'content',
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'is_visible',
        'meta_description',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} slug
 */
Author.get = async (id) => {
    try {
        const data = await Author.findOne({
            where: {
                [Op.or]: [{
                    id: !isNaN(+id) && `${id}`.length < 10 ?
                        parseInt(id, 0) : null
                },
                {
                    slug: id.toString()
                },
                ]
            },

        });
        if (!data) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Đường dẫn không tồn tại'
            });
        }
        return data;
    } catch (ex) {
        throw ex;
    }
};

/**
 * List records in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Supplider[]>}
 */
Author.list = async ({
    keyword,
    is_visible,
    min_created_at,
    max_created_at,

    // page
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        keyword,
        is_visible,
        min_created_at,
        max_created_at
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Author.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
Author.totalRecords = ({
    keyword,
    is_visible,
    min_created_at,
    max_created_at
}) => {
    const options = filterConditions({
        keyword,
        is_visible,
        min_created_at,
        max_created_at
    });
    return Author.count({ where: options });
};

/**
 * Check Duplicate
 *
 * @public
 */
Author.checkDuplicate = (error) => {
    const execption = error.errors[0];
    if (execption.type === 'unique violation') {
        return new APIError({
            message: 'Slug của trang đã tồn tại!',
            errors: [{
                field: 'slug',
                location: 'body',
                messages: ['"slug" đã tồn tại. Vui lòng thử lại!']
            }],
            stack: execption.stack,
            status: httpStatus.CONFLICT
        });
    }
    return error;
};


/**
 * Filter only allowed fields from Author
 *
 * @param {Object} params
 */
Author.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Author
 */
export default Author;
