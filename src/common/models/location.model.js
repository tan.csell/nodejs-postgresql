import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isNaN, isNil, isUndefined, omitBy, pick } from 'lodash';
import moment from 'moment-timezone';
import { serviceName } from '../../config/vars';
import postgres from '../../config/postgres';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
const { sequelize } = postgres;
class Location extends Model {}

const PUBLIC_FIELDS = [
    'name',
    'code',
    'providers'
];

const LocationTypes = {
    PROVINCE: 'province',
    DISTRICT: 'district',
    WARD: 'ward'
};

/**
 * Location Schema
 * @public
 */
Location.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(155),
        allowNull: false
    },
    code: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    type: {
        type: DataTypes.STRING(100),
        defaultValue: LocationTypes.PROVINCE
    },
    parent_id: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    // 3RD Location
    providers: {
        type: DataTypes.ARRAY(DataTypes.JSONB),
        defaultValue: []
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'location',
    tableName: 'tbl_locations'
});

/**
 * Register event emiter
 */
Location.Events = {
    LOCATION_CREATED: `${serviceName}.location.created`,
    LOCATION_UPDATED: `${serviceName}.location.updated`,
    LOCATION_DELETED: `${serviceName}.location.deleted`,
};
Location.EVENT_SOURCE = `${serviceName}.location`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Location.addHook('afterCreate', () => {});

Location.addHook('afterUpdate', () => {});

Location.addHook('afterDestroy', () => {});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    // TODO: load condition
    if (options.name) {
        options.name = {
            [Op.iLike]: `%${options.name}%`
        };
    }

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Location.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'name',
        'code',
        'providers'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Location.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'code',
        'name',
        'parent_id',
        'providers'
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Location.get = async (id) => {
    try {
        const data = await Location.findOne({
            where: {
                [Op.or]: [
                    { code: id },
                    { id: isNaN(id) ? null : id },
                ],
                is_active: true
            }
        });
        if (!data) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy địa chỉ!'
            });
        }
        return data;
    } catch (ex) {
        throw ex;
    }
};

/**
 * List users in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Supplider[]>}
 */
Location.list = async ({
    name,
    parent_id,
    types,
    sort_by,
    order_by,
    skip = 0,
    limit = 100,
}) => {
    const options = filterConditions({
        name,
        parent_id,
        types,
    });
    const sort = sortConditions({ sort_by, order_by });
    return Location.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
Location.totalRecords = ({
    name,
    parent_id,
    types,
}) => {
    const options = filterConditions({
        name,
        parent_id,
        types,
    });

    return Location.count({ where: options });
};

/**
 * Filter only allowed fields from Location
 *
 * @param {Object} params
 */
Location.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Location
 */
export default Location;
