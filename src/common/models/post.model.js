/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { DataTypes, Model, Op } from 'sequelize';
import { isEqual, includes, pick, values, omitBy, isNil } from 'lodash';
import eventBus from '../services/event-bus';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';

const CATEGORY_FIELDS = [
    'id',
    'name',
    'slug'
];

/**
 * Create connection
 */
const sequelize = postgres.connect();
class Post extends Model {}

Post.Types = {
    BLOG: 'blog'
};

/**
 * Post Schema
 * @public
 */
Post.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    type: {
        type: DataTypes.STRING(24),
        values: values(Post.Types),
        required: true
    },
    title: {
        type: DataTypes.STRING(255),
        required: true
    },
    slug: {
        type: DataTypes.STRING(255),
        unique: true,
        required: true
    },
    products: {
        type: DataTypes.JSONB,
        deefaultValue: []
    },
    avatar: {
        type: DataTypes.STRING(255),
        required: true
    },
    description: {
        type: DataTypes.TEXT,
        required: true
    },
    content: {
        type: DataTypes.TEXT,
        required: true
    },
    categories: {
        type: DataTypes.JSONB,
        required: true,
        defaultValue: [{
            id: null,
            name: null,
            slug: null
        }]
    },
    normalize_category: {
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
    },
    hashtag: {
        type: DataTypes.ARRAY(DataTypes.STRING(24)),
        defaultValue: []
    },
    position: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },

    // metric
    like_count: {
        type: DataTypes.INTEGER,
        min: 0,
        default: 0
    },
    view_count: {
        type: DataTypes.INTEGER,
        min: 0,
        default: 0
    },

    // social seo
    breadcrumbs: {
        type: DataTypes.ARRAY(DataTypes.JSONB),
        defaultValue: []
    },
    meta_url: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_title: {
        type: DataTypes.STRING(155),
        defaultValue: null
    },
    meta_image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_keyword: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_description: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },

    // manager
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_home_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_favorite_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'post',
    tableName: 'tbl_posts'
});

/**
 * Register event emiter
 */
Post.EVENT_SOURCE = `${serviceName}.post`;
Post.Events = {
    POST_CREATED: `${serviceName}.post.created`,
    POST_UPDATED: `${serviceName}.post.updated`,
    POST_DELETED: `${serviceName}.post.deleted`
};

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Post.addHook('afterCreate', (model) => {
    eventBus.emit(Post.Events.POST_CREATED, model);
});

Post.addHook('afterUpdate', () => {});

Post.addHook('afterDestroy', () => {});

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);

    if (options.keyword) {
        options.title = {
            [Op.iLike]: `%${options.keyword}%`
        };
    }
    delete options.keyword;

    if (options.categories) {
        options.normalize_category = {
            [Op.overlap]: options.categories.split(',')
        };
    }
    delete options.categories;

    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'title':
            sort = ['title', order_by];
            break;
        case 'position':
            sort = ['position', order_by];
            break;
        case 'view_count':
            sort = ['view_count', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform mongoose model to expose object
 */
Post.transform = (model) => {
    const transformed = {};
    const fields = [
        'id',
        'type',
        'title',
        'slug',
        'avatar',
        'content',
        'description',
        'categories',
        'hashtag',
        'position',
        'products',

        // metric
        'like_count',
        'view_count',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',

        // manager
        'is_visible',
        'is_home_visible',
        'is_favorite_visible',
        'is_active',
        'created_by',
        'created_at',
        'updated_at'
    ];

    fields.forEach((field) => {
        transformed[field] = model[field];
    });

    transformed.created_at = moment(model.created_at).unix();
    transformed.updated_at = moment(model.updated_at).unix();
    return transformed;
};

/**
 * Get all changed properties
 *
 * @public
 * @param {Object} data newModel || oleModel
 */
Post.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'title',
        'slug',
        'avatar',
        'content',
        'products',
        'description',
        'categories',
        'hashtag',
        'position',
        'normalize_category',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',

        // manager
        'is_visible',
        'is_home_visible',
        'is_favorite_visible',
        'is_active',
        'updated_at'
    ];

    /** get all changable properties */
    allChangableProperties.forEach((field) => {
        if (includes(allChangableProperties, field)) {
            changedProperties.push(field);
        }
    });

    /** get data changed */
    const dataChanged = [];
    changedProperties.forEach(field => {
        if (!isEqual(newModel[field], oldModel[field])) {
            dataChanged.push(field);
        }
    });
    return dataChanged;
};

/**
 * Get Post By Id
 *
 * @public
 * @param {String} postId
 */
Post.get = async (postId) => {
    try {
        const post = await Post.findByPk(postId);
        if (!post) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy bài viết ${postId}`
            });
        }
        if (post) {
            await post.increment({
                view_count: 1
            });
        }
        return post;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * List records
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Post[]>}
 */
Post.list = async ({
    // sort
    skip = 0,
    limit = 20,
    sort_by,
    order_by,

    // search
    keyword,
    is_visible,
    is_home_visible,
    is_favorite_visible,
    min_created_at,
    max_created_at,
    categories,
}) => {
    const options = filterConditions({
        keyword,
        is_visible,
        is_home_visible,
        is_favorite_visible,
        min_created_at,
        max_created_at,
        categories
    });
    const sort = sortConditions({ sort_by, order_by });
    return Post.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
Post.totalRecords = ({
    keyword,
    is_visible,
    is_home_visible,
    is_favorite_visible,
    min_created_at,
    max_created_at,
    categories
}) => {
    const options = filterConditions({
        keyword,
        is_visible,
        is_home_visible,
        is_favorite_visible,
        min_created_at,
        max_created_at,
        categories
    });
    return Post.count({
        where: options
    });
};

/**
 * Filter only allowed fields from import
 *
 * @param {Object} params
 */
Post.filterParams = (params) => pick(params, CATEGORY_FIELDS);

/**
 * @typedef Post
 */
export default Post;
