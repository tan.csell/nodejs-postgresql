/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { Model, DataTypes, Op } from 'sequelize';
import { values, includes, isEqual, isNil, omitBy, pick, parseInt } from 'lodash';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
const sequelize = postgres.connect();
class Cart extends Model {}

const PUBLIC_FIELDS = [
    'note',
    'store',
    'customer',
    'products',
    'payments',
    'deliveries',
    'source',

    // amount
    'total_coin',
    'total_point',
    'total_quantity',
    'total_return_quantity',
    'total_price_before_discount',
    'total_price_after_discount',
    'total_discount_value',
    'total_exchange_price',
    'total_shipping_fee',
    'total_return_fee',
    'total_price',
    'total_paid',
    'total_unpaid',

    // config
    'is_favorite',
    'is_delivery',
    'is_warning',
];

Cart.Statuses = {
    PICKING: 'picking',
    PENDING: 'pending',
    CANCELLED: 'cancelled',
    CONFIRMED: 'confirmed',
    FAILURED: 'failured'
};

Cart.NameStatuses = {
    PICKING: 'Đang mua',
    PENDING: 'Đang xử lý',
    CANCELLED: 'Đã huỷ',
    CONFIRMED: 'Đã duyệt',
    FAILURED: 'Đơn lỗi'
};

Cart.Sources = {
    SYSTEM: 'system',
    FACEBOOK: 'facebook',
    GOOGLE: 'google',
    OTHER: 'other'
};

/**
 * Cart Schema
 * @private
 */
Cart.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    code: {
        type: DataTypes.STRING(24),
        defaultValue: null
    },
    type: {
        type: DataTypes.STRING(10),
        defaultValue: 'order'
    },
    note: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    store: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    source: {
        type: DataTypes.STRING(50),
        values: values(Cart.Sources),
        defaultValue: Cart.Sources.ERP
    },
    customer: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    hashtag: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    channel: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    price_book: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    status: {
        type: DataTypes.STRING(25),
        values: values(Cart.Statuses),
        defaultValue: Cart.Statuses.PICKING
    },
    status_name: {
        type: DataTypes.STRING(50),
        values: values(Cart.Statuses),
        defaultValue: Cart.NameStatuses.PICKING
    },
    payments: {
        type: DataTypes.JSONB,
        defaultValue: []
    },
    deliveries: {
        type: DataTypes.JSONB,
        defaultValue: []
    },
    discounts: {
        type: DataTypes.JSONB,
        defaultValue: []
    },
    products: {
        type: DataTypes.JSONB,
        defaultValue: []
    },
    total_coin: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    total_point: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    total_quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    total_return_quantity: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    total_price_before_discount: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_price_after_discount: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_discount_value: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_exchange_price: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_shipping_fee: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_return_fee: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_price: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_paid: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_unpaid: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_delivery: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_favorite: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    client_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    cancelled_at: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    cancelled_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    confirmed_at: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    confirmed_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    completed_at: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    completed_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    tableName: 'tbl_carts'
});

/**
 * Register event emiter
 */
Cart.EVENT_SOURCE = `${serviceName}.cart`;
Cart.Events = {
    CART_CREATED: `${serviceName}.cart.created`,
    CART_UPDATED: `${serviceName}.cart.updated`,
    CART_CONFIRMED: `${serviceName}.cart.confirmed`
};

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else if (type === 'Time') {
        _min = new Date(options[`min_${field}`]);
        _min = _min.setHours(_min.getHours(), _min.getMinutes(), 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max = _max.setHours(_max.getHours(), _max.getMinutes(), 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);

    if (options.stores) {
        options['store.id'] = {
            [Op.in]: options.stores.split(',')
        };
    }
    delete options.stores;

    if (options.customer) {
        options['customer.phone'] = {
            [Op.iLike]: options.customer
        };
    }
    delete options.customer;

    if (options.product_name) {
        options.products = {
            [Op.contains]: [{ name: options.product_name }]
        };
    }
    delete options.product_name;

    if (options.product_sku) {
        options.products = {
            [Op.contains]: [{ sku: options.product_sku.toLowerCase() }]
        };
    }
    delete options.product_sku;

    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;

    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Cart.transform = (params, includeRestrictedFields = false) => {
    const transformed = {};
    const fields = [
        'id',
        'code',
        'type',
        'note',
        'source',
        'payment',
        'shipping',
        'products',
        'payments',
        'deliveries'
    ];

    // check private
    if (includeRestrictedFields) {
        const privateFields = [
            'store',
            'source',
            'customer',
            'status',
            'status_name',
            'is_delivery',
            'is_favorite',
            'is_warning',
            'created_by',
            'created_at',
            'updated_at',
            'updated_by',
            'confirmed_by',
            'confirmed_at',
            'completed_by',
            'completed_at',
            'cancelled_by',
            'cancelled_at',
        ];
        fields.push(...privateFields);
    }

    // trasnform
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe decimal
    const decimalFields = [
        'total_point',
        'total_coin',
        'total_quantity',
        'total_return_quantity',
        'total_price_before_discount',
        'total_price_after_discount',
        'total_discount_value',
        'total_exchange_price',
        'total_shipping_fee',
        'total_return_fee',
        'total_price',
        'total_paid',
        'total_unpaid'
    ];
    decimalFields.forEach((field) => {
        transformed[field] = parseInt(params[field], 0);
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',
        'confirmed_at',
        'completed_at',
        'cancelled_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};


/**
 * Get all changed properties
 *
 * @public
 * @param {Object} data newModel || oleModel
 */
Cart.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        // attributes
        'note',
        'payment',
        'shipping',
        'customer',
        'products',
        'payments',
        'deliveries',

        // amount
        'total_coin',
        'total_point',
        'total_quantity',
        'total_return_quantity',
        'total_price_before_discount',
        'total_price_after_discount',
        'total_discount_value',
        'total_exchange_price',
        'total_shipping_fee',
        'total_return_fee',
        'total_price',
        'total_paid',
        'total_unpaid',

        // config
        'is_favorite',
        'is_delivery',
        'is_warning',
    ];

    // get all changable properties
    Object.keys(newModel).forEach((field) => {
        if (includes(allChangableProperties, field)) {
            changedProperties.push(field);
        }
    });

    // get data changed
    const dataChanged = [];
    changedProperties.forEach(field => {
        if (!isEqual(newModel[field], oldModel[field])) {
            dataChanged.push(field);
        }
    });
    return dataChanged;
};


/**
 * Detail
 *
 * @public
 * @param {string} client_id
 */
Cart.get = async (client_id) => {
    try {
        const order = await Cart.findOne({
            where: { client_id, status: Cart.Statuses.PICKING }
        });
        return order;
    } catch (error) {
        throw error;
    }
};

/**
 * Get list order
 *
 * @public
 * @param {Parameters} params
 */
Cart.list = async ({
    // search
    id,
    note,
    status,
    hashtag,
    customer,
    product_sku,
    product_name,
    min_created_at,
    max_created_at,

    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        id,
        note,
        status,
        hashtag,
        customer,
        product_sku,
        product_name,
        min_created_at,
        max_created_at,
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Cart.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total quantity items list records
 *
 * @public
 * @param {Parameters} params
 */
Cart.totalRecords = async ({
    id,
    note,
    status,
    hashtag,
    customer,
    product_sku,
    product_name,
    min_created_at,
    max_created_at,
}) => {
    try {
        const options = filterConditions({
            id,
            note,
            status,
            hashtag,
            customer,
            product_sku,
            product_name,
            min_created_at,
            max_created_at,
        });

        return Cart.count({
            where: options
        });
    } catch (ex) {
        throw ex;
    }
};

/**
 * Check Duplicate clientId
 *
 * @public
 * @param clientId
 */
Cart.checkDuplicate = async (clientId) => {
    try {
        const cart = await Cart.findOne({
            where: {
                client_id: clientId,
                status: Cart.Statuses.PENDING
            }
        });
        if (cart) {
            throw new APIError({
                status: httpStatus.CONFLICT,
                message: `Giỏ hàng bị trùng client_id ${clientId} !`
            });
        }
        return true;
    } catch (ex) {
        throw (ex);
    }
};


/**
 * Filter only allowed fields from cart
 *
 * @param {Object} params
 */
Cart.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Cart
 */
export default Cart;
