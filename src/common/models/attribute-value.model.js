/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isNil, isUndefined, omitBy, pick } from 'lodash';
import moment from 'moment-timezone';

import { serviceName } from '../../config/vars';
import postgres from '../../config/postgres';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
const { sequelize } = postgres;
class AttributeValue extends Model {}

const PUBLIC_FIELDS = [
    'id',
    'slug',
    'icon',
    'name',
    'value',
    'content',
    'position',
    'system_id',
    'is_visible',
    'attribute_id',
    'attribute_code',
    'attribute_group',
    'meta_url',
    'meta_title',
    'meta_image',
    'meta_keyword',
    'meta_description',
];

/**
 * Product Attribute Schema
 * @public
 */
AttributeValue.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    slug: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    icon: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    name: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    value: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    content: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    position: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    meta_url: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_title: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_image: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_keyword: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    meta_description: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    attribute_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    attribute_code: {
        type: DataTypes.STRING(155),
        defaultValue: null
    },
    attribute_group: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },

    // manager
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'attribute_value',
    tableName: 'tbl_attribute_values'
});

/**
 * Register event emiter
 */
AttributeValue.Events = {
    ATTRIBUTE_CREATED: `${serviceName}.attribute-value.created`,
    ATTRIBUTE_UPDATED: `${serviceName}.attribute-value.updated`,
    ATTRIBUTE_DELETED: `${serviceName}.attribute-value.deleted`,
};
AttributeValue.EVENT_SOURCE = `${serviceName}.attribute-value`;

/**
 * Converter
 * @param {*} str
 */
function convertToEn(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
    str = str.replace(/Đ/g, 'D');
    str = str.toLowerCase();
    return str;
}

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
AttributeValue.addHook('beforeCreate', () => {});

AttributeValue.addHook('afterCreate', async (data) => {
    if (data.value) {
        const parseName = convertToEn(data.value);
        await AttributeValue.update({
            slug: `${parseName.split(' ').join('-')}-i.${data.id}`
        }, {
            where: {
                id: data.id
            }
        });
    }
});

AttributeValue.addHook('afterUpdate', () => {});

AttributeValue.addHook('afterDestroy', () => {});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    // TODO: load condition

    if (options.attribute_value) {
        options.value = {
            [Op.iLike]: `%${options.attribute_value}%`
        };
    }
    delete options.attribute_value;

    if (options.systems) {
        options.system_id = {
            [Op.in]: options.systems.split(',')
        };
    }
    delete options.systems;


    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        case 'position':
            sort = ['position', order_by];
            break;
        case 'name':
            sort = ['name', order_by];
            break;
        default:
            sort = ['position', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
AttributeValue.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'slug',
        'icon',
        'count',
        'name',
        'value',
        'content',
        'position',
        'is_visible',
        'attribute_id',
        'attribute_code',
        'attribute_group',
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
AttributeValue.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'slug',
        'icon',
        'name',
        'value',
        'content',
        'position',
        'system_id',
        'is_visible',
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description'
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
AttributeValue.get = async (id) => {
    try {
        const data = await AttributeValue.findOne({
            where: {
                id,
                is_active: true
            }
        });
        if (!data) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy thuộc tính này'
            });
        }
        return data;
    } catch (ex) {
        throw ex;
    }
};

/**
 * List records in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Supplider[]>}
 */
AttributeValue.list = async ({
    attribute_group,
    attribute_value,
    attribute_code,
    is_visible,
    systems,

    // paging
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        attribute_group,
        attribute_value,
        attribute_code,
        is_visible,
        systems
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return AttributeValue.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
AttributeValue.totalRecords = async ({
    attribute_group,
    attribute_value,
    attribute_code,
    is_visible,
    systems
}) => {
    const options = filterConditions({
        attribute_group,
        attribute_value,
        attribute_code,
        is_visible,
        systems
    });
    return AttributeValue.count({
        where: options
    });
};

/**
 * Filter only allowed fields from AttributeValue
 *
 * @param {Object} params
 */
AttributeValue.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef AttributeValue
 */
export default AttributeValue;
