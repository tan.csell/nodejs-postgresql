import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isNil, isUndefined, omitBy, pick, values } from 'lodash';
import moment from 'moment-timezone';
import APIError from '../utils/APIError';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import AttributeValue from './attribute-value.model';

/**
 * Create connection
 */
class Attribute extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'code',
    'icon',
    'type',
    'name',
    'group',
    'require',
    'is_visible'
];

Attribute.Groups = {
    CUSTOMER_TYPE: 'customer_type', // khách hàng -> Loại khách
    CUSTOMER_GENDER: 'customer_gender', // khách hàng -> Giới tính
    CUSTOMER_STATUS: 'customer_status', // khách hàng -> Trạng thái

    PRODUCT_TYPE: 'product_type', // hàng hóa -> Loại hàng
    PRODUCT_STOCK: 'product_stock', // hàng hóa -> Tồn kho
    PRODUCT_STATUS: 'product_status', // hàng hóa -> Trạng thái
    PRODUCT_DISPLAY: 'product_direct_sale', // hàng hóa -> hiển thị
    PRODUCT_VARIATION: 'product_variation', // hàng hóa -> Phân loại hàng
    PRODUCT_ATTRIBUTE: 'product_attribute', // hàng hóa -> Thuộc tính

    ORDER_LIST_CHANNEL: 'order_list_channel', // đặt hàng -> Kênh bán
    ORDER_ORDER_STATUS: 'order_order_status', // hóa đơn đặt hàng -> trạng thái
    ORDER_RETAIL_STATUS: 'order_retail_status', // hóa đơn bán lẻ -> trạng thái
    ORDER_RETURN_STATUS: 'order_return_status', // hóa đơn trả hàng -> trạng thái

    PAYMENT_TYPE: 'payment_type', // sổ quỹ -> Loại thu chi
    PAYMENT_CARD: 'payment_card', // sổ quỹ -> Số tài khoản
    PAYMENT_GROUP: 'payment_group', // sổ quỹ -> Loại chứng từ
    PAYMENT_STATUS: 'payment_status', // sổ quỹ -> Trạng thái
    PAYMENT_METHOD: 'payment_method', // sổ quỹ -> Phương thức
    PAYMENT_PARTNER: 'payment_partner', // sổ quỹ -> Đối tượng

    DELIVERY_TYPE: 'delivery_type', // vận đơn -> Loại giao
    DELIVERY_STATUS: 'delivery_status', // vận đơn -> Trạng thái
    DELIVERY_PARTNER: 'delivery_partner', // vận đơn -> Đối tác giao hàng
    DELIVERY_PAYMENT_BY: 'delivery_payment_by', // vận đơn -> Người trả phí
    DELIVERY_RECEIVER_NOTE: 'delivery_receiver_note', // vận đơn -> Hình thức xem hàng
    DELIVERY_PARTNER_STATUS: 'delivery_partner_status', // vận đơn -> đối tác giao hàng -> Trạng thái

    STOCK_EXPORT_STATUS: 'export_status', // xuất hủy -> Trạng thái
    STOCK_IMPORT_STATUS: 'import_status', // nhập hàng -> Trạng thái
    STOCK_TAKE_STATUS: 'stock_take_status', // kiểm hàng -> Trạng thái
    STOCK_TRANSFER_STATUS: 'transfer_status', // chuyển hàng -> Trạng thái

    PROMOTION_DEAL_TYPE: 'promotion_deal_type', // khuyến mãi -> Loại chương trình
    PROMOTION_DEAL_STATUS: 'promotion_deal_status', // khuyến mãi -> Trạng thái
    PROMOTION_VOUCHER_STATUS: 'promotion_voucher_status', // voucher -> Trạng thái

    CONFIG_ROLE_STATUS: 'role_status', // chức vụ -> Trạng thái
    CONFIG_USER_STATUS: 'user_status', // người dùng -> Trạng thái
    CONFIG_STORE_STATUS: 'store_status', // chi nhánh -> Trạng thái

    SETTING_CMS_MENU: 'setting_cms_menu', // thiết lập -> menu cms
    SETTING_CMS_CONFIG: 'setting_cms_config', // thiết lập -> cau hinh cms
    SETTING_ERP_MENU: 'setting_erp_menu', // thiết lập -> menu erp
    SETTING_ERP_CONFIG: 'setting_erp_config', // thiết lập -> cau hinh erp
    SETTING_WEB_MENU: 'setting_web_menu', // thiết lập -> menu web
    SETTING_WEB_CONFIG: 'setting_web_config', // thiết lập -> cau hinh web
    SETTING_PERMISSION: 'setting_permission', // thiết lập -> phân quyền
    SETTING_PRODUCT_RULE: 'setting_product_rule', // thiết lập -> giao dịch
    SETTING_TRANSACTION_RULE: 'setting_transaction_rule', // thiết lập -> giao dịch
};

Attribute.Types = {
    TEXT: 'text',
    DATE: 'date',
    ARRAY: 'array',
    NUMBER: 'number',
    OBJECT: 'object',
    BOOLEAN: 'boolean'
};

/**
 * Attribute Schema
 * @public
 */
Attribute.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    code: {
        type: DataTypes.STRING(155),
        allowNull: false
    },
    icon: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    type: {
        type: DataTypes.STRING(155),
        values: values(Attribute.Types),
        defaultValue: Attribute.Types.TEXT
    },
    name: {
        type: DataTypes.STRING(155),
        allowNull: false
    },
    group: {
        type: DataTypes.STRING(50),
        values: values(Attribute.Groups),
        defaultValue: null
    },
    require: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },

    // manager
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'attribute',
    tableName: 'tbl_attributes'
});

/**
 * Add your
 * - foreignKey
 * - relations
 */
Attribute.hasMany(
    AttributeValue, {
        as: 'values',
        sourceKey: 'code',
        foreignKey: 'attribute_code',
        constraints: false
    }
);

/**
 * Register event emiter
 */
Attribute.Events = {
    ATTRIBUTE_CREATED: `${serviceName}.attribute.created`,
    ATTRIBUTE_UPDATED: `${serviceName}.attribute.updated`,
    ATTRIBUTE_DELETED: `${serviceName}.attribute.deleted`,
};
Attribute.EVENT_SOURCE = `${serviceName}.attribute`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Attribute.addHook('afterCreate', () => {});

Attribute.addHook('afterUpdate', () => {});

Attribute.addHook('afterDestroy', () => {});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.name) {
        options.name = {
            [Op.iLike]: `%${options.name}%`
        };
    }

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Attribute.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'code',
        'icon',
        'type',
        'name',
        'group',
        'values',
        'require',
        'is_visible',
        'is_active',
        'created_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Attribute.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'code',
        'icon',
        'type',
        'name',
        'group',
        'require',
        'is_visible'
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Attribute.get = async (id) => {
    try {
        const data = await Attribute.findOne({
            where: {
                id,
                is_active: true
            }
        });
        if (!data) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy thuộc tính này'
            });
        }
        return data;
    } catch (ex) {
        throw ex;
    }
};

/**
 * List records in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Supplider[]>}
 */
Attribute.list = async ({
    code,
    name,
    group,
    system_id,
    is_visible,

    // page
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        code,
        name,
        group,
        system_id,
        is_visible,
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Attribute.findAll({
        where: options,
        include: [{
            model: AttributeValue,
            as: 'values',
            attributes: ['id', 'name', 'value']
        }],
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
Attribute.totalRecords = ({
    code,
    name,
    group,
    system_id,
    is_visible,
}) => {
    const options = filterConditions({
        code,
        name,
        group,
        system_id,
        is_visible,
    });
    return Attribute.count({ where: options });
};

/**
 * Filter only allowed fields from Attribute
 *
 * @param {Object} params
 */
Attribute.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Attribute
 */
export default Attribute;
