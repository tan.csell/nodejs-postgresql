/* eslint-disable no-param-reassign */
import { Model, DataTypes, Op } from 'sequelize';
import { pick, isEqual, values, isNil, omitBy, isUndefined } from 'lodash';
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import postgres from '../../config/postgres';
import eventBus from '../services/event-bus';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';
import messages from '../../config/messages';

/**
 * Create connection
 */
class Store extends Model {}
const { sequelize } = postgres;

Store.Statuses = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
};

const PUBLIC_FIELDS = [
    'name',
    'logo',
    'phone',
    'email',
    'description',
    'is_visible',

    // store location
    'address',
    'province',
    'district',
    'ward',
];

/**
 * Store Schema
 * @public
 */
Store.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(155),
        allowNull: false
    },
    logo: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    phone: {
        type: DataTypes.STRING(20),
        defaultValue: null
    },
    email: {
        type: DataTypes.STRING(255),
        validate: { isEmail: true },
        defaultValue: null
    },
    address: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    description: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    condinate: {
        type: DataTypes.JSONB,
        defaultValue: [0, 0]
    },
    province: {
        type: DataTypes.JSONB,
        allowNull: false // id || name
    },
    district: {
        type: DataTypes.JSONB,
        allowNull: false // id || name
    },
    ward: {
        type: DataTypes.JSONB,
        allowNull: false // id || name
    },
    status: {
        type: DataTypes.STRING(10),
        values: values(Store.Statuses),
        defaultValue: Store.Statuses.ACTIVE,
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    created_by: {
        type: DataTypes.JSONB,
        allowNull: false,
        defaultValue: {
            id: null,
            name: null
        }
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    tableName: 'tbl_stores'
});

/**
 * Register event emiter
 */
Store.EVENT_SOURCE = `${serviceName}.store`;
Store.Events = {
    STORE_CREATED: `${serviceName}.store.created`,
    STORE_UPDATED: `${serviceName}.store.updated`,
    STORE_DELETED: `${serviceName}.store.deleted`
};

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Store.addHook('afterSave', (model) => {
    eventBus.emit(Store.Events.STORE_CREATED, model);
});

Store.addHook('afterUpdate', (model) => {
    eventBus.emit(Store.Events.STORE_UPDATED, model);
});

Store.addHook('afterDestroy', (model) => {
    eventBus.emit(Store.Events.STORE_DELETED, model);
});

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.keyword) {
        options.name = {
            [Op.iLike]: `%${options.keyword}%`
        };
    }
    delete options.keyword;

    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;

    // date filters
    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Transform postgres model to expose object
 */
Store.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'name',
        'logo',
        'phone',
        'email',
        'status',
        'condinate',
        'description',
        'is_visible',

        /** store location */
        'address',
        'province',
        'district',
        'ward',

        'created_by',
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',

    ];
    dateFields.forEach((field) => {
        transformed[field] = moment(params[field]).unix();
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Store.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'name',
        'logo',
        'phone',
        'email',
        'condinate',
        'description',
        'is_visible',

        /** store location */
        'address',
        'province',
        'district',
        'ward',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Get Store By Id
 *
 * @public
 * @param {String} StoreId
 */
Store.get = async (id) => {
    try {
        const store = await Store.findOne({
            where: {
                id,
                is_active: true
            }
        });
        if (!store) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: messages.NOT_FOUND
            });
        }
        return store;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * List records
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Store[]>}
 */
Store.list = async ({
    keyword,
    statuses,
    is_visible,
    min_created_at,
    max_created_at,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        keyword,
        statuses,
        is_visible,
        min_created_at,
        max_created_at
    });
    return Store.findAll({
        where: options,
        order: [
            ['created_at', 'desc']
        ],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
Store.totalRecords = ({
    keyword,
    statuses,
    is_visible,
    min_created_at,
    max_created_at
}) => {
    const options = filterConditions({
        keyword,
        statuses,
        is_visible,
        min_created_at,
        max_created_at
    });

    return Store.count({ where: options });
};


/**
 * Filter only allowed fields from user
 *
 * @param {Object} params
 */
Store.filterParams = params => pick(params, PUBLIC_FIELDS);


/**
 * @typedef Store
 */
export default Store;
