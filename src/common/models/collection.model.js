/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { DataTypes, Model, Op } from 'sequelize';
import { isEqual, includes, pick, omitBy, isNil } from 'lodash';
import postgres from '../../config/postgres';
import eventBus from '../services/event-bus';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
const sequelize = postgres.connect();
class Collection extends Model {}

const PUBLIC_FIELDS = [
    'title',
    'slug',
    'avatar',
    'content',
    'products',
    'images',
    'mobile_content',
    'mobile_images',
    'cover',
    'background',
    'is_visible',
    'description',
    'is_home_visible'
];

// product fields
Collection.PRODUCT_FIELDS = [
    'id',
    'sku',
    'slug',
    'name',
    'thumbnail_url',
    'normal_price',
    'price'
];
/**
 * Collection Schema
 * @public
 */
Collection.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    title: {
        type: DataTypes.STRING(155),
        required: true,
    },
    slug: {
        type: DataTypes.STRING(255),
        unique: true,
        required: false
    },
    avatar: {
        type: DataTypes.STRING(255),
        required: true,
    },
    description: {
        type: DataTypes.STRING(255),
        required: true,
    },
    content: {
        type: DataTypes.TEXT,
        required: true,
    },
    mobile_content: {
        type: DataTypes.TEXT,
        required: true,
    },
    images: {
        type: DataTypes.ARRAY(DataTypes.STRING(255)),
        required: true,
    },
    mobile_images: {
        type: DataTypes.ARRAY(DataTypes.STRING(255)),
        required: true,
    },
    cover: {
        type: DataTypes.STRING(255),
        required: false,
    },
    background: {
        type: DataTypes.STRING(255),
        required: false,
    },
    products: {
        type: DataTypes.JSONB,
        deefaultValue: []
    },
    normalize_products: {
        type: DataTypes.JSONB,
        deefaultValue: []
    },
    // social seo
    meta_url: {
        type: DataTypes.STRING(255),
        defaultValue: null,
    },
    meta_title: {
        type: DataTypes.STRING(155),
        defaultValue: null,
    },
    meta_image: {
        type: DataTypes.STRING(255),
        defaultValue: null,
    },
    meta_keyword: {
        type: DataTypes.STRING(255),
        defaultValue: null,
    },
    meta_description: {
        type: DataTypes.STRING(255),
        defaultValue: null,
    },

    // manager
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    },
    is_home_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown',
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown',
    },
    system_id: {
        type: DataTypes.INTEGER,
        defaultValue: null,
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null,
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date(),
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null,
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date(),
    },
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'collection',
    tableName: 'tbl_collections',
});

/**
 * Register event emiter
 */
Collection.EVENT_SOURCE = `${serviceName}.collection`;
Collection.Events = {
    COLLECTION_CREATED: `${serviceName}.collection.created`,
    COLLECTION_UPDATED: `${serviceName}.collection.updated`,
    COLLECTION_DELETED: `${serviceName}.collection.deleted`,
};

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Collection.addHook('afterCreate', (model) => {
    eventBus.emit(Collection.Events.COLLECTION_CREATED, model);
});

Collection.addHook('afterUpdate', () => {});

Collection.addHook('afterDestroy', () => {});

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) || !isNil(options[`max_${field}`])) {
        if (options[`min_${field}`] && !options[`max_${field}`]) {
            options[field] = {
                [Op.gte]: _min,
            };
        } else if (!options[`min_${field}`] && options[`max_${field}`]) {
            options[field] = {
                [Op.lte]: _max,
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0,
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);

    if (options.keyword) {
        options.title = {
            [Op.iLike]: `%${options.keyword}%`,
        };
    }
    delete options.keyword;

    if (options.product_sku) {
        options.normalize_product = {
            [Op.iLike]: `%${options.product_sku}%`
        };
    }
    delete options.product_sku;

    if (options.product_name) {
        options.normalize_product = {
            [Op.iLike]: `%${options.product_name}%`
        };
    }
    delete options.product_name;


    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'title':
            sort = ['title', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform mongoose model to expose object
 */
Collection.transform = (model) => {
    const transformed = {};
    const fields = [
        'id',
        'title',
        'slug',
        'avatar',
        'content',
        'products',
        'images',
        'cover',
        'mobile_content',
        'mobile_images',
        'background',
        'description',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',

        // manager
        'is_visible',
        'is_home_visible',
        'is_active',
        'created_by',
        'created_at',
        'updated_at',
    ];

    fields.forEach((field) => {
        transformed[field] = model[field];
    });

    transformed.created_at = moment(model.created_at).unix();
    transformed.updated_at = moment(model.updated_at).unix();
    return transformed;
};

/**
 * Get all changed properties
 *
 * @public
 * @param {Object} data newModel || oleModel
 */
Collection.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'title',
        'slug',
        'avatar',
        'content',
        'products',
        'images',
        'cover',
        'background',
        'description',
        'mobile_content',
        'mobile_images',

        // social seo
        'meta_url',
        'meta_title',
        'meta_image',
        'meta_keyword',
        'meta_description',

        // manager
        'is_visible',
        'is_home_visible',
        'is_active',
        'updated_at',
    ];

    /** get all changable properties */
    allChangableProperties.forEach((field) => {
        if (includes(allChangableProperties, field)) {
            changedProperties.push(field);
        }
    });

    /** get data changed */
    const dataChanged = [];
    changedProperties.forEach((field) => {
        if (!isEqual(newModel[field], oldModel[field])) {
            dataChanged.push(field);
        }
    });
    return dataChanged;
};

/**
 * Get Collection By Id
 *
 * @public
 * @param {String} collectionId
 */
Collection.get = async (collectionId) => {
    try {
        const collection = await Collection.findByPk(collectionId);
        if (!collection) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy bộ sưu tập ${collectionId}`,
            });
        }
        return collection;
    } catch (ex) {
        throw ex;
    }
};

/**
 * List records
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Collection[]>}
 */
Collection.list = async ({
    // sort
    skip = 0,
    limit = 20,
    sort_by,
    order_by,

    // search
    keyword,
    product_sku,
    product_name,
    is_visible,
    is_home_visible,
    min_created_at,
    max_created_at,
}) => {
    const options = filterConditions({
        keyword,
        product_sku,
        product_name,
        is_visible,
        is_home_visible,
        min_created_at,
        max_created_at,

    });
    const sort = sortConditions({ sort_by, order_by });
    return Collection.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit,
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
Collection.totalRecords = ({
    keyword,
    product_sku,
    product_name,
    is_visible,
    is_home_visible,
    min_created_at,
    max_created_at,
}) => {
    const options = filterConditions({
        keyword,
        product_sku,
        product_name,
        is_visible,
        is_home_visible,
        min_created_at,
        max_created_at,

    });
    return Collection.count({
        where: options,
    });
};

/**
 * Filter only allowed fields from import
 *
 * @param {Object} params
 */
Collection.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Collection
 */
export default Collection;
