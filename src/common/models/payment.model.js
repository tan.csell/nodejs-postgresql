/* eslint-disable no-param-reassign */
import { Model, DataTypes, Op } from 'sequelize';
import { pick, isEqual, values, omitBy, isNil, parseInt } from 'lodash';
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import eventBus from '../services/event-bus';
import APIError from '../../common/utils/APIError';

/**
 * Create connection
 */
const { sequelize } = postgres;
class Payment extends Model {}

const PUBLIC_FIELDS = [
    'type',
    'note',
    'value',
    'card',
    'store',
    'group',
    'order',
    'partner',
    'voucher',
    'method',
    'status',
    'status_name',
    'transaction_code',
    'transaction_type',
    'is_visible',
    'is_auto'
];

const PROVIDER_FIELDS = [
    'id',
    'payload' // respone từ các bên cung cấp trả về
];

const ORDER_FIELDS = [
    'id',
    'index',
    'code',
    'total_paid',
    'total_price'
];

const PARTNER_FIELDS = [
    'id',
    'name',
    'phone',
    'address'
];

const STORE_FIELDS = [
    'id',
    'name',
    'phone',
    'address'
];

const CARD_FIELDS = [
    'ccv',
    'name',
    'bank',
    'number'
];

const VOUCHER_FIELDS = [
    'id',
    'code',
    'name',
    'discount_type',
    'discount_value'
];

Payment.Types = {
    PHIEU_THU: 1,
    PHIEU_CHI: 2
};

Payment.Methods = {
    TIEN_MAT: 1,
    CHUYEN_KHOAN: 2,
    QUET_THE: 3,
    VISA: 4,
    ONE_PAY: 5,
    MOMO: 6,
    VNPAY: 7,
    VOUCHER: 8,
    POINT: 9
};

Payment.NameMethods = {
    1: 'Tiền mặt',
    2: 'Chuyển khoản',
    3: 'Quẹt Thẻ',
    4: 'Visa',
    5: 'One Pay',
    6: 'Momo',
    7: 'VNPAY',
    8: 'Voucher',
    9: 'Điểm'
};

Payment.Statuses = {
    PENDING: 'pending',
    COMPLETED: 'completed',
    TRANSFORMED: 'transformed',
    CANCELLED: 'cancelled'
};

Payment.NameStatuses = {
    PENDING: 'Đang chờ duyệt',
    COMPLETED: 'Đã thanh toán',
    TRANSFORMED: 'Chuyển tạm ứng',
    CANCELLED: 'Đã huỷ',
};

Payment.Providers = {
    ONEPAY: 'one_pay',
    VNPAY: 'vn_pay',
    MOMO: 'momo'
};


Payment.TransactionTypes = {
    OTHER: 'other',
    ORDER: 'order',
    DELIVERY: 'delivery',
    TRANSFER: 'transfer'
};

/**
 * Payment Schema
 * @public
 */
Payment.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    code: {
        type: DataTypes.STRING(24),
        defaultValue: null
    },
    type: {
        type: DataTypes.INTEGER,
        values: values(Payment.Types),
        defaultValue: Payment.Types.PHIEU_THU
    },
    note: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    value: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    card: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    store: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    group: {
        type: DataTypes.JSONB,
        defaultValue: null // nhóm phiếu thu | chi
    },
    partner: {
        type: DataTypes.JSONB,
        allowNull: false
    },
    order: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    provider: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    voucher: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    method: {
        type: DataTypes.INTEGER,
        defaultValue: Payment.Methods.TIEN_MAT
    },
    method_name: {
        type: DataTypes.STRING(50),
        defaultValue: Payment.NameMethods[Payment.Methods.TIEN_MAT]
    },
    status: {
        type: DataTypes.STRING(25),
        values: values(Payment.Statuses),
        defaultValue: Payment.Statuses.PENDING
    },
    status_name: {
        type: DataTypes.STRING(50),
        values: values(Payment.NameStatuses),
        defaultValue: Payment.NameStatuses.PENDING
    },
    transaction_code: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    transaction_type: {
        type: DataTypes.STRING(10),
        defaultValue: Payment.TransactionTypes.OTHER
    },


    // manager
    is_auto: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    confirmed_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    confirmed_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    cancelled_at: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    cancelled_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'payment',
    tableName: 'tbl_payments'
});

/**
 * Register event emiter
 */
Payment.Events = {
    PAYMENT_CREATED: `${serviceName}.payment.created`,
    PAYMENT_UPDATED: `${serviceName}.payment.updated`,
    PAYMENT_DELETED: `${serviceName}.payment.deleted`,
    PAYMENT_COMPLETED: `${serviceName}.payment.completed`,
    PAYMENT_CANCELLED: `${serviceName}.payment.cancelled`,
};
Payment.EVENT_SOURCE = `${serviceName}.payment`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Payment.addHook('beforeCreate', async (model) => {
    if (!isNil(model.order)) {
        model.transaction_code = model.order.code;
    }

    return model;
});
Payment.addHook('afterCreate', async (data) => {
    const code = await Payment.generateCode(data);
    await data.update({ code: code }, { hooks: false });
});

// Payment.addHook('afterBulkCreate', (data) => {
//     const operaations = data.filter(i => i.is_visible && i.is_auto);
//     operaations.forEach(i => eventBus.emit(Payment.Events.PAYMENT_CREATED, i));
// });

Payment.addHook('afterCreate', (data) => {
    eventBus.emit(Payment.Events.PAYMENT_CREATED, data);
});

Payment.addHook('afterUpdate', (data) => {
    eventBus.emit(Payment.Events.PAYMENT_UPDATED, data);
});

Payment.addHook('afterDestroy', (data) => {
    eventBus.emit(Payment.Events.PAYMENT_DELETED, data);
});


/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.cards) {
        options['card.number'] = {
            [Op.in]: options.cards.split(',')
        };
    }
    delete options.cards;

    if (options.types) {
        options.type = {
            [Op.in]: options.types.split(',')
        };
    }
    delete options.types;

    if (options.stores) {
        options['store.id'] = {
            [Op.in]: options.stores.split(',')
        };
    }
    delete options.stores;

    if (options.creaters) {
        options['created_by.id'] = {
            [Op.in]: options.creaters.split(',')
        };
    }
    delete options.creaters;

    if (options.confirmers) {
        options['confirmed_by.id'] = {
            [Op.in]: options.confirmers.split(',')
        };
    }
    delete options.confirmers;

    if (options.groups) {
        options['group.id'] = {
            [Op.in]: options.groups.split(',')
        };
    }
    delete options.groups;

    if (options.methods) {
        options.method = {
            [Op.in]: options.methods.split(',')
        };
    }
    delete options.methods;

    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;

    if (options.partners) {
        options['partner.id'] = {
            [Op.in]: options.partners.split(',')
        };
    }
    delete options.partners;

    if (options.note) {
        options.note = {
            [Op.iLike]: `%${options.note}%`
        };
    }

    if (options.code) {
        options.code = {
            [Op.iLike]: `%${options.code}%`
        };
    }

    if (options.transaction_code) {
        options.transaction_code = {
            [Op.iLike]: `%${options.transaction_code}%`
        };
    }

    // Min Max Filter
    checkMinMaxOfConditionFields(options, 'created_at', 'Date');

    return options;
}

/**
 * Transform postgres model to expose object
 */
Payment.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'code',
        'type',
        'note',
        'value',
        'card',
        'store',
        'group',
        'order',
        'voucher',
        'partner',
        'provider',
        'method',
        'method_name',
        'status',
        'status_name',
        'transaction_code',
        'transaction_type',
        'created_by',
        'created_at',
        'updated_by',
        'updated_at',
        'confirmed_by',
        'confirmed_at',
        'cancelled_by',
        'cancelled_at',
        'is_visible',
        'is_auto'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe decimal
    const decimalFields = [
        'value'
    ];
    decimalFields.forEach((field) => {
        transformed[field] = parseInt(params[field], 0);
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',
        'confirmed_at',
        'cancelled_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Payment.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'note',
        'type',
        'card',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isEqual(newModel[field], oldModel[field])) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Get
 *
 * @public
 * @param {String} paymentId
 */
Payment.get = async (paymentId) => {
    try {
        const payment = await Payment.findOne({
            where: {
                id: paymentId,
                is_active: true
            }
        });
        if (isNil(payment)) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy phiếu thanh toán: ${paymentId}`
            });
        }
        return payment;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * List
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Store[]>}
 */
Payment.list = async ({
    code,
    note,
    cards,
    types,
    stores,
    groups,
    methods,
    partners,
    statuses,
    creaters,
    confirmers,
    system_id,
    transaction_code,
    transaction_type,
    min_created_at,
    max_created_at,
    is_visible,
    is_auto,

    // sort condition
    skip = 0,
    limit = 20,
    sort_by = 'desc',
    order_by = 'created_at',
}) => {
    const options = filterConditions({
        code,
        note,
        cards,
        types,
        stores,
        groups,
        methods,
        partners,
        statuses,
        creaters,
        confirmers,
        system_id,
        transaction_code,
        transaction_type,
        min_created_at,
        max_created_at,
        is_visible,
        is_auto,
    });
    return Payment.findAll({
        where: options,
        order: [
            [
                order_by,
                sort_by
            ]
        ],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
Payment.totalRecords = ({
    code,
    note,
    cards,
    types,
    stores,
    groups,
    methods,
    partners,
    statuses,
    creaters,
    confirmers,
    system_id,
    transaction_code,
    transaction_type,
    min_created_at,
    max_created_at,
    is_visible,
    is_auto,
}) => {
    const options = filterConditions({
        code,
        note,
        cards,
        types,
        stores,
        groups,
        methods,
        partners,
        statuses,
        creaters,
        confirmers,
        system_id,
        transaction_code,
        transaction_type,
        min_created_at,
        max_created_at,
        is_visible,
        is_auto,
    });
    return Payment.count({ where: options });
};

/**
 * Total values.
 *
 * @param {number} skip - Number of values to be skipped.
 * @param {number} limit - Limit number of values to be returned.
 * @returns {Promise<Number>}
 */
Payment.totalValues = async ({
    code,
    note,
    cards,
    types,
    stores,
    groups,
    methods,
    partners,
    statuses,
    creaters,
    confirmers,
    system_id,
    transaction_code,
    transaction_type,
    min_created_at,
    max_created_at,
    is_visible,
    is_auto,
}) => {
    const options = filterConditions({
        code,
        note,
        cards,
        types,
        stores,
        groups,
        methods,
        partners,
        statuses,
        creaters,
        confirmers,
        system_id,
        transaction_code,
        transaction_type,
        min_created_at,
        max_created_at,
        is_visible,
        is_auto,
    });
    const total_value_0 = 0;
    const total_value_1 = await Payment.sum(
        'value', {
            where: Object.assign(
                options, { type: Payment.Types.PHIEU_THU }
            )
        });
    const total_value_2 = await Payment.sum(
        'value', {
            where: Object.assign(
                options, { type: Payment.Types.PHIEU_CHI }
            )
        });
    const total_value_3 = Math.ceil(
        (total_value_0 + total_value_1) - total_value_2
    );
    return {
        total_value_0,
        total_value_1,
        total_value_2,
        total_value_3
    };
};

/**
 * Generate code
 *
 * @public
 */
Payment.generateCode = async (model) => {
    const idRule = 'TT';
    if (model &&
        model.order &&
        model.order.id &&
        model.order.code
    ) {
        const newCode = (`${idRule}${model.order.code}`);
        return `${newCode}.${model.order.index ? model.order.index : 0}`;
    }
    return (`${idRule}0000000${model.id}`);
};
/**
 * Filter only allowed fields from payment
 *
 * @param {Object} params
 */
Payment.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * Filter only allowed fields from payment
 *
 * @param {Object} params
 */
Payment.filterProviderParams = (params) => pick(params, PROVIDER_FIELDS);

/**
 * Filter only allowed fields from payment
 *
 * @param {Object} params
 */
Payment.filterPartnerParams = (params) => pick(params, PARTNER_FIELDS);

/**
 * Filter only allowed fields from payment
 *
 * @param {Object} params
 */
Payment.filterStoreParams = (params) => pick(params, STORE_FIELDS);

/**
 * Filter only allowed fields from payment
 *
 * @param {Object} params
 */
Payment.filterCardParams = (params) => pick(params, CARD_FIELDS);

/**
 * Filter only allowed fields from payment
 *
 * @param {Object} params
 */
Payment.filterOrderParams = (params) => pick(params, ORDER_FIELDS);

/**
 * Filter only allowed fields from voucher
 *
 * @param {Object} params
 */
Payment.filterVoucherParams = (params) => pick(params, VOUCHER_FIELDS);

/**
 * @typedef Payment
 */
export default Payment;
