/* eslint-disable no-param-reassign */
import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isNaN, isNil, isUndefined, omitBy, parseInt, pick, values } from 'lodash';
import moment from 'moment-timezone';
import { serviceName } from '../../config/vars';
import eventBus from '../services/event-bus';
import postgres from '../../config/postgres';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
class Delivery extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'code',
    'note',
    'weight',
    'length',
    'height',
    'width',
    'status',
    'status_name',
    'is_favorite',
    'order_id',
    'order_code',
    'order_items',
    'store_id',
    'store_name',
    'store_phone',
    'service_id',
    'service_name',
    'customer_id',
    'customer_name',
    'customer_phone',
    'provider_code',
    'provider_name',
    'provider_payload',
    'sender_name',
    'sender_phone',
    'sender_address',
    'sender_province',
    'sender_district',
    'sender_ward',
    'receiver_name',
    'receiver_phone',
    'receiver_address',
    'receiver_province',
    'receiver_district',
    'receiver_ward',
    'receiver_date',
    'receiver_note',
    'payment_method',
    'payment_by',
    'total_order_price',
    'total_shipping_fee',
    'total_shipping_cod',
    'total_shipping_price',
    'total_insurrance_price',
];

Delivery.Statuses = {
    FAILURE: 'failure',
    PENDING: 'pending',
    PICKING: 'picking',
    RE_PICK: 're_pick',
    PICKED: 'picked',
    DELIVERING: 'delivering',
    RE_DELIVERY: 're_delivery',
    DELIVERED: 'delivered',
    PENDING_RETURN: 'pending_return',
    RETURNING: 'returning',
    RE_RETURN: 're_return',
    CONFIRM_PENDING_RETURN: 'confirm_pending_return',
    RETURNED: 'returned',
    CANCELLED: 'cancelled'
};

Delivery.NameStatuses = {
    FAILURE: 'Bị lỗi',
    PENDING: 'Chờ xử lý',
    PICKING: 'Đang lấy hàng',
    RE_PICK: 'Chờ lấy lại',
    PICKED: 'Đã lấy hàng',
    DELIVERING: 'Đang giao hàng',
    RE_DELIVERY: 'Chờ giao lại',
    DELIVERED: 'Giao thành công',
    PENDING_RETURN: 'Chờ chuyển hoàn',
    RETURNING: 'Đang chuyển hoàn',
    RE_RETURN: 'Chờ chuyển hoàn lại',
    CONFIRM_PENDING_RETURN: 'Chờ xác nhận hoàn',
    RETURNED: 'Đã chuyển hoàn',
    CANCELLED: 'Đã hủy'
};

Delivery.PaymentBys = {
    NGUOIGUI: 'NGUOIGUI',
    NGUOINHAN: 'NGUOINHAN'
};

/**
 * Delivery Schema
 * @public
 */
Delivery.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    code: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    note: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    weight: {
        type: DataTypes.DECIMAL,
        defaultValue: 500
    },
    length: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    height: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    width: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    status: {
        type: DataTypes.STRING(25),
        values: values(Delivery.Statuses),
        defaultValue: null
    },
    status_name: {
        type: DataTypes.STRING(50),
        values: values(Delivery.NameStatuses),
        defaultValue: null
    },
    order_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    order_code: {
        type: DataTypes.STRING(25),
        defaultValue: null
    },
    order_items: {
        type: DataTypes.JSONB,
        defaultValue: []
    },
    store_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    store_name: {
        type: DataTypes.STRING(155),
        defaultValue: null
    },
    store_phone: {
        type: DataTypes.STRING(15),
        defaultValue: null
    },
    service_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    service_name: {
        type: DataTypes.STRING(155),
        defaultValue: null
    },
    customer_id: {
        type: DataTypes.BIGINT,
        defaultValue: null
    },
    customer_name: {
        type: DataTypes.STRING(155),
        defaultValue: null
    },
    customer_phone: {
        type: DataTypes.STRING(15),
        defaultValue: null
    },
    provider_code: {
        type: DataTypes.STRING(25),
        defaultValue: null
    },
    provider_name: {
        type: DataTypes.STRING(155),
        defaultValue: null
    },
    provider_payload: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    sender_name: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    sender_phone: {
        type: DataTypes.STRING(15),
        defaultValue: null
    },
    sender_address: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    sender_province: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    sender_district: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    sender_ward: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    receiver_name: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    receiver_phone: {
        type: DataTypes.STRING(15),
        defaultValue: null
    },
    receiver_note: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    receiver_address: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    receiver_province: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    receiver_district: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    receiver_ward: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    receiver_date: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    payment_method: {
        type: DataTypes.STRING(25),
        defaultValue: 'cash'
    },
    payment_code: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    payment_by: {
        type: DataTypes.STRING(50),
        values: values(Delivery.PaymentBys),
        defaultValue: Delivery.PaymentBys.NGUOINHAN
    },
    total_order_price: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_shipping_fee: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_shipping_cod: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_shipping_price: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },
    total_insurrance_price: {
        type: DataTypes.DECIMAL,
        defaultValue: 0
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_favorite: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    client_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    confirmed_at: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    confirmed_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
    completed_at: {
        type: DataTypes.DATE,
        defaultValue: null
    },
    completed_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    },
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'delivery',
    tableName: 'tbl_deliveries'
});

/**
 * Register event emiter
 */
Delivery.Events = {
    DELIVERY_CREATED: `${serviceName}.delivery.created`,
    DELIVERY_UPDATED: `${serviceName}.delivery.updated`,
    DELIVERY_DELETED: `${serviceName}.delivery.deleted`,
};
Delivery.EVENT_SOURCE = `${serviceName}.delivery`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Delivery.addHook('afterCreate', (model) => {
    eventBus.emit(Delivery.Events.DELIVERY_CREATED, model);
});

Delivery.addHook('afterUpdate', (model) => {
    eventBus.emit(Delivery.Events.DELIVERY_UPDATED, model);
});

Delivery.addHook('afterDestroy', (model) => {
    eventBus.emit(Delivery.Events.DELIVERY_DELETED, model);
});

/**
 * Check min or max in condition
 * @param {*} options
 * @param {*} field
 * @param {*} type
 */
function checkMinMaxOfConditionFields(options, field, type = 'Number') {
    let _min = null;
    let _max = null;

    // Transform min max
    if (type === 'Date') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(0, 0, 0, 0);

        _max = new Date(options[`max_${field}`]);
        _max.setHours(23, 59, 59, 999);
    } else if (type === 'Time') {
        _min = new Date(options[`min_${field}`]);
        _min.setHours(_min.getHours(), _min.getMinutes(), _min.getSeconds(), _min.getMilliseconds());

        _max = new Date(options[`max_${field}`]);
        _max.setHours(_max.getHours(), _max.getMinutes(), _max.getSeconds(), _max.getMilliseconds());
    } else {
        _min = parseFloat(options[`min_${field}`]);
        _max = parseFloat(options[`max_${field}`]);
    }

    // Transform condition
    if (!isNil(options[`min_${field}`]) ||
        !isNil(options[`max_${field}`])
    ) {
        if (
            options[`min_${field}`] &&
            !options[`max_${field}`]
        ) {
            options[field] = {
                [Op.gte]: _min
            };
        } else if (!options[`min_${field}`] &&
            options[`max_${field}`]
        ) {
            options[field] = {
                [Op.lte]: _max
            };
        } else {
            options[field] = {
                [Op.gte]: _min || 0,
                [Op.lte]: _max || 0
            };
        }
    }

    // Remove first condition
    delete options[`max_${field}`];
    delete options[`min_${field}`];
}

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.code) {
        options.code = {
            [Op.iLike]: `%${options.code}%`
        };
    }

    if (options.note) {
        options.note = {
            [Op.iLike]: `%${options.note}%`
        };
    }

    if (options.order_code) {
        options.order_code = {
            [Op.iLike]: `%${options.order_code}%`
        };
    }

    if (options.payment_code) {
        options.payment_code = {
            [Op.iLike]: `%${options.payment_code}%`
        };
    }

    if (options.customer) {
        options[Op.or] = [{
            receiver_name: {
                [Op.iLike]: `%${options.customer}%`
            }
        },
        {
            receiver_phone: {
                [Op.iLike]: `%${options.customer}%`
            }
        }
        ];
    }
    delete options.customer;

    if (options.province_code) {
        options['receiver_provider.code'] = options.province_code;
    }
    delete options.province_code;

    if (options.district_code) {
        options['receiver_district.code'] = options.district_code;
    }
    delete options.district_code;

    if (options.stores) {
        options.store_id = {
            [Op.in]: options.stores.split(',')
        };
    }
    delete options.stores;

    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;

    if (options.providers) {
        options.provider_code = {
            [Op.in]: options.providers.split(',')
        };
    }
    delete options.providers;

    if (options.has_cod) {
        options.total_shipping_cod = {
            [Op.gt]: 0
        };
    }
    delete options.has_cod;

    // date range
    checkMinMaxOfConditionFields(options, 'created_at', options.date_type || 'Date');
    checkMinMaxOfConditionFields(options, 'confirmed_at', options.date_type || 'Date');
    checkMinMaxOfConditionFields(options, 'completed_at', options.date_type || 'Date');
    checkMinMaxOfConditionFields(options, 'receiver_date', options.date_type || 'Date');
    delete options.date_type;

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'code':
            sort = ['code', order_by];
            break;
        case 'note':
            sort = ['note', order_by];
            break;
        case 'is_favorite':
            sort = ['is_favorite', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        case 'total_shipping_fee':
            sort = ['total_shipping_fee', order_by];
            break;
        case 'total_shipping_price':
            sort = ['total_shipping_price', order_by];
            break;
        case 'total_shipping_code':
            sort = ['total_shipping_code', order_by];
            break;
        case 'total_insurrance_price':
            sort = ['total_insurrance_price', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Delivery.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'code',
        'note',
        'weight',
        'length',
        'height',
        'width',
        'status',
        'status_name',
        'is_favorite',
        'order_id',
        'order_code',
        'order_items',
        'store_id',
        'store_name',
        'store_phone',
        'service_id',
        'service_name',
        'customer_id',
        'customer_name',
        'customer_phone',
        'provider_code',
        'provider_name',
        'provider_payload',
        'sender_name',
        'sender_phone',
        'sender_address',
        'sender_province',
        'sender_district',
        'sender_ward',
        'receiver_name',
        'receiver_phone',
        'receiver_note',
        'receiver_address',
        'receiver_province',
        'receiver_district',
        'receiver_ward',
        'receiver_date',
        'total_order_price',
        'total_shipping_fee',
        'total_shipping_cod',
        'total_shipping_price',
        'total_insurrance_price',
        'payment_method',
        'payment_code',
        'payment_by',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'confirmed_at',
        'confirmed_by',
        'cancelled_at',
        'cancelled_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',
        'confirmed_at',
        'cancelled_at',
        'receiver_date'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Delivery.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'note',
        'status',
        'status_name',
        'is_favorite',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Delivery.get = async (id) => {
    try {
        const data = await Delivery.findOne({
            where: {
                [Op.or]: [
                    { code: id.toString() },
                    { id: !isNaN(parseInt(id, 0)) ? id : null }
                ],
                is_active: true
            }
        });
        if (!data) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy phiếu vận đơn này'
            });
        }
        return data;
    } catch (error) {
        throw error;
    }
};

/**
 * List records in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Supplider[]>}
 */
Delivery.list = async ({
    code,
    note,
    has_cod,
    customer,
    order_code,
    payment_code,
    system_id,
    date_type,
    stores,
    statuses,
    providers,
    min_created_at,
    max_created_at,
    min_confirmed_at,
    max_confirmed_at,
    min_completed_at,
    max_completed_at,
    min_receiver_date,
    max_receiver_date,

    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        code,
        note,
        has_cod,
        customer,
        order_code,
        payment_code,
        system_id,
        date_type,
        stores,
        statuses,
        providers,
        min_created_at,
        max_created_at,
        min_confirmed_at,
        max_confirmed_at,
        min_completed_at,
        max_completed_at,
        min_receiver_date,
        max_receiver_date,
    });
    const sort = sortConditions({
        sort_by,
        order_by
    });
    return Delivery.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
Delivery.totalRecords = ({
    code,
    note,
    has_cod,
    customer,
    order_code,
    payment_code,
    system_id,
    date_type,
    stores,
    statuses,
    providers,
    min_created_at,
    max_created_at,
    min_confirmed_at,
    max_confirmed_at,
    min_completed_at,
    max_completed_at,
    min_receiver_date,
    max_receiver_date,
}) => {
    const options = filterConditions({
        code,
        note,
        has_cod,
        customer,
        order_code,
        payment_code,
        system_id,
        date_type,
        stores,
        statuses,
        providers,
        min_created_at,
        max_created_at,
        min_confirmed_at,
        max_confirmed_at,
        min_completed_at,
        max_completed_at,
        min_receiver_date,
        max_receiver_date,
    });

    return Delivery.count({ where: options });
};

/**
 * Filter only allowed fields from Delivery
 *
 * @param {Object} params
 */
Delivery.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Delivery
 */
export default Delivery;
