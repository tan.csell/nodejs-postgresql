import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { DataTypes, Model } from 'sequelize';
import { isEqual, includes, omitBy, isNil } from 'lodash';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';
import APIError from '../utils/APIError';
import eventBus from '../services/event-bus';

const PUBLIC_FILEDS = [
    'id',
    'content',
    'like_count',
    'reply_count',
    'created_by',
    'created_at',
    'updated_at'
];

/**
 * Create connection
 */
const sequelize = postgres.connect();
class ProductComment extends Model {}

ProductComment.Statuses = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
};

/**
 * User Schema
 * @public
 */
ProductComment.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    parent_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    product: {
        type: DataTypes.JSONB,
        defaultValue: {
            id: null,
            option_id: null,
            name: null,
            thumbnail_url: null
        }
    },
    like_count: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    reply_count: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    is_reply: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: () => new Date()
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    underscored: true,
    schema: serviceName,
    sequelize: sequelize,
    tableName: 'tbl_product_comments'
});

ProductComment.hasMany(ProductComment, { as: 'comments', sourceKey: 'id', foreignKey: 'parent_id', constraints: false });
/**
 * Register event emiter
 */
ProductComment.EVENT_SOURCE = `${serviceName}.comment`;
ProductComment.Events = {
    COMMENT_CREATED: `${serviceName}.comment.created`,
    COMMENT_UPDATED: `${serviceName}.comment.updated`,
    COMMENT_DELETED: `${serviceName}.comment.deleted`
};

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
ProductComment.addHook('afterCreate', (model) => {
    eventBus.emit(ProductComment.Events.COMMENT_CREATED, model);
});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_reply = false;
    options.is_active = true;

    if (options.product_id) {
        options['product.id'] = options.product_id;
    }
    delete options.product_id;

    if (options.user_id) {
        options['created_by.id'] = options.user_id;
    }
    delete options.user_id;

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'parent_id':
            sort = ['parent_id', order_by];
            break;
        case 'like_count':
            sort = ['like_count', order_by];
            break;
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform mongoose model to expose object
 */
ProductComment.transform = (model) => {
    const transformed = {};
    const fields = [
        'id',
        'content',
        'product',
        'comments',
        'like_count',
        'reply_count',
        'created_by',
        'created_at',
        'updated_at'
    ];

    fields.forEach((field) => {
        transformed[field] = model[field];
    });

    const { comments } = model;
    if (comments) {
        comments.map(x => {
            const item = x.dataValues;
            item.created_at = moment(x.created_at).unix();
            item.updated_at = moment(x.updated_at).unix();
            return item;
        });
    }

    transformed.created_at = moment(model.created_at).unix();
    transformed.updated_at = moment(model.updated_at).unix();
    return transformed;
};

/**
 * Get all changed properties
 *
 * @public
 * @param {Object} data newModel || oleModel
 */
ProductComment.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'content'
    ];

    // get all changable properties
    allChangableProperties.forEach((field) => {
        if (includes(allChangableProperties, field)) {
            changedProperties.push(field);
        }
    });

    // get data changed
    const dataChanged = [];
    changedProperties.forEach(field => {
        if (!isEqual(newModel[field], oldModel[field])) {
            dataChanged.push(field);
        }
    });
    return dataChanged;
};

/**
 * Get ProductComment By Id
 *
 * @public
 * @param {String} commentId
 */
ProductComment.get = async (commentId) => {
    try {
        const comment = await ProductComment.findByPk(commentId);
        if (!comment) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy bình luận ${commentId} !`
            });
        }
        return comment;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * List users in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Supplider[]>}
 */
ProductComment.list = async ({
    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,

    // fulltext
    user_id,
    system_id,
    product_id,
}) => {
    const options = filterConditions({
        product_id,
        system_id,
        user_id
    });
    const sort = sortConditions({ sort_by, order_by });
    return ProductComment.findAll({
        include: [{
            as: 'comments',
            model: ProductComment,
            attributes: PUBLIC_FILEDS,
            order: [sort]
        }],
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
ProductComment.totalRecords = ({
    user_id,
    system_id,
    product_id,
}) => {
    const options = filterConditions({
        user_id,
        system_id,
        product_id
    });
    return ProductComment.count({ where: options });
};

/**
 * @typedef ProductComment
 */
export default ProductComment;
