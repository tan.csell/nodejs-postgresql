import { Model, DataTypes, Op } from 'sequelize';
import { pick, isEqual, isNil, omitBy, isUndefined, values } from 'lodash';
import httpStatus from 'http-status';
import moment from 'moment-timezone';
import { serviceName } from '../../config/vars';
import eventBus from '../services/event-bus';
import postgres from '../../config/postgres';
import APIError from '../utils/APIError';

/**
 * Product Price
 * @public
 */
class ProductPrice extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'type',
    'name',
    'status',
    'status_name',
    'applied_groups',
    'applied_stores',
    'applied_members',
    'applied_condition',
    'applied_start_time',
    'applied_stop_time',
    'is_auto_create',
    'is_auto_update',
    'is_default'
];


ProductPrice.Types = {
    PUBLIC: 'public',
    PRIVATE: 'private',
    PUBLIC_WITH_WARNNING: 'public_with_warnning'
};

ProductPrice.Statuses = {
    ACTIVE: 'active',
    INACTIVE: 'inactive'
};

ProductPrice.NameStatuses = {
    ACTIVE: 'Kích hoạt',
    INACTIVE: 'Chưa áp dụng'
};

ProductPrice.DefaultValues = {
    id: 1000,
    name: 'Bảng giá chung'
};

/**
 * Init
 */
ProductPrice.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    type: {
        type: DataTypes.STRING(50),
        values: values(ProductPrice.Types),
        defaultValue: ProductPrice.Types.PRIVATE
    },
    name: {
        type: DataTypes.STRING(50),
        allowNull: false
    },
    status: {
        type: DataTypes.STRING(25),
        values: values(ProductPrice.Statuses),
        defaultValue: ProductPrice.Statuses.ACTIVE
    },
    status_name: {
        type: DataTypes.STRING(50),
        values: values(ProductPrice.NameStatuses),
        defaultValue: ProductPrice.NameStatuses.ACTIVE
    },
    applied_groups: {
        // name: 'Áp dụng cho nhóm khách hàng'
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_stores: {
        // name: 'Áp dụng cho chi nhánh bán hàng'
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_members: {
        // name: 'Áp dụng cho nhân viên bán hàng'
        type: DataTypes.ARRAY(DataTypes.TEXT),
        defaultValue: []
    },
    applied_condition: {
        // name: 'Điều kiện thay đổi giá hàng hoá'
        type: DataTypes.JSONB,
        defaultValue: null
    },
    applied_start_time: {
        // name: 'Ngày bắt đầu'
        type: DataTypes.DATE,
        allowNull: false
    },
    applied_stop_time: {
        // name: 'Ngày kết thúc'
        type: DataTypes.DATE,
        allowNull: false
    },

    // manager
    is_auto_create: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_auto_update: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_default: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_by: {
        type: DataTypes.JSONB,
        defaultValue: null
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'product_price',
    tableName: 'tbl_product_prices'
});

/**
 * Register event emiter
 */
ProductPrice.Events = {
    PRODUCT_PRICE_CREATED: `${serviceName}.product-price.created`,
    PRODUCT_PRICE_UPDATED: `${serviceName}.product-price.updated`,
    PRODUCT_PRICE_DELETED: `${serviceName}.product-price.deleted`,
};
ProductPrice.EVENT_SOURCE = `${serviceName}.product-price`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
ProductPrice.addHook('afterCreate', (data) => {
    eventBus.emit(ProductPrice.Events.PRODUCT_PRICE_CREATED, data);
});

ProductPrice.addHook('afterUpdate', (data) => {
    eventBus.emit(ProductPrice.Events.PRODUCT_PRICE_UPDATED, data);
});

ProductPrice.addHook('afterDestroy', (data) => {
    eventBus.emit(ProductPrice.Events.PRODUCT_PRICE_DELETED, data);
});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.types) {
        options.type = {
            [Op.in]: options.types.split(',')
        };
    }
    delete options.types;

    if (options.statuses) {
        options.status = {
            [Op.in]: options.statuses.split(',')
        };
    }
    delete options.statuses;

    if (options.keyword) {
        options.name = {
            [Op.iLike]: `%${options.keyword}%`
        };
    }
    delete options.keyword;

    return options;
}

/**
 * Transform postgres model to expose object
 */
ProductPrice.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'type',
        'name',
        'status',
        'status_name',
        'applied_groups',
        'applied_stores',
        'applied_members',
        'applied_condition',
        'applied_start_time',
        'applied_stop_time',
        'is_auto_create',
        'is_auto_update',
        'is_default',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at',
        'applied_start_time',
        'applied_stop_time',
    ];
    dateFields.forEach((field) => {
        transformed[field] = moment(params[field]).unix();
    });

    return transformed;
};

/**
 * Get all changed properties
 */
ProductPrice.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'type',
        'name',
        'status',
        'status_name',
        'applied_groups',
        'applied_stores',
        'applied_members',
        'applied_condition',
        'applied_start_time',
        'applied_stop_time',
        'is_auto_create',
        'is_auto_update',
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Get
 *
 * @public
 * @param {String} priceId
 */
ProductPrice.get = async (priceId) => {
    try {
        const price = await ProductPrice.findByPk(
            priceId
        );
        if (isNil(price)) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: `Không tìm thấy bảng giá: ${priceId}`
            });
        }

        return price;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * List
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Store[]>}
 */
ProductPrice.list = async ({
    keyword,
    types,
    statuses,
    system_id,

    // sort condition
    skip = 0,
    limit = 50,
    sort_by = 'desc',
    order_by = 'created_at',
}) => {
    const options = filterConditions({
        keyword,
        types,
        statuses,
        system_id,
    });
    return ProductPrice.findAll({
        where: options,
        order: [
            [
                order_by,
                sort_by
            ]
        ],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Number>}
 */
ProductPrice.totalRecords = ({
    keyword,
    types,
    statuses,
    system_id,
}) => {
    const options = filterConditions({
        keyword,
        types,
        statuses,
        system_id,
    });
    return ProductPrice.count({ where: options });
};

/**
 * Get Is Default
 */
ProductPrice.getIsDefault = async () => {
    try {
        const priceIsDefault = await ProductPrice.findOne({
            where: {
                is_active: true,
                is_default: true
            }
        });

        return priceIsDefault;
    } catch (ex) {
        throw (ex);
    }
};

/**
 * Get Price Operator For Update
 * @param {*} params
 * @returns
 */
ProductPrice.getOperator = ({
    applied_field,
    applied_type,
    applied_value,
    applied_operation
}) => {
    let priceCondition = '';

    if (
        applied_field === 'price' &&
        applied_type === 1
    ) {
        priceCondition = applied_operation === 1 ?
            `CAST(book.item ->> 'price' as numeric) + CAST(book.item ->> 'price' as numeric) * ${(applied_value / 100)}` :
            `CAST(book.item ->> 'price' as numeric) - CAST(book.item ->> 'price' as numeric) *  ${(applied_value / 100)}`;
    }
    if (
        applied_field === 'price' &&
        applied_type === 2
    ) {
        priceCondition = applied_operation === 1 ?
            `CAST(book.item ->> 'price' as numeric) + ${(applied_value)}` :
            `CAST(book.item ->> 'price' as numeric) -  ${(applied_value)}`;
    }
    if (
        applied_field === 'original_price' &&
        applied_type === 1
    ) {
        priceCondition = applied_operation === 1 ?
            `sub.original_price + sub.original_price * ${(applied_value / 100)}` :
            `sub.original_price - sub.original_price *  ${(applied_value / 100)}`;
    }
    if (
        applied_field === 'original_price' &&
        applied_type === 2
    ) {
        priceCondition = applied_operation === 1 ?
            `sub.original_price + ${(applied_value)}` :
            `sub.original_price -  ${(applied_value)}`;
    }
    if (
        applied_field === 'current_price' &&
        applied_type === 1
    ) {
        priceCondition = applied_operation === 1 ?
            `CAST(book.item ->> 'price' as numeric) + CAST(book.item ->> 'price' as numeric) * ${(applied_value / 100)}` :
            `CAST(book.item ->> 'price' as numeric) - CAST(book.item ->> 'price' as numeric) *  ${(applied_value / 100)}`;
    }
    if (
        applied_field === 'current_price' &&
        applied_type === 2
    ) {
        priceCondition = applied_operation === 1 ?
            `CAST(book.item ->> 'price' as numeric) + ${(applied_value)}` :
            `CAST(book.item ->> 'price' as numeric) -  ${(applied_value)}`;
    }

    return priceCondition;
};

/**
 * Filter only allowed fields from product
 *
 * @param {Object} params
 */
ProductPrice.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef ProductPrice
 */
export default ProductPrice;
