import { Model, DataTypes, Op } from 'sequelize';
import { isNil, omitBy, pick } from 'lodash';
import postgres from '../../config/postgres';
import { serviceName } from '../../config/vars';

/**
 * Create connection
 */
class Stock extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'store_id',
    'store_name',
    'product_id',
    'product_option_id',
    'total_stock_quantity',
    'total_order_quantity',
    'total_quantity'
];

/**
 * Stock Schema
 * @public
 */
Stock.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    store_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    store_name: {
        type: DataTypes.STRING(155),
        allowNull: false
    },
    product_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    product_option_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    product_master_id: {
        type: DataTypes.INTEGER,
        defaultValue: null
    },
    total_quantity: {
        type: DataTypes.DECIMAL, // tổng tồn thực tế - tổng đặt hàng
        defaultValue: 0
    },
    total_order_quantity: {
        type: DataTypes.DECIMAL, // tổng tồn đặt hàng
        defaultValue: 0
    },
    total_stock_quantity: {
        type: DataTypes.DECIMAL, // tổng tồn thực tế
        defaultValue: 0
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_warning: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    is_favorite: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'stock',
    tableName: 'tbl_stocks'
});

/**
 * Register event emiter
 */
Stock.Events = {
    STOCK_CREATED: `${serviceName}.stock.created`,
    STOCK_UPDATED: `${serviceName}.stock.updated`,
    STOCK_DELETED: `${serviceName}.stock.deleted`,
    STOCK_PROVIDER_UPDATED: `${serviceName}.stock-provider.updated`
};
Stock.EVENT_SOURCE = `${serviceName}.stock`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Stock.addHook('afterCreate', () => {});

Stock.addHook('afterUpdate', () => {});

Stock.addHook('afterDestroy', () => {});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.is_master) {
        options.product_master_id = {
            [Op.or]: {
                [Op.is]: null,
                [Op.eq]: sequelize.col('product_option_id')
            }
        };
    }
    delete options.is_master;

    return options;
}

/**
 * Transform postgres model to expose object
 */
Stock.transform = (params) => {
    const transformed = {};
    transformed.id = params.store_id;
    transformed.name = params.store_name;
    transformed.product_id = params.product_id;
    transformed.product_option_id = params.product_option_id;
    transformed.product_master_id = params.product_master_id;

    // pipe decimal
    const decimalFields = [
        'total_quantity',
        'total_stock_quantity',
        'total_order_quantity',
    ];
    decimalFields.forEach((field) => {
        transformed[field] = parseFloat(params[field]);
    });

    return transformed;
};

/**
 * List
 *
 * @param {number} skip - Number of records to be skipped.
 * @param {number} limit - Limit number of records to be returned.
 * @returns {Promise<Store[]>}
 */
Stock.list = async ({
    is_master,
    system_id,
    product_id,
    product_option_id,

    // sort condition
    skip = 0,
    limit = 100
}) => {
    const options = filterConditions({
        is_master,
        system_id,
        product_id,
        product_option_id,
    });
    return Stock.findAll({
        where: options,
        order: [
            ['id', 'DESC']
        ],
        attributes: [
            'store_id',
            'store_name',
            'total_quantity',
            'total_stock_quantity',
            'total_order_quantity'
        ],
        offset: skip,
        limit: limit
    });
};

/**
 * Filter only allowed fields from stock
 *
 * @param {Object} params
 */
Stock.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Stock
 */
export default Stock;
