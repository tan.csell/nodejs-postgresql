import httpStatus from 'http-status';
import { Model, DataTypes, Op } from 'sequelize';
import { isEqual, isNil, isUndefined, omitBy, pick, values } from 'lodash';
import moment from 'moment-timezone';
import { serviceName } from '../../config/vars';
import postgres from '../../config/postgres';
import APIError from '../utils/APIError';

/**
 * Create connection
 */
class Banner extends Model {}
const { sequelize } = postgres;

const PUBLIC_FIELDS = [
    'url',
    'type',
    'title',
    'content',
    'position',
    'image_url',
    'mobile_url',
    'is_visible'
];

Banner.Types = {
    HOME_V1_POPUP: 'home_v1_popup',
    HOME_V1_SLIDER: 'home_v1_slider',
    HOME_V1_SUB_BANNER: 'home_v1_sub_banner',

    HOME_V1_PRIMARY_BANNER_1: 'home_v1_primary_banner_1',
    HOME_V1_PRIMARY_BANNER_2: 'home_v1_primary_banner_2',
    HOME_V1_PRIMARY_BANNER_3: 'home_v1_primary_banner_3',
    HOME_V1_PRIMARY_BANNER_4: 'home_v1_primary_banner_4',

    HOME_V1_CATEGORY_HOME_BANNER_1: 'home_v1_category_home_banner_1',
    HOME_V1_CATEGORY_HOME_BANNER_2: 'home_v1_category_home_banner_2',
    HOME_V1_CATEGORY_HOME_BANNER_3: 'home_v1_category_home_banner_3',
    HOME_V1_CATEGORY_HOME_BANNER_4: 'home_v1_category_home_banner_4',
    HOME_V1_CATEGORY_HOME_BANNER_5: 'home_v1_category_home_banner_5',

    FLASH_SALE_DETAIL_BANNER: 'flash_sale_detail_banner',
    HOT_DEAL_DETAIL_BANNER: 'hot_deal_detail_banner',
    PRODUCT_DETAIL_BANNER: 'product_detail_banner'
};

/**
 * Banner Schema
 * @public
 */
Banner.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    url: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    type: {
        type: DataTypes.STRING(155),
        values: values(Banner.Types),
        defaultValue: Banner.Types.HOME_BANNER_V_1
    },
    title: {
        type: DataTypes.STRING(255),
        defaultValue: null
    },
    content: {
        type: DataTypes.TEXT,
        defaultValue: null
    },
    position: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    image_url: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    mobile_url: {
        type: DataTypes.STRING(255),
        allowNull: false
    },

    // metric
    view_count: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },
    click_count: {
        type: DataTypes.INTEGER,
        defaultValue: 0
    },

    // manager
    is_active: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    is_visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },
    device_id: {
        type: DataTypes.STRING(255),
        defaultValue: 'unkown'
    },
    device_ip: {
        type: DataTypes.STRING(12),
        defaultValue: 'unkown'
    },
    system_id: {
        type: DataTypes.STRING(50),
        defaultValue: null
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW
    },
    created_by: {
        type: DataTypes.JSONB,
        defaultValue: null // id | name
    }
}, {
    timestamps: false,
    sequelize: sequelize,
    schema: serviceName,
    modelName: 'banner',
    tableName: 'tbl_banners'
});

/**
 * Register event emiter
 */
Banner.Events = {
    BANNER_CREATED: `${serviceName}.banner.created`,
    BANNER_UPDATED: `${serviceName}.banner.updated`,
    BANNER_DELETED: `${serviceName}.banner.deleted`,
};
Banner.EVENT_SOURCE = `${serviceName}.banner`;

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
Banner.addHook('afterCreate', () => {});

Banner.addHook('afterUpdate', () => {});

Banner.addHook('afterDestroy', () => {});

/**
 * Load query
 * @param {*} params
 */
function filterConditions(params) {
    const options = omitBy(params, isNil);
    options.is_active = true;

    if (options.types) {
        options.type = {
            [Op.in]: options.types.split(',')
        };
    }
    delete options.types;

    return options;
}

/**
 * Load sort query
 * @param {*} sort_by
 * @param {*} order_by
 */
function sortConditions({ sort_by, order_by }) {
    let sort = null;
    switch (sort_by) {
        case 'created_at':
            sort = ['created_at', order_by];
            break;
        case 'updated_at':
            sort = ['updated_at', order_by];
            break;
        default:
            sort = ['created_at', 'DESC'];
            break;
    }
    return sort;
}

/**
 * Transform postgres model to expose object
 */
Banner.transform = (params) => {
    const transformed = {};
    const fields = [
        'id',
        'url',
        'type',
        'title',
        'content',
        'position',
        'image_url',
        'mobile_url',
        'view_count',
        'click_count',
        'is_active',
        'is_visible',
        'created_by'
    ];
    fields.forEach((field) => {
        transformed[field] = params[field];
    });

    // pipe date
    const dateFields = [
        'created_at',
        'updated_at'
    ];
    dateFields.forEach((field) => {
        if (params[field]) {
            transformed[field] = moment(params[field]).unix();
        } else {
            transformed[field] = null;
        }
    });

    return transformed;
};

/**
 * Get all changed properties
 */
Banner.getChangedProperties = ({ newModel, oldModel }) => {
    const changedProperties = [];
    const allChangableProperties = [
        'url',
        'type',
        'title',
        'content',
        'position',
        'image_url',
        'mobile_url',
        'is_visible'
    ];
    if (!oldModel) {
        return allChangableProperties;
    }

    allChangableProperties.forEach((field) => {
        if (!isUndefined(newModel[field]) &&
            !isEqual(newModel[field], oldModel[field])
        ) {
            changedProperties.push(field);
        }
    });

    return changedProperties;
};

/**
 * Detail
 *
 * @public
 * @param {string} id
 */
Banner.get = async (id) => {
    try {
        const data = await Banner.findOne({
            where: {
                id,
                is_active: true
            }
        });
        if (!data) {
            throw new APIError({
                status: httpStatus.NOT_FOUND,
                message: 'Không tìm thấy banner!'
            });
        }
        return data;
    } catch (ex) {
        throw ex;
    }
};

/**
 * List users in descending order of 'createdAt' timestamp.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Supplider[]>}
 */
Banner.list = async ({
    types,
    system_id,
    is_visible,
    // sort
    sort_by,
    order_by,
    skip = 0,
    limit = 20,
}) => {
    const options = filterConditions({
        types,
        system_id,
        is_visible
    });
    const sort = sortConditions({ sort_by, order_by });
    return Banner.findAll({
        where: options,
        order: [sort],
        offset: skip,
        limit: limit
    });
};

/**
 * Total records.
 *
 * @param {number} skip - Number of users to be skipped.
 * @param {number} limit - Limit number of users to be returned.
 * @returns {Promise<Number>}
 */
Banner.totalRecords = ({
    types,
    system_id,
    is_visible
}) => {
    const options = filterConditions({
        types,
        system_id,
        is_visible
    });

    return Banner.count({ where: options });
};

/**
 * Filter only allowed fields from banner
 *
 * @param {Object} params
 */
Banner.filterParams = (params) => pick(params, PUBLIC_FIELDS);

/**
 * @typedef Banner
 */
export default Banner;
