import { serviceName } from '../../config/vars';

export default {
    // Service Permission
    USER: 'user',
    LOGGED_IN: 'staff',

    // POST
    POST_VIEW: `${serviceName}_post_view`,
    POST_CREATE: `${serviceName}_post_create`,
    POST_UPDATE: `${serviceName}_post_update`,
    POST_DELETE: `${serviceName}_post_delete`,

    // CATEGORY POST
    POST_CATEGORY_CREATE: `${serviceName}_post_category_create`,
    POST_CATEGORY_UPDATE: `${serviceName}_post_category_update`,
    POST_CATEGORY_DELETE: `${serviceName}_post_category_delete`,

    // CATEGORY PRODUCT
    CATEGORY_CREATE: `${serviceName}_category_create`,
    CATEGORY_UPDATE: `${serviceName}_category_update`,
    CATEGORY_DELETE: `${serviceName}_category_delete`,

    // For Product Route
    PRODUCT_VIEW: `${serviceName}_product_view`,
    PRODUCT_CREATE: `${serviceName}_product_create`,
    PRODUCT_UPDATE: `${serviceName}_product_update`,
    PRODUCT_DELETE: `${serviceName}_product_delete`,

    // For Order Route
    ORDER_VIEW: `${serviceName}_order_view`,
    ORDER_CREATE: `${serviceName}_order_create`,
    ORDER_UPDATE: `${serviceName}_order_update`,

    // For Location Route
    LOCATION_VIEW: `${serviceName}_location_view`,
    LOCATION_CREATE: `${serviceName}_location_create`,
    LOCATION_UPDATE: `${serviceName}_location_update`,
    LOCATION_DELETE: `${serviceName}_location_delete`,

    // For Banner Route
    BANNER_VIEW: `${serviceName}_banner_view`,
    BANNER_CREATE: `${serviceName}_banner_create`,
    BANNER_UPDATE: `${serviceName}_banner_update`,
    BANNER_DELETE: `${serviceName}_banner_delete`,

    // For Banner Route
    VOUCHER_VIEW: `${serviceName}_voucher_view`,
    VOUCHER_CREATE: `${serviceName}_voucher_create`,
    VOUCHER_UPDATE: `${serviceName}_voucher_update`,
    VOUCHER_DELETE: `${serviceName}_voucher_delete`,

    // For Research Route
    RESEARCH_VIEW: `${serviceName}_research_view`,
    RESEARCH_CREATE: `${serviceName}_research_create`,
    RESEARCH_UPDATE: `${serviceName}_research_update`,
    RESEARCH_DELETE: `${serviceName}_research_delete`,

    // For Promotion Route
    PROMOTION_VIEW: `${serviceName}_promotion_view`,
    PROMOTION_CREATE: `${serviceName}_promotion_create`,
    PROMOTION_UPDATE: `${serviceName}_promotion_update`,
};
